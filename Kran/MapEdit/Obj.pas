unit Obj;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, E_Engine, E_Type, sSkinManager, sGroupBox,
  sButton, sPanel, sComboBox, sDialogs, sLabel, Spin, Menus, ComCtrls, sTrackBar,
  ImgList, ToolWin, sToolBar;

type
  TForm2 = class(TForm)
    sPanel1: TsPanel;
    sLabel1: TsLabel;
    sLabel2: TsLabel;
    sLabel3: TsLabel;
    sLabel4: TsLabel;
    sLabel5: TsLabel;
    sLabel6: TsLabel;
    SpinEdit1: TSpinEdit;
    SpinEdit2: TSpinEdit;
    SpinEdit3: TSpinEdit;
    OpenDialogObj: TsOpenDialog;
    SpinEdit4: TSpinEdit;
    SpinEdit5: TSpinEdit;
    SpinEdit6: TSpinEdit;
    sLabel7: TsLabel;
    sLabel8: TsLabel;
    sLabel9: TsLabel;
    sLabel10: TsLabel;
    sLabel11: TsLabel;
    sLabel12: TsLabel;
    sPanel2: TsPanel;
    sTrackBar1: TsTrackBar;
    sLabel13: TsLabel;
    sPanel3: TsPanel;
    sComboBox1: TsComboBox;
    sButton1: TsButton;
    sToolBar1: TsToolBar;
    ToolButton2: TToolButton;
    ImageList1: TImageList;
    sSaveDialog1: TsSaveDialog;
    ToolButton3: TToolButton;
    OpenDialogMap: TsOpenDialog;
    CheckBox2: TCheckBox;
    RadioGroup1: TRadioGroup;
    procedure sComboBox1Change(Sender: TObject);
    procedure sButton1Click(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure SpinEdit2Change(Sender: TObject);
    procedure SpinEdit3Change(Sender: TObject);
    procedure SpinEdit5Change(Sender: TObject);
    procedure SpinEdit4Change(Sender: TObject);
    procedure SpinEdit6Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sTrackBar1Change(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton3Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;
  map: array of TModel;
  DeleteObj: array of Boolean;

implementation

uses Unit1;

{$R *.dfm}

procedure TForm2.CheckBox2Click(Sender: TObject);
begin
  DeleteObj[sComboBox1.ItemIndex] := CheckBox2.Checked;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  Left := 0;
  Top := 0;

  Form1.Left := 200;
  Form1.Top := 0;
end;

procedure TForm2.RadioGroup1Click(Sender: TObject);
begin
  map[sComboBox1.ItemIndex].TypeObj := RadioGroup1.ItemIndex;
  Caption := IntToStr(RadioGroup1.ItemIndex);
end;

procedure TForm2.sButton1Click(Sender: TObject);
begin
  if OpenDialogObj.Execute then
  begin
    SetLength(map, Length(map)+1);
    SetLength(DeleteObj, Length(DeleteObj)+1);
    ChDir(Dir);

    DeleteObj[Length(DeleteObj)-1] := False;
    map[Length(map)-1] := MSE.ObjLoad.Load(OpenDialogObj.FileName);

    map[Length(map)-1].Path := OpenDialogObj.FileName;
    Delete(map[Length(map)-1].Path, 1, Length(Dir));

    sComboBox1.Items.Add(map[Length(map)-1].Path);
    sComboBox1.ItemIndex := Length(map)-1;

    sPanel1.Enabled := True;
  end;
end;

procedure TForm2.sComboBox1Change(Sender: TObject);
begin
  SpinEdit1.Value := Round(map[sComboBox1.ItemIndex].Pos.X*100);
  SpinEdit2.Value := Round(map[sComboBox1.ItemIndex].Pos.Y*100);
  SpinEdit3.Value := Round(map[sComboBox1.ItemIndex].Pos.Z*100);

  SpinEdit4.Value := Round(map[sComboBox1.ItemIndex].Rot.X);
  SpinEdit5.Value := Round(map[sComboBox1.ItemIndex].Rot.Y);
  SpinEdit6.Value := Round(map[sComboBox1.ItemIndex].Rot.Z);

  RadioGroup1.ItemIndex := map[sComboBox1.ItemIndex].TypeObj;
end;

procedure TForm2.SpinEdit1Change(Sender: TObject);
begin
  map[sComboBox1.ItemIndex].Pos.X := SpinEdit1.Value/100;
end;

procedure TForm2.SpinEdit2Change(Sender: TObject);
begin
  map[sComboBox1.ItemIndex].Pos.Y := SpinEdit2.Value/100;
end;

procedure TForm2.SpinEdit3Change(Sender: TObject);
begin
  map[sComboBox1.ItemIndex].Pos.Z := SpinEdit3.Value/100;
end;

procedure TForm2.SpinEdit4Change(Sender: TObject);
begin
  map[sComboBox1.ItemIndex].Rot.X := SpinEdit4.Value;
end;

procedure TForm2.SpinEdit5Change(Sender: TObject);
begin
  map[sComboBox1.ItemIndex].Rot.Y := SpinEdit5.Value;
end;

procedure TForm2.SpinEdit6Change(Sender: TObject);
begin
  map[sComboBox1.ItemIndex].Rot.Z := SpinEdit6.Value;
end;

procedure TForm2.sTrackBar1Change(Sender: TObject);
begin
  sLabel13.Caption := '�������� ������='+IntToStr(sTrackBar1.Position);
end;

procedure MapSave(MapName: string);
var
  f: file of Byte;
  i, j: Integer;
  s1, s2: PAnsiChar;
  NameSize: Word;
begin
  if ExtractFileExt(MapName)='' then
    MapName := MapName+'.map';

  AssignFile(f, MapName);
  Rewrite(f);

  //i := Length(map);
  i := 0;
  for j := 0 to Length(map)-1 do
    if DeleteObj[j]=False then
      Inc(i);


  BlockWrite(f, i, SizeOf(i));

  for i := 0 to Length(map)-1 do
  if DeleteObj[i]=False then
  begin
    //�������� ��������
    NameSize := Length(map[i].Path);
    GetMem(s1, NameSize+1);
    s1 := PAnsiChar(AnsiString(map[i].Path));
    BlockWrite(f, NameSize, SizeOf(Word));
    BlockWrite(f, s1^, NameSize+1);
    s1 := nil;
  end;

  //�������
  for i := 0 to Length(map)-1 do
  if DeleteObj[i]=False then
    BlockWrite(f, map[i].Pos, SizeOf(map[i].Pos));

  ///����������
  for i := 0 to Length(map)-1 do
  if DeleteObj[i]=False then
    BlockWrite(f, map[i].Rot, SizeOf(map[i].Rot));

  //����
  for i := 0 to Length(map)-1 do
  if DeleteObj[i]=False then
    BlockWrite(f, map[i].TypeObj, SizeOf(map[i].TypeObj));

  CloseFile(f);
end;

procedure TForm2.ToolButton1Click(Sender: TObject);
begin
//
end;

procedure TForm2.ToolButton2Click(Sender: TObject);
begin
  if sSaveDialog1.Execute then
    MapSave(sSaveDialog1.FileName);
end;

procedure TForm2.ToolButton3Click(Sender: TObject);
var
  m: TMap;
  i: Integer;
begin
  if OpenDialogMap.Execute then
  begin
    ChDir(Dir);
    m := MSE.Map.Load(OpenDialogMap.FileName);

    SetLength(map, m.ModelCount);
    SetLength(DeleteObj, m.ModelCount);
    for i := 0 to m.ModelCount-1 do
    begin
      map[i] := m.Model[i];
      map[i].Pos := m.Model[i].Pos;
      map[i].Rot := m.Model[i].Rot;
      map[i].Path := m.Model[i].Path;

      sComboBox1.Items.Add(map[i].Path);
      sComboBox1.ItemIndex := 0;
    end;
  end;
end;

end.
