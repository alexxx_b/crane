unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, E_Engine, E_Type, sSkinManager, sGroupBox,
  sButton, sPanel, sComboBox, sDialogs, sLabel, Spin, Menus, ComCtrls,
  sStatusBar, sCheckBox;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    sSkinManager1: TsSkinManager;
    sStatusBar1: TsStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Sky: TSkyBox;
  cPos, cRot: TVector3f;
  mx, my: Integer;
  K: array[1..6] of Boolean;
  Dir: string;
  ShadowEnable: Boolean=True;

implementation

uses Obj, OpenGL20;

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  Dir := Application.ExeName;
  Dir := ExtractFilePath(Dir);
  Delete(Dir, Pos('MapEdit\', Dir), Length('MapEdit\'));

  MSE := TEngine.Create(Form1.Handle);
  //MSE.Log.Enable := True;
  //MSE.Log.New('Log');

  MSE.Window.Start;
  MSE.Phys.Init(0, -10, 0, 1);

  MSE.Camera.Enable(True);
  MSE.Camera.FOV := 60;
  MSE.Camera.zFar := 0.1;
  MSE.Camera.zNear := 10000;
  MSE.Camera.SetSpeed(5000);

  Sky := MSE.Draw.CreateSkyBox('..\Texture\SkyBox\sky');

  MSE.Draw.Init;
  MSE.Camera.Init;

  MSE.Light.Enable(1);
  MSE.Light.Position(1, 100, 100, 100);
  MSE.Light.Diffuse(1, 1, 1, 1, 1);
  MSE.Light.Ambient(0.2, 0.2, 0.2, 1);

  MSE.ObjLoad.Redaktor := True;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=65 then K[1] := True;
  if Key=68 then K[2] := True;
  if Key=87 then K[3] := True;
  if Key=83 then K[4] := True;
  if Key=17 then K[5] := True;
  if Key=16 then K[6] := True;
end;

procedure TForm1.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key=65 then K[1] := False;
  if Key=68 then K[2] := False;
  if Key=87 then K[3] := False;
  if Key=83 then K[4] := False;
  if Key=17 then K[5] := False;
  if Key=16 then K[6] := False;
end;

procedure TForm1.FormMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
const
  Sens=0.3;
begin
  if ssLeft in Shift then
  begin
    if X>mx then cRot.y := cRot.y+Sens*Abs(Round(x-mx));
    if X<mx then cRot.y := cRot.y-Sens*Abs(Round(x-mx));
    if Y>my then
    begin
      cRot.x := cRot.x+Sens*Abs(Round(y-my));
      if cRot.X > 90 then
        cRot.X := 90;
    end;
    if Y<my then
    begin
      cRot.x := cRot.x-Sens*Abs(Round(y-my));
      if cRot.X<-90 then
        cRot.X:=-90;
    end;
  end;
    mx := X;
    my := Y;
end;

procedure TForm1.FormResize(Sender: TObject);
begin
  glViewport(0, 0, Width, Height);
end;

procedure TForm1.Timer1Timer(Sender: TObject);
var
  i, j: Integer;
begin
  if K[1] then
  begin
      cPos.X := cPos.X + (Form2.sTrackBar1.Position/100)*cos(cRot.y/57);
      cPos.Z := cPos.Z + (Form2.sTrackBar1.Position/100)*sin(cRot.y/57);
  end;
  if K[2] then
  begin
      cPos.X := cPos.X - (Form2.sTrackBar1.Position/100)*cos(cRot.y/57);
      cPos.Z := cPos.Z - (Form2.sTrackBar1.Position/100)*sin(cRot.y/57);
  end;
  if K[3] then
  begin
      cPos.X := cPos.X - (Form2.sTrackBar1.Position/100)*sin(cRot.y/57);
      cPos.Z := cPos.Z + (Form2.sTrackBar1.Position/100)*cos(cRot.y/57);
      cPos.Y := cPos.Y + (Form2.sTrackBar1.Position/100)*sin(cRot.x/57);
  end;
  if K[4] then
  begin
      cPos.X := cPos.X + (Form2.sTrackBar1.Position/100)*sin(cRot.y/57);
      cPos.Z := cPos.Z - (Form2.sTrackBar1.Position/100)*cos(cRot.y/57);
      cPos.Y := cPos.Y - (Form2.sTrackBar1.Position/100)*sin(cRot.x/57);
  end;
  if K[5] then cPos.Y := cPos.Y + (Form2.sTrackBar1.Position/100);
  if K[6] then cPos.Y := cPos.Y - (Form2.sTrackBar1.Position/100);

  MSE.OGL.Clear(True, True, False);
  MSE.OGL.Set3DRender;

  MSE.OGL.RotateX(cRot.X);
  MSE.OGL.RotateY(cRot.Y);
  MSE.OGL.RotateZ(cRot.Z);
  MSE.Draw.SkyBox(Sky);
  MSE.OGL.Translate(cPos.X, cPos.Y, cPos.Z);

  MSE.ObjLoad.ShadowEnable := True;

  for i := 0 to Length(map)-1 do
  if DeleteObj[i]=False then
  begin
    MSE.ObjLoad.Draw(map[i]);
    if Form2.sComboBox1.ItemIndex=i then
    begin
      glPushMatrix;
        MSE.OGL.Translate(map[i].Pos);
        MSE.OGL.RotateX(map[i].Rot.X);
        MSE.OGL.RotateY(map[i].Rot.Y);
        MSE.OGL.RotateZ(map[i].Rot.Z);
        MSE.Draw.BoxLine(map[i].BBox, 2, 0, 1, 0);
      glPopMatrix;
    end;
  end;

  if ((Length(map)-1)>0) then
  begin
    for j:=0 to Length(map)-1 do
    begin
      for i:=0 to 15 do
        map[j].Matrix[i] := 0;
      map[j].Matrix[0] := 1;
      map[j].Matrix[5] := 1;
      map[j].Matrix[10] := 1;
      map[j].Matrix[15] := 1;
    end;
  end;

  MSE.ObjLoad.Render;

  MSE.Light.Position(1, 100, 100, 100);
  MSE.OGL.SwapBuffer;

  sStatusBar1.SimpleText := IntToStr(MSE.OGL.GetFPS);
end;

end.
