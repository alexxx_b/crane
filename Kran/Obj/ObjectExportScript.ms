utility master "OBJSave to File" width:162 height:186
(
	button btn12 "������� ���� *.obj" pos:[23,35] width:118 height:108
	on btn12 pressed  do
(
	filename = GetSaveFileName types:"������� (*.obj)|*.obj|All Files(*.*)|*.*|"	
	if filename == undefined then
		exit 
	else
		file = fopen FileName "wb"

	--��������� ������� � ��� ��������
	count = 0
	for mesh in objects do
	(
		count = count+1
	)
	writeLong file count

--���� �� ���� ��������
for mesh in objects do
(
	canConvertTo mesh TriMeshGeometry
	--if ((classOf mesh) != BoneGeometry) and ((classOf mesh) != Biped_Object) then
	if (classOf mesh != BoneGeometry) and (classOf mesh != Biped_Object) and (canConvertTo mesh TriMeshGeometry) then
	(
		--messagebox mesh.name;
		--��������
		WriteShort file mesh.name.count
		WriteString file mesh.name

		--��������
		WriteShort file 0
		WriteString file ""

		--����� ������������
		WriteByte file 255

		--����
		WriteFloat file (mesh.wirecolor.r/255)
		WriteFloat file (mesh.wirecolor.g/255)
		WriteFloat file (mesh.wirecolor.b/255)

		--������
		WriteByte file 0

		--����������
		WriteByte file 0

		WriteLong file mesh.numVerts
		WriteLong file mesh.numFaces
		WriteLong file mesh.numTVerts

		--�������
              	for i=1 to mesh.numVerts do
              	(
              		local p = [0,0,0]
              		p = GetVert mesh i
			WriteFloat file p.x
			WriteFloat file p.z
			WriteFloat file -p.y
              	)

          	--�����
              	for i=1 to mesh.numFaces do
              	(
                	local p = [0,0,0]
                	p = GetFace mesh i
                	x = (p.x-1) as integer
                	y = (p.y-1) as integer
                	z = (p.z-1) as integer
			WriteShort file x
			WriteShort file y
			WriteShort file z
		)

         	--�������  
              	--for i=1 to mesh.numVerts do
              	--(
                --	local p = [0,0,0]
                --	p = GetNormal mesh i
	--		WriteFloat file p.x
	--		WriteFloat file p.y
	--		WriteFloat file p.z
              	--)
    -- norms    
    for i = 1 to mesh.numFaces do
    (
      if (getfacesmoothgroup mesh i) == 0 then
      (
        in coordsys local fan = getfacenormal mesh i
        n1 = fan
        n2 = fan
        n3 = fan
      )
      else
      (
        in coordsys local fan = (meshop.getfacernormals mesh i)

        if fan.count != 3 do
        (
          in coordsys local fan[1] = getfacenormal mesh i
          fan[2] = fan[1]
          fan[3] = fan[1]
        )

        n1 = fan[1]
        n2 = fan[2]
        n3 = fan[3]
      )

      n1 = normalize(n1)
      n2 = normalize(n2)
      n3 = normalize(n3)
      
	WriteFloat file n1.x
	WriteFloat file n1.z
	WriteFloat file -n1.y

	WriteFloat file n2.x
	WriteFloat file n2.z
	WriteFloat file -n2.y

	WriteFloat file n3.x
	WriteFloat file n3.z
	WriteFloat file -n3.y
    )	
                        
		--���������� ����������
		for i = 1 to mesh.numTVerts do
		(
			try
			(
				p = GetTVert mesh i
				WriteFloat file p.x
				WriteFloat file p.y
			)
			catch
			(
				WriteFloat file 0
				WriteFloat file 0
			)
		)  
		
		--��������� �����
		for i = 1 to mesh.numFaces do
		(
			try
			(
				p = (GetTVFace mesh i) - [1, 1, 1]
				WriteShort file p.x
				WriteShort file p.y
				WriteShort file p.z
			)
			catch
			(
				WriteShort file 65000
				WriteShort file 65000
				WriteShort file 65000
			)
		)		
	)
)


fclose file
print "Ready"
)
)