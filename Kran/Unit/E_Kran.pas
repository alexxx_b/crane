unit E_Kran;

interface

uses
  Windows, ODEImport, E_Type, SysUtils;

type
  TKran = class
    private
      TowerMaxAngularVel, HydroLen, Strela_2_Len, Strela_3_Len, OporaLen: Single;
    public
      RopeLen: Single;
      B_Body, B_Wheel_Back, B_Wheel_Front, B_Tower, B_Strela_1, B_Strela_2, B_Strela_3,
      B_Hydro_1, B_Hydro_2, B_Hook, B_Rudder, B_Lever: TModel;
      B_Opora_1: array[1..4] of TModel;
      B_Opora_2: array[1..4] of TModel;
      LeverPos: array[1..4] of Single;
      Rope: TRope;
      KeyControl: array[1..10] of Byte;
      SmokeWheel: TSmoke;
      MotorRunning: Boolean;
      constructor Create;
      procedure KranUpDate;
      procedure Draw;
  end;

  TCar = record
    Length: Single;//������ �����
    Width: Single;//������ ������
    Height: Single;//������ ������
    Mass: Single;//������ �����

    wRadius: Single;//������
    wMass: Single;//����� ������

    BodySpace: PdxSpace;
    Phys: array[0..7] of TBody;//����
    Joint: array[0..6] of TdJointID; //��������
    Geom: array[0..7] of PdxGeom;//���������

    Rudder_Body: TBody;
    Rudder_Geom: PdxGeom;
    Rudder_Joint: TdJointID;

    Strela_1_Body: TBody;
    Strela_1_Geom: PdxGeom;
    Strela_1_Joint: TdJointID;

    Strela_2_Body: TBody;
    Strela_2_Geom: PdxGeom;
    Strela_2_Joint: TdJointID;

    Strela_3_Body: TBody;
    Strela_3_Geom: PdxGeom;
    Strela_3_Joint: TdJointID;

    Hydro_1_Body: TBody;
    Hydro_1_Geom: PdxGeom;
    Hydro_1_Joint1: TdJointID;

    Hydro_2_Body: TBody;
    Hydro_2_Geom: PdxGeom;
    Hydro_2_Joint1, Hydro_2_Joint2: TdJointID;

    Rope_Body: TBody;
    Rope_Geom: PdxGeom;
    Rope_Joint: TdJointID;

    Hook_Body: TBody;
    Hook_Geom: PdxGeom;
    Hook_Joint, Hook_Joint2: TdJointID;
    Hook_Contact: Boolean;
    Hook_Contact_Geom: PdxGeom;
    p1: TBody;

    Opora_1_Body: array[1..4] of TBody;
    Opora_1_Geom: array[1..4] of PdxGeom;
    Opora_1_Joint1: array[1..4] of TdJointID;

    Lever_Body: array[1..4] of TBody;
    Lever_Geom: array[1..4] of PdxGeom;
    Lever_Joint1: array[1..4] of TdJointID;

    Opora_2_Body: array[1..4] of TBody;
    Opora_2_Geom: array[1..4] of PdxGeom;
    Opora_2_Joint1: array[1..4] of TdJointID;

    Camera_Body: array[1..3] of TBody;
    Camera_Geom: array[1..3] of PdxGeom;
    Camera_Joint: array[1..3] of TdJointID;

    Speed: Double;//��������
    Steer: Double;//���� ��������

    v: TdReal;

    Geom2, Geom3: PdxGeom;
    PhysMass, PhysMass2, PhysMass3: TBody;
    JointMass, JointMass2, JointMass3: TdJointID;
  end;

var
  map: TMap;
  Cars, Cars2: TCar;
  Kran: TKran;

implementation

uses E_Engine, E_Math;

constructor TKran.Create;
var
  i: Integer;
  m: TdMass;
  q: TdQuaternion;
  a: PdVector3;

  Geom: PdxGeom;
  mass: TdMass;
  x, y, z: Single;
  r: TdMatrix3;
begin
  SmokeWheel := MSE.Particles.NewSystem(MSE.Texture.Load('Texture\Particles\Smoke.tga'), 10, 1.5, 0.2, 2, 1, 0);

  KeyControl[1] := 37;
  KeyControl[2] := 38;
  KeyControl[3] := 39;
  KeyControl[4] := 40;
  //������
  KeyControl[5] := 32;
  //������ �����
  KeyControl[6] := 65;
  KeyControl[7] := 81;
  //��������� �����
  KeyControl[8] := 187;
  KeyControl[9] := 189;
  //���������� �����
  KeyControl[10] := 13;

  B_Body := MSE.ObjLoad.Load('obj\Kran\Body.obj');
  B_Rudder := MSE.ObjLoad.Load('obj\Kran\Rudder.obj');
  B_Wheel_Front := MSE.ObjLoad.Load('obj\Kran\Wheel_Front.obj');
  B_Wheel_Back := MSE.ObjLoad.Load('obj\Kran\Wheel_Back.obj');
  B_Tower := MSE.ObjLoad.Load('obj\Kran\Tower.obj');
  B_Strela_1 := MSE.ObjLoad.Load('obj\Kran\Strela_1.obj');
  B_Strela_2 := MSE.ObjLoad.Load('obj\Kran\Strela_2.obj');
  B_Strela_3 := MSE.ObjLoad.Load('obj\Kran\Strela_3.obj');

  B_Hydro_1 := MSE.ObjLoad.Load('obj\Kran\Hydro_1.obj');
  B_Hydro_2 := MSE.ObjLoad.Load('obj\Kran\Hydro_2.obj');
  B_Hook := MSE.ObjLoad.Load('obj\Kran\Hook.obj');
  for i:=1 to 4 do
  begin
    B_Opora_1[i] := MSE.ObjLoad.Load('obj\Kran\Opora_1.obj');
    B_Opora_2[i] := MSE.ObjLoad.Load('obj\Kran\Opora_2.obj');

    LeverPos[i] := 0;
  end;
  B_Lever := MSE.ObjLoad.Load('obj\Kran\Lever.obj');

  Cars.Hook_Contact := False;
  Cars.Hook_Joint2 := 0;

  TowerMaxAngularVel := 0.1;
  HydroLen := 0;
  Strela_2_Len := 0;
  Strela_3_Len := 0;
  RopeLen := 1;
  OporaLen := 0;

  x := -10;
  y := 5;
  z := -60;
   with Cars do
   Begin
    BodySpace := dSimpleSpaceCreate(MSE.Phys.Space);
    dSpaceSetCleanup(BodySpace, 0);

    Length := B_Body.BBox.D;//�����
    Width  := B_Body.BBox.W; //������
    Height := B_Body.BBox.H/2;//������
    Mass   := 1.1; //�����

    wRadius := 1.1;//������ ������
    wMass   := 1; //����� ������

    //������ �����
    Phys[0] := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(Phys[0], x, y, z);
    dMassSetBox(m, 5, Width, Height, Length);
    dMassAdjust(m, 5);
    dBodySetMass(Phys[0], @m);
    Geom[0] := dCreateBox(nil, Width, Height, Length);
    dGeomSetBody(Geom[0], Phys[0]);
    B_Body.Rot.Y := B_Body.Rot.Y+180;

    //������� �����
    {PhysMass2 := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(PhysMass2, x, y, z);
    dMassSetBox(m, 1, Width/10, Height, Length/10);
    dMassAdjust(m, 10);
    dBodySetMass(PhysMass2, @m);
    Geom2 := dCreateBox(nil, Width/10, Height, Length/10);
    dGeomSetBody(Geom2, PhysMass2);
    JointMass2 := dJointCreateHinge(MSE.Phys.World, 0);
    dJointAttach(JointMass2, PhysMass2, Phys[0]);
    A := dBodyGetPosition(PhysMass2);
    dJointSetHingeAnchor(JointMass2, A[0], A[1], A[2]);
    dJointSetHingeAxis(JointMass2, 0, 1, 0);}

    //������� �����
    PhysMass := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(PhysMass, x, y-5, z);
    dMassSetBox(m, 5, Width/5, Height/5, Length/5);
    dMassAdjust(m, 5);
    dBodySetMass(PhysMass, @m);
    JointMass := dJointCreateHinge(MSE.Phys.World, 0);
    dJointAttach(JointMass, PhysMass, Phys[0]);
    A := dBodyGetPosition(PhysMass);
    dJointSetHingeAnchor(JointMass, A[0], A[1], A[2]);
    dJointSetHingeAxis(JointMass, 0, 1, 0);

    //����
    Rudder_Body := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(Rudder_Body, x+0.6, y+0.0, z+4.0);
    dMassSetBox(m, 0.001, 1, 1, 1);
    dMassAdjust(m, 0.001);
    dBodySetMass(Rudder_Body, @m);
    Rudder_Geom := dCreateBox(nil, 1, 1, 1);
    dGeomSetBody(Rudder_Geom, Rudder_Body);
    //������� � �����
    Rudder_Joint := dJointCreateHinge2(MSE.Phys.World, 0);
    dJointAttach(Rudder_Joint, Rudder_Body, Phys[0]);
    A := dBodyGetPosition(Rudder_Body);
    dJointSetHinge2Anchor(Rudder_Joint, A[0], A[1], A[2]);
    dJointSetHinge2Axis1(Rudder_Joint, 0, 1, 0);
    dJointSetHinge2Axis2(Rudder_Joint, 1, 0, 0);
    B_Rudder.Rot.Y := B_Rudder.Rot.Y+180;

    dRSetIdentity(R);
    dRFromAxisAndAngle(R, 1, 0, 0, -30/Rad);
    dBodySetRotation(Rudder_Body, R);

    //������ �����
    Phys[7] := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(Phys[7], x+0.1, y+0.5, z-3);
    dMassSetBox(m, 0.5, B_Tower.BBox.W, B_Tower.BBox.H, B_Tower.BBox.D);
    dMassAdjust(m, 0.5);
    dBodySetMass(Phys[7], @m);
    Geom[7] := dCreateBox(nil, B_Tower.BBox.W, B_Tower.BBox.H, B_Tower.BBox.D);
    dGeomSetBody(Geom[7], Phys[7]);
    //������� ����� � ������
    Joint[6] := dJointCreateHinge(MSE.Phys.World, 0);
    dJointAttach(Joint[6], Phys[0], Phys[7]);
    A := dBodyGetPosition(Phys[7]);
    dJointSetHingeAnchor(Joint[6], A[0], A[1]-(B_Tower.BBox.H/2), A[2]+1.05);
    dJointSetHingeAxis(Joint[6], 0, 1, 0);
    //B_Tower.Pos.Y := B_Tower.Pos.Y-2;
    B_Tower.Rot.Y := B_Tower.Rot.Y+180;

    //������ ������ 1
    Strela_1_Body := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(Strela_1_Body, x, y+0.9, z+0.8);
    dMassSetBox(m, 0.001, B_Strela_1.BBox.W, B_Strela_1.BBox.H, B_Strela_1.BBox.D);
    dMassAdjust(m, 0.001);
    dBodySetMass(Strela_1_Body, @m);
    Strela_1_Geom := dCreateBox(nil, B_Strela_1.BBox.W, B_Strela_1.BBox.H, B_Strela_1.BBox.D);
    dGeomSetBody(Strela_1_Geom, Strela_1_Body);
    //������� ������ 1 � �����
    Strela_1_Joint := dJointCreateHinge(MSE.Phys.World, 0);
    dJointAttach(Strela_1_Joint, Phys[7], Strela_1_Body);
    A := dBodyGetPosition(Strela_1_Body);
    dJointSetHingeAnchor(Strela_1_Joint, A[0], A[1]-(B_Strela_1.BBox.H/2)+0.2, A[2]-(B_Strela_1.BBox.D/2)+0.2);
    dJointSetHingeAxis(Strela_1_Joint, 1, 0, 0);
    //B_Strela_1.Pos.Y := B_Strela_1.Pos.Y-0.5;
    //B_Strela_1.Pos.Z := B_Strela_1.Pos.Z-8;
    B_Strela_1.Rot.Y := B_Strela_1.Rot.Y+180;

    //������ ������ 2
    Strela_2_Body := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(Strela_2_Body, x, y+1.1, z+2.6);
    dMassSetBox(m, 0.3, B_Strela_2.BBox.W, B_Strela_2.BBox.H, B_Strela_2.BBox.D);
    dMassAdjust(m, 0.3);
    dBodySetMass(Strela_2_Body, @m);
    Strela_2_Geom := dCreateBox(nil, B_Strela_2.BBox.W, B_Strela_2.BBox.H, B_Strela_2.BBox.D);
    dGeomSetBody(Strela_2_Geom, Strela_2_Body);
    //������� ������ 1 � �����
    Strela_2_Joint := dJointCreateSlider(MSE.Phys.World, 0);
    dJointAttach(Strela_2_Joint, Strela_1_Body, Strela_2_Body);
    dJointSetSliderAxis(Strela_2_Joint, 0, 0, 1);
    B_Strela_2.Rot.Y := B_Strela_2.Rot.Y+180;

    //������ ������ 3
    Strela_3_Body := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(Strela_3_Body, x, y+0.95, z+1.7);
    dMassSetBox(m, 0.2, B_Strela_3.BBox.W, B_Strela_3.BBox.H, B_Strela_3.BBox.D);
    dMassAdjust(m, 0.2);
    dBodySetMass(Strela_3_Body, @m);
    Strela_3_Geom := dCreateBox(nil, B_Strela_3.BBox.W, B_Strela_3.BBox.H, B_Strela_3.BBox.D);
    dGeomSetBody(Strela_3_Geom, Strela_3_Body);
    //������� ������ 3 � ������ 2
    Strela_3_Joint := dJointCreateSlider(MSE.Phys.World, 0);
    dJointAttach(Strela_3_Joint, Strela_2_Body, Strela_3_Body);
    dJointSetSliderAxis(Strela_3_Joint, 0, 0, 1);
    B_Strela_3.Rot.Y := B_Strela_3.Rot.Y+180;

    //����� 1
    Hydro_1_Body := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(Hydro_1_Body, x, y+0.25, z-1.5);
    dMassSetBox(m, 0.2, B_Hydro_1.BBox.W, B_Hydro_1.BBox.H, B_Hydro_1.BBox.D);
    dMassAdjust(m, 0.2);
    dBodySetMass(Hydro_1_Body, @m);
    Hydro_1_Geom := dCreateBox(nil, B_Hydro_1.BBox.W, B_Hydro_1.BBox.H, B_Hydro_1.BBox.D);
    dGeomSetBody(Hydro_1_Geom, Hydro_1_Body);
    //������ ����
    dRFromAxisAndAngle (r, 1, 0, 0, 0);
    dBodySetRotation(Hydro_1_Body, r);
    //������� ����� 1 � �����
    Hydro_1_Joint1 := dJointCreateHinge(MSE.Phys.World, 0);
    dJointAttach(Hydro_1_Joint1, Phys[7], Hydro_1_Body);
    A := dBodyGetPosition(Hydro_1_Body);
    dJointSetHingeAnchor(Hydro_1_Joint1, A[0], A[1]-(B_Hydro_1.BBox.H/2), A[2]-(B_Hydro_1.BBox.D/2));
    dJointSetHingeAxis(Hydro_1_Joint1, 1, 0, 0);
    B_Hydro_1.Rot.Y := B_Hydro_1.Rot.Y+180;

    dRSetIdentity(R);
    dRFromAxisAndAngle(R, 1, 0, 0, -16.5/Rad);
    dBodySetRotation(Hydro_1_Body, R);

    //����� 2
    Hydro_2_Body := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(Hydro_2_Body, x, y+0.7, z);
    dMassSetBox(m, 0.2, B_Hydro_2.BBox.W, B_Hydro_2.BBox.H, B_Hydro_2.BBox.D);
    dMassAdjust(m, 0.2);
    dBodySetMass(Hydro_2_Body, @m);
    Hydro_2_Geom := dCreateBox(nil, B_Hydro_2.BBox.W, B_Hydro_2.BBox.H, B_Hydro_2.BBox.D);
    dGeomSetBody(Hydro_2_Geom, Hydro_2_Body);
    //������ ����
    dRFromAxisAndAngle (r, 1, 0, 0, 0);
    dBodySetRotation(Hydro_2_Body, r);
    //������� ����� 2 � ������
    Hydro_2_Joint1 := dJointCreateHinge(MSE.Phys.World, 0);
    dJointAttach(Hydro_2_Joint1, Strela_1_Body, Hydro_2_Body);
    A := dBodyGetPosition(Hydro_2_Body);
    dJointSetHingeAnchor(Hydro_2_Joint1, A[0], A[1]+(B_Hydro_2.BBox.H/2), A[2]+(B_Hydro_2.BBox.D/2));
    dJointSetHingeAxis(Hydro_2_Joint1, 1, 0, 0);

    dRSetIdentity(R);
    dRFromAxisAndAngle(R, 1, 0, 0, -17/Rad);
    dBodySetRotation(Hydro_2_Body, R);
    //�������� ����� 1 � ����� 2
    Hydro_2_Joint2 := dJointCreateSlider(MSE.Phys.World, 0);
    dJointAttach(Hydro_2_Joint2, Hydro_1_Body, Hydro_2_Body);
    dJointSetSliderAxis(Hydro_2_Joint2, 0, 0.3, 1.0);
    B_Hydro_2.Rot.Y := B_Hydro_2.Rot.Y+180;

    //����
    Rope_Body := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(Rope_Body, x, y-(1/2)+0.8, z+6.1);
    dMassSetBox(m, 0.01, 0.1, 1, 0.1);
    dMassAdjust(m, 0.01);
    dBodySetMass(Rope_Body, @m);
    Rope_Geom := dCreateBox(nil, 0.1, 1, 0.1);
    dGeomSetBody(Rope_Geom, Rope_Body);
    //������� � ������
    Rope_Joint := dJointCreateBall(MSE.Phys.World, 0);
    dJointAttach(Rope_Joint, Strela_3_Body, Rope_Body);
    A := dBodyGetPosition(Rope_Body);
    dJointSetBallAnchor(Rope_Joint, A[0], A[1]+(1/2), A[2]);

    //����
    Hook_Body := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(Hook_Body, x, y-1.5, z+6.1);
    dMassSetBox(m, 0.05, B_Hook.BBox.W, B_Hook.BBox.H, B_Hook.BBox.D);
    dMassAdjust(m, 0.05);
    dBodySetMass(Hook_Body, @m);
    Hook_Geom := dCreateBox(nil, B_Hook.BBox.W, B_Hook.BBox.H, B_Hook.BBox.D);
    dGeomSetBody(Hook_Geom, Hook_Body);
    //������� � �����
    Hook_Joint := dJointCreateSlider(MSE.Phys.World, 0);
    dJointAttach(Hook_Joint, Rope_Body, Hook_Body);
    dJointSetSliderAxis(Hook_Joint, 0, 1, 0);
    B_Hook.Rot.Y := B_Hook.Rot.Y+180;
    Cars.p1 := Hook_Body;

    //�����
    for i:=1 to 4 do
    begin
      Opora_1_Body[i] := dBodyCreate(MSE.Phys.World);
      //dBodySetPosition(Opora_1_Body, x+0.7, y-0.95, z+0.37);
    //����������� ������� ��� ������ �����
      if i=1 then dBodySetPosition(Opora_1_Body[1], x+0.85, y-0.92, z+0.37);
      if i=4 then dBodySetPosition(Opora_1_Body[4], x+0.85, y-0.92, z-4.50);
      if i=2 then dBodySetPosition(Opora_1_Body[2], x-0.86, y-0.92, z+0.37);
      if i=3 then dBodySetPosition(Opora_1_Body[3], x-0.86, y-0.92, z-4.50);
      dMassSetBox(m, 0.1, B_Opora_1[i].BBox.W, B_Opora_1[i].BBox.H, B_Opora_1[i].BBox.D);
      dMassAdjust(m, 0.1);
      dBodySetMass(Opora_1_Body[i], @m);
      Opora_1_Geom[i] := dCreateBox(nil, B_Opora_1[i].BBox.W, B_Opora_1[i].BBox.H, B_Opora_1[i].BBox.D);
      dGeomSetBody(Opora_1_Geom[i], Opora_1_Body[i]);
      //������� ����� � ������
      Opora_1_Joint1[i] := dJointCreateSlider(MSE.Phys.World, 0);
      dJointAttach(Opora_1_Joint1[i], Phys[0], Opora_1_Body[i]);
      dJointSetSliderAxis(Opora_1_Joint1[i], 1, 0, 0);

      Opora_2_Body[i] := dBodyCreate(MSE.Phys.World);
      //����������� ������� ��� ������ �����
      if i=1 then dBodySetPosition(Opora_2_Body[1], x+1.583, y-1.0, z+0.37);
      if i=4 then dBodySetPosition(Opora_2_Body[4], x+1.583, y-1.0, z-4.50);
      if i=2 then dBodySetPosition(Opora_2_Body[2], x-1.593, y-1.0, z+0.37);
      if i=3 then dBodySetPosition(Opora_2_Body[3], x-1.593, y-1.0, z-4.50);
      dMassSetBox(m, 0.1, B_Opora_2[i].BBox.W, B_Opora_2[i].BBox.H, B_Opora_2[i].BBox.D);
      dMassAdjust(m, 0.1);
      dBodySetMass(Opora_2_Body[i], @m);
      Opora_2_Geom[i] := dCreateBox(nil, B_Opora_2[i].BBox.W, B_Opora_2[i].BBox.H, B_Opora_2[i].BBox.D);
      dGeomSetBody(Opora_2_Geom[i], Opora_2_Body[i]);
      //������� ����� � ������
      Opora_2_Joint1[i] := dJointCreateSlider(MSE.Phys.World, 0);
      dJointAttach(Opora_2_Joint1[i], Opora_1_Body[i], Opora_2_Body[i]);
      dJointSetSliderAxis(Opora_2_Joint1[i], 0, 1, 0);
    end;
    //������������� ����� ������� ������ ����� �����
    B_Opora_1[1].Rot.Y := B_Opora_1[1].Rot.Y+180;
    B_Opora_1[4].Rot.Y := B_Opora_1[4].Rot.Y+180;

    //������ ����������
    for i:=1 to 4 do
    begin
      Lever_Body[i] := dBodyCreate(MSE.Phys.World);
      //����������� ������� ��� ������� ������
      if i=1 then dBodySetPosition(Lever_Body[1], x+1.2, y+0.19, z-1.25);
      if i=2 then dBodySetPosition(Lever_Body[2], x+1.1, y+0.19, z-1.25);
      if i=3 then dBodySetPosition(Lever_Body[3], x+1.0, y+0.19, z-1.25);
      if i=4 then dBodySetPosition(Lever_Body[4], x+0.9, y+0.19, z-1.25);
      dMassSetBox(m, 0.001, B_Lever.BBox.W, B_Lever.BBox.H, B_Lever.BBox.D);
      dMassAdjust(m, 0.001);
      dBodySetMass(Lever_Body[i], @m);
      Lever_Geom[i] := dCreateBox(nil, B_Lever.BBox.W, B_Lever.BBox.H, B_Lever.BBox.D);
      dGeomSetBody(Lever_Geom[i], Lever_Body[i]);
      //������� ����� � ������
      Lever_Joint1[i] := dJointCreateHinge(MSE.Phys.World, 0);
      dJointAttach(Lever_Joint1[i], Phys[7], Lever_Body[i]);
      A := dBodyGetPosition(Lever_Body[i]);
      dJointSetHingeAnchor(Lever_Joint1[i], A[0], A[1]-(B_Lever.BBox.H/2), A[2]);
      dJointSetHingeAxis(Lever_Joint1[i], 1, 0, 0);
    end;
    B_Lever.Rot.Y := B_Lever.Rot.Y+180;

    //������ 1
    Camera_Body[1] := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(Camera_Body[1], x+0.6, y+0.5, z+3.4);
    dMassSetBox(m, 0.001, 1, 1, 1);
    dMassAdjust(m, 0.001);
    dBodySetMass(Camera_Body[1], @m);
    Camera_Geom[1] := dCreateBox(nil, 1, 1, 1);
    dGeomSetBody(Camera_Geom[1], Camera_Body[1]);
    //������� � �����
    Camera_Joint[1] := dJointCreateHinge(MSE.Phys.World, 0);
    dJointAttach(Camera_Joint[1], Camera_Body[1], Phys[0]);
    A := dBodyGetPosition(Camera_Body[1]);
    dJointSetHingeAnchor(Camera_Joint[1], A[0], A[1], A[2]);
    dJointSetHingeAxis(Camera_Joint[1], 0, 1, 0);

    //������ 2
    Camera_Body[2] := dBodyCreate(MSE.Phys.World);
    dBodySetPosition(Camera_Body[2], x+1.1, y+1.0, z-1.8);
    dMassSetBox(m, 0.001, 1, 1, 1);
    dMassAdjust(m, 0.001);
    dBodySetMass(Camera_Body[2], @m);
    Camera_Geom[2] := dCreateBox(nil, 1, 1, 1);
    dGeomSetBody(Camera_Geom[2], Camera_Body[2]);
    //������� � �����
    Camera_Joint[2] := dJointCreateHinge(MSE.Phys.World, 0);
    dJointAttach(Camera_Joint[2], Camera_Body[2], Phys[7]);
    A := dBodyGetPosition(Camera_Body[2]);
    dJointSetHingeAnchor(Camera_Joint[2], A[0], A[1], A[2]);
    dJointSetHingeAxis(Camera_Joint[2], 0, 1, 0);

    //������ 6 �����
    for i:=1 to 6 do
    begin
      Phys[i] := dBodyCreate(MSE.Phys.World);
      dQFromAxisAndAngle(Q, 1, 0, 0, PI*0.5);
      dBodySetQuaternion(Phys[i], Q);
      dMassSetSphere(M, 0.3, B_Wheel_Front.BBox.H/2);
      if (i>2) then
        dMassAdjust(M, 0.3)
      else
        dMassAdjust(M, 0.3);
      dBodySetMass(Phys[i], @M);
      Geom[i] := dCreateSphere(nil, B_Wheel_Front.BBox.H/2);
      dBodySetFiniteRotationMode(Phys[i], 1);
      dGeomSetBody(Geom[i], Phys[i]);

      if (i=1) or (i=4) or (i=6) then
      begin
        dQFromAxisAndAngle(Q, 0, 1, 0, PI);
        dBodySetQuaternion(Phys[i], Q);
      end;
    end;

    //������������� ������� �����
    dBodySetPosition (Phys[1],  1.15+x, y-1.42,  3.1+z);
    dBodySetPosition (Phys[2], -1.15+x, y-1.42,  3.1+z);
    dBodySetPosition (Phys[3], -1.18+x, y-1.45, -1.1+z);
    dBodySetPosition (Phys[4],  1.18+x, y-1.45, -1.1+Z);
    dBodySetPosition (Phys[5], -1.18+x, y-1.45, -2.6+z);
    dBodySetPosition (Phys[6],  1.18+x, y-1.45, -2.6+Z);

    //��������� ������ � �������� � ������������� ��������
    for i := 0 to 5 do
    begin
      Joint[i] := dJointCreateHinge2(MSE.Phys.World, 0);
      dJointAttach                  (Joint[i], Phys[0], Phys[i+1]);
      A := dBodyGetPosition         (Phys[i+1]);
      dJointSetHinge2Anchor         (Joint[i], A[0], A[1], A[2]);
      dJointSetHinge2Axis1          (Joint[i], 0, 1, 0);
      dJointSetHinge2Axis2          (Joint[i], 1, 0, 0);

      //���������� �������������
      dJointSetHinge2Param(Joint[i], dParamSuspensionERP, 0.08);
      //��������
      dJointSetHinge2Param(Joint[i], dParamSuspensionCFM, 0.02);

      // ���� �������� ���� �������������� � ���� ��� ������
      dJointSetHinge2Param (Joint[i], dParamLoStop, 0);
      dJointSetHinge2Param (Joint[i], dParamHiStop, 0);
    end;

    dJointSetHinge2Param (joint[0], dParamLoStop, -0.75);
    dJointSetHinge2Param (joint[0], dParamHiStop, 0.75);
    dJointSetHinge2Param (joint[1], dParamLoStop, -0.75);
    dJointSetHinge2Param (joint[1], dParamHiStop, 0.75);

    for i:=0 to 7 do
      dSpaceAdd(BodySpace, Geom[i]);

    dSpaceAdd(BodySpace, Strela_1_Geom);
    dSpaceAdd(BodySpace, Strela_2_Geom);
    dSpaceAdd(BodySpace, Strela_3_Geom);
    dSpaceAdd(BodySpace, Hydro_1_Geom);
    dSpaceAdd(BodySpace, Hydro_2_Geom);
    //dSpaceAdd(BodySpace, Rope_Geom);
    dSpaceAdd(BodySpace, Hook_Geom);
    for i:=1 to 4 do
    begin
      dSpaceAdd(BodySpace, Opora_1_Geom[i]);
      dSpaceAdd(BodySpace, Opora_2_Geom[i]);
    end;

    dSpaceAdd(BodySpace, Cars.Camera_Geom[1]);
   end;


  for i:=1 to 4 do
  begin
    dJointSetSliderParam(Cars.Opora_1_Joint1[i], dParamLoStop, 0);
    dJointSetSliderParam(Cars.Opora_1_Joint1[i], dParamHiStop, 0);

    dJointSetSliderParam(Cars.Opora_2_Joint1[i], dParamLoStop, 0);
    dJointSetSliderParam(Cars.Opora_2_Joint1[i], dParamHiStop, 0);
  end;
  dJointSetSliderParam(Cars.Hook_Joint, dParamLoStop, RopeLen);
  dJointSetSliderParam(Cars.Hook_Joint, dParamHiStop, RopeLen);

  KranUpDate;
end;

procedure TKran.KranUpDate;
const
  FMax=50;
  cACCEL      = 0.02;//�������� ������ �������� lol
  cTURN_SPEED = 0.005;//�������� �������� ����
  MaxSpeed = 18;//������������ ��������
var
  i: Integer;
  Tr, Tr1: PdVector3;
  r: PdMatrix3;
  x, y, z: Single;
  A: TdVector3;

  Rot2: TdMatrix3;
  OporaLen2: Single;
Begin
  with Cars do
  begin
    //������ �����
    if MSE.Input.KeyDown(KeyControl[6]) then
    begin
      dJointSetHingeParam(joint[6], dParamVel, -TowerMaxAngularVel);
      dJointSetHingeParam(joint[6], dParamFMax, 3.0);
      if LeverPos[1]<0.2 then LeverPos[1] := LeverPos[1]+0.005;
    end
    else
    if MSE.Input.KeyDown(KeyControl[7]) then
    begin
      dJointSetHingeParam(joint[6], dParamVel, TowerMaxAngularVel);
      dJointSetHingeParam(joint[6], dParamFMax, 3.0);
      if LeverPos[1]>-0.2 then LeverPos[1] := LeverPos[1]-0.005;
    end
    else
    begin
      if LeverPos[1]<>0 then LeverPos[1] := LeverPos[1]*0.95;
      dJointSetHingeParam(joint[6], dParamVel, 0);
      dJointSetHingeParam(joint[6], dParamFMax, 2000.0);
    end;

    //������ 1
    dJointSetHingeParam (Strela_1_Joint, dParamLoStop, 0.05);
    dJointSetHingeParam (Strela_1_Joint, dParamHiStop, 1.9);

    //��������� ������ �����������
    if MSE.Input.KeyDown(68) and (HydroLen<0.7) then
    begin
      HydroLen := HydroLen+0.0005;
      if LeverPos[3]<0.2 then LeverPos[3] := LeverPos[3]+0.005;
    end
    else
    if MSE.Input.KeyDown(69) and (HydroLen>0.0) and (Cars.Hook_Contact=False) then
    begin
      HydroLen := HydroLen-0.0005;
      if LeverPos[3]>-0.2 then LeverPos[3] := LeverPos[3]-0.005;
    end
    else
      if LeverPos[3]<>0 then LeverPos[3] := LeverPos[3]*0.95;
    dJointSetSliderParam(Hydro_2_Joint2, dParamLoStop, -HydroLen);
    dJointSetSliderParam(Hydro_2_Joint2, dParamHiStop, -HydroLen);

    //��������� ������ ������
    if MSE.Input.KeyDown(87) then
    begin
      if (Strela_2_Len<5.0) then
        Strela_2_Len := Strela_2_Len+0.008
      else
      if (Strela_3_Len<5.0) then
        Strela_3_Len := Strela_3_Len+0.008;

      if (Strela_2_Len<5.0) or (Strela_3_Len<5.0) then
      if LeverPos[2]>-0.2 then LeverPos[2] := LeverPos[2]-0.005;
    end
    else
    if MSE.Input.KeyDown(83) then
    begin
      if (Strela_3_Len>0) then
        Strela_3_Len := Strela_3_Len-0.008
      else
      if (Strela_2_Len>0) then
        Strela_2_Len := Strela_2_Len-0.008;

      if (Strela_3_Len>0) or (Strela_2_Len>0) then
      if LeverPos[2]<0.2 then LeverPos[2] := LeverPos[2]+0.005;
    end
    else
      if LeverPos[2]<>0 then LeverPos[2] := LeverPos[2]*0.95;

    //����������� ����
    if MSE.Input.KeyDown(82) and (RopeLen>-100) and (Cars.Hook_Contact=False) then
    begin
      RopeLen := RopeLen-0.006;
      if LeverPos[4]>-0.2 then LeverPos[4] := LeverPos[4]-0.005;
    end
    else
    if MSE.Input.KeyDown(70) and (RopeLen<1.0) then
    begin
      RopeLen := RopeLen+0.006;
      if LeverPos[4]<0.2 then LeverPos[4] := LeverPos[4]+0.005;
    end
    else
      if LeverPos[4]<>0 then LeverPos[4] := LeverPos[4]*0.95;


    dJointSetSliderParam(Cars.Hook_Joint, dParamLoStop, -RopeLen);
    dJointSetSliderParam(Cars.Hook_Joint, dParamHiStop, -RopeLen);
    dJointSetSliderParam(Strela_2_Joint, dParamLoStop, -Strela_2_Len);
    dJointSetSliderParam(Strela_2_Joint, dParamHiStop, -Strela_2_Len);
    dJointSetSliderParam(Strela_3_Joint, dParamLoStop, -Strela_3_Len);
    dJointSetSliderParam(Strela_3_Joint, dParamHiStop, -Strela_3_Len);

    //��������� �����
    if MSE.Input.KeyDown(KeyControl[8]) and (OporaLen<1.7) then
    begin
      OporaLen := OporaLen+0.004;
    end;
    //����������
    if MSE.Input.KeyDown(KeyControl[9]) and (OporaLen>0) then
    begin
      OporaLen := OporaLen-0.004;
    end;

    for i:=1 to 4 do
    begin
      OporaLen2 := OporaLen;
      if OporaLen>1 then OporaLen2 := 1;

      if (i=1) or (i=4) then
      begin
        dJointSetSliderParam(Opora_1_Joint1[i], dParamLoStop, -OporaLen2);
        dJointSetSliderParam(Opora_1_Joint1[i], dParamHiStop, -OporaLen2);
      end
      else
      begin
        dJointSetSliderParam(Opora_1_Joint1[i], dParamLoStop, OporaLen2);
        dJointSetSliderParam(Opora_1_Joint1[i], dParamHiStop, OporaLen2);
      end;

      //������ �����
      OporaLen2 := 0;
      if OporaLen>1 then OporaLen2 := OporaLen-1;

      if (i=1) or (i=4) then
      begin
        dJointSetSliderParam(Opora_2_Joint1[i], dParamLoStop, OporaLen2);
        dJointSetSliderParam(Opora_2_Joint1[i], dParamHiStop, OporaLen2);
      end
      else
      begin
        dJointSetSliderParam(Opora_2_Joint1[i], dParamLoStop, OporaLen2);
        dJointSetSliderParam(Opora_2_Joint1[i], dParamHiStop, OporaLen2);
      end;
    end;


    if MSE.Input.KeyDown(KeyControl[3]) then
      steer := steer+cTURN_SPEED
    else
    if MSE.Input.KeyDown(KeyControl[1]) then
     steer := steer-cTURN_SPEED
    else
      Steer := Steer*0.98;

    if Steer > 0.75 then Steer := 0.75;

    if Steer < -0.75 then Steer := -0.75;

    dJointSetHinge2Param(Rudder_Joint, dParamLoStop, -steer*4);
    dJointSetHinge2Param(Rudder_Joint, dParamHiStop, -steer*4);
    dJointSetHinge2Param (Rudder_Joint, dParamVel2, 0);
    dJointSetHinge2Param (Rudder_Joint, dParamFMax2, 200000);

    for i:=1 to 4 do
    begin
      dJointSetHingeParam(Lever_Joint1[i], dParamLoStop, LeverPos[i]);
      dJointSetHingeParam(Lever_Joint1[i], dParamHiStop, LeverPos[i]);
    end;

    if (Cars.Hook_Joint2<>0) then
    begin
      dJointSetSliderParam(Hook_Joint2, dParamLoStop, 0);
      dJointSetSliderParam(Hook_Joint2, dParamHiStop, 0.7);

      //dJointSetSliderParam(Hook_Joint2, dParamVel, 0.7);
      //dJointSetSliderParam(Hook_Joint2, dParamFMax, 1000);

      if MSE.Input.KeyDown(KeyControl[10]) then
      begin
        dJointDestroy(Hook_Joint2);
        Hook_Joint2 := 0;
        Cars.p1 := Hook_Body;
      end;
    end;
    

    //���������� ����� ������
    v := steer - dJointGetHinge2Angle1(joint[0]);
    if (v > 0.1) then
      v := 0.1;
    if (v < -0.1) then
      v := -0.1;
    v := v * 20.0;
    dJointSetHinge2Param (joint[0], dParamVel, v);
    dJointSetHinge2Param (joint[0], dParamFMax, 100.0);
    dJointSetHinge2Param (joint[0], dParamFudgeFactor, 0);

    //���������� ������ ������
    v := steer - dJointGetHinge2Angle1(joint[1]);
    if (v > 0.1) then
      v := 0.1;
    if (v < -0.1) then
      v := -0.1;
    v := v * 20.0;
    dJointSetHinge2Param (joint[1], dParamVel, v);
    dJointSetHinge2Param (joint[1], dParamFMax, 100.0);
    dJointSetHinge2Param (joint[1], dParamFudgeFactor, 0);

    MotorRunning := True;

    if MSE.Input.KeyDown(KeyControl[2]) then
    begin
      Speed := Speed+cACCEL;
    end
    else
    if MSE.Input.KeyDown(KeyControl[4]) then
    begin
      Speed := Speed-cACCEL;
    end
    else
    begin
      MotorRunning := False;
      Speed := Speed * 0.98;
    end;

    if Speed > MaxSpeed then
      Speed := MaxSpeed;

    if Speed < -MaxSpeed then
      Speed := -MaxSpeed;

  if MotorRunning then
  begin
    // motor
    dJointSetHinge2Param (joint[0], dParamVel2, -speed);
    dJointSetHinge2Param (joint[0], dParamFMax2, FMax);

    dJointSetHinge2Param (joint[1], dParamVel2, -speed);
    dJointSetHinge2Param (joint[1], dParamFMax2, FMax);

    dJointSetHinge2Param (joint[2], dParamVel2, -speed);
    dJointSetHinge2Param (joint[2], dParamFMax2, FMax);

    dJointSetHinge2Param (joint[3], dParamVel2, -speed);
    dJointSetHinge2Param (joint[3], dParamFMax2, FMax);

    dJointSetHinge2Param (joint[4], dParamVel2, -speed);
    dJointSetHinge2Param (joint[4], dParamFMax2, FMax);

    dJointSetHinge2Param (joint[5], dParamVel2, -speed);
    dJointSetHinge2Param (joint[5], dParamFMax2, FMax);
  end
  else
  begin
    // motor
    dJointSetHinge2Param (joint[0], dParamVel2, -speed);
    dJointSetHinge2Param (joint[0], dParamFMax2, FMax);

    dJointSetHinge2Param (joint[1], dParamVel2, -speed);
    dJointSetHinge2Param (joint[1], dParamFMax2, FMax);

    dJointSetHinge2Param (joint[2], dParamVel2, -speed);
    dJointSetHinge2Param (joint[2], dParamFMax2, FMax);

    dJointSetHinge2Param (joint[3], dParamVel2, -speed);
    dJointSetHinge2Param (joint[3], dParamFMax2, FMax);

    dJointSetHinge2Param (joint[4], dParamVel2, -speed);
    dJointSetHinge2Param (joint[4], dParamFMax2, FMax);

    dJointSetHinge2Param (joint[5], dParamVel2, -speed);
    dJointSetHinge2Param (joint[5], dParamFMax2, FMax);
  end;

  if MSE.Input.KeyDown(KeyControl[5]) then
  begin
    Speed := 0;
    dJointSetHinge2Param (joint[0], dParamVel2, 0);
    dJointSetHinge2Param (joint[0], dParamFMax2, 10);

    dJointSetHinge2Param (joint[1], dParamVel2, 0);
    dJointSetHinge2Param (joint[1], dParamFMax2, 10);

    dJointSetHinge2Param (joint[2], dParamVel2, 0);
    dJointSetHinge2Param (joint[2], dParamFMax2, 10);

    dJointSetHinge2Param (joint[3], dParamVel2, 0);
    dJointSetHinge2Param (joint[3], dParamFMax2, 10);

    dJointSetHinge2Param (joint[4], dParamVel2, 0);
    dJointSetHinge2Param (joint[4], dParamFMax2, 10);

    dJointSetHinge2Param (joint[5], dParamVel2, 0);
    dJointSetHinge2Param (joint[5], dParamFMax2, 10);
  end;
end;
end;

procedure TKran.Draw;
var
  i: Integer;
  Box: TBBox;
  Rot: PdMatrix3;
  Rot2, RotOld: TdMatrix3;
begin
  MSE.ObjLoad.FrustOff := True;

  for i:=0 to 7 do
  begin
    MSE.OGL.PushMatrix;
      MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(Cars.Phys[i]));

      if i=0 then
      begin
          B_Body.Matrix := MSE.Phys.GetObjectOrientation(Cars.Phys[i]);
          MSE.ObjLoad.Draw(B_Body);

        Box.X := 0;
        Box.Y := 0;
        Box.Z := 0;
        Box.W := Cars.Width;
        Box.H := Cars.Height;
        Box.D := Cars.Length;
        MSE.OGL.SetColor(0, 1, 0);
        //MSE.Draw.BoxLine(Box, 1);
        MSE.OGL.SetColor(1, 1, 1);
      end;

      if (i>0) and (i<3) then
      begin
        B_Wheel_Front.Matrix := MSE.Phys.GetObjectOrientation(Cars.Phys[i]);
        MSE.ObjLoad.Draw(B_Wheel_Front);
        //MSE.Draw.Sphere(1.1, 0);
      end;

      if (i>2) and (i<7) then
      begin
        MSE.OGL.PushMatrix;
        B_Wheel_Back.Matrix := MSE.Phys.GetObjectOrientation(Cars.Phys[i]);
        MSE.ObjLoad.Draw(B_Wheel_Back);
        MSE.OGL.PopMatrix;
        //MSE.Draw.Sphere(B_Wheel_Front.BBox.H/2, 0);
      end;

      if i=7 then
      begin
        B_Tower.Matrix := MSE.Phys.GetObjectOrientation(Cars.Phys[i]);
        MSE.ObjLoad.Draw(B_Tower);

        Box.X := 0;
        Box.Y := 0;
        Box.Z := 0;
        Box.W := B_Tower.BBox.W;
        Box.H := B_Tower.BBox.H;
        Box.D := B_Tower.BBox.D;
        MSE.OGL.SetColor(0, 1, 0);
        //MSE.Draw.BoxLine(Box, 1);
        MSE.OGL.SetColor(1, 1, 1);
      end;
    MSE.OGL.PopMatrix
  end;

  //����
  MSE.OGL.PushMatrix;
    MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(Cars.Rudder_Body));
    B_Rudder.Matrix := MSE.Phys.GetObjectOrientation(Cars.Rudder_Body);
    MSE.ObjLoad.Draw(B_Rudder);

    Box.X := 0;
    Box.Y := 0;
    Box.Z := 0;
    Box.W := B_Rudder.BBox.W;
    Box.H := B_Rudder.BBox.H;
    Box.D := B_Rudder.BBox.D;
    MSE.OGL.SetColor(0, 1, 0);
    //MSE.Draw.BoxLine(Box, 1);
    MSE.OGL.SetColor(1, 1, 1);
  MSE.OGL.PopMatrix;

  //������ 1
  MSE.OGL.PushMatrix;
  MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(Cars.Strela_1_Body));
    B_Strela_1.Matrix := MSE.Phys.GetObjectOrientation(Cars.Strela_1_Body);
    MSE.ObjLoad.Draw(B_Strela_1);

  Box.X := 0;
  Box.Y := 0;
  Box.Z := 0;
  Box.W := B_Strela_1.BBox.W;
  Box.H := B_Strela_1.BBox.H;
  Box.D := B_Strela_1.BBox.D;
  MSE.OGL.SetColor(0, 1, 0);
  //MSE.Draw.BoxLine(Box, 1);
  MSE.OGL.SetColor(1, 1, 1);
  MSE.OGL.PopMatrix;

  //������ 2
  MSE.OGL.PushMatrix;
  MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(Cars.Strela_2_Body));
    B_Strela_2.Matrix := MSE.Phys.GetObjectOrientation(Cars.Strela_2_Body);
    MSE.ObjLoad.Draw(B_Strela_2);

  Box.X := 0;
  Box.Y := 0;
  Box.Z := 0;
  Box.W := B_Strela_2.BBox.W;
  Box.H := B_Strela_2.BBox.H;
  Box.D := B_Strela_2.BBox.D;
  MSE.OGL.SetColor(0, 1, 0);
  //MSE.Draw.BoxLine(Box, 1);
  MSE.OGL.SetColor(1, 1, 1);
  MSE.OGL.PopMatrix;

  //������ 3
  MSE.OGL.PushMatrix;
  MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(Cars.Strela_3_Body));
    B_Strela_3.Matrix := MSE.Phys.GetObjectOrientation(Cars.Strela_3_Body);
    MSE.ObjLoad.Draw(B_Strela_3);

  Box.X := 0;
  Box.Y := 0;
  Box.Z := 0;
  Box.W := B_Strela_3.BBox.W;
  Box.H := B_Strela_3.BBox.H;
  Box.D := B_Strela_3.BBox.D;
  MSE.OGL.SetColor(0, 1, 0);
  //MSE.Draw.BoxLine(Box, 1);
  MSE.OGL.SetColor(1, 1, 1);
  MSE.OGL.PopMatrix;

  //����� 1
  MSE.OGL.PushMatrix;
  MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(Cars.Hydro_1_Body));
    B_Hydro_1.Matrix := MSE.Phys.GetObjectOrientation(Cars.Hydro_1_Body);
    MSE.ObjLoad.Draw(B_Hydro_1);

  Box.X := 0;
  Box.Y := 0;
  Box.Z := 0;
  Box.W := B_Hydro_1.BBox.W;
  Box.H := B_Hydro_1.BBox.H;
  Box.D := B_Hydro_1.BBox.D;
  MSE.OGL.SetColor(0, 1, 0);
  //MSE.Draw.BoxLine(Box, 1);
  MSE.OGL.SetColor(1, 1, 1);
  MSE.OGL.PopMatrix;

  //����� 2
  MSE.OGL.PushMatrix;
  MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(Cars.Hydro_2_Body));
    B_Hydro_2.Matrix := MSE.Phys.GetObjectOrientation(Cars.Hydro_2_Body);
    MSE.ObjLoad.Draw(B_Hydro_2);

  Box.X := 0;
  Box.Y := 0;
  Box.Z := 0;
  Box.W := B_Hydro_2.BBox.W;
  Box.H := B_Hydro_2.BBox.H;
  Box.D := B_Hydro_2.BBox.D;
  MSE.OGL.SetColor(0, 1, 0);
  //MSE.Draw.BoxLine(Box, 1);
  MSE.OGL.SetColor(1, 1, 1);
  MSE.OGL.PopMatrix;

  //����
  MSE.OGL.PushMatrix;
  MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(Cars.Hook_Body));
    B_Hook.Matrix := MSE.Phys.GetObjectOrientation(Cars.Hook_Body);
    MSE.ObjLoad.Draw(B_Hook);

  Box.X := 0;
  Box.Y := 0;
  Box.Z := 0;
  Box.W := B_Hook.BBox.W;
  Box.H := B_Hook.BBox.H;
  Box.D := B_Hook.BBox.D;
  MSE.OGL.SetColor(0, 1, 0);
  //MSE.Draw.BoxLine(Box, 1);
  MSE.OGL.SetColor(1, 1, 1);
  MSE.OGL.PopMatrix;

  //����
  MSE.OGL.PushMatrix;
  MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(Cars.Rope_Body));

  Box.X := 0;
  Box.Y := ((RopeLen-2)/2)+0.45;
  Box.Z := 0;
  Box.W := 0.01;
  Box.H := (RopeLen-2);
  Box.D := 0.01;
  MSE.OGL.SetColor(0.01, 0.01, 0.01);
    //MSE.Draw.Box(Box, 0);
    MSE.OGL.PushMatrix;
      MSE.OGL.Translate(0.09, ((RopeLen-2)/2)+0.45, 0.09);
      MSE.OGL.RotateX(90);
      MSE.Draw.Cylinder(0.01, (RopeLen-2)+0.1, 0);
    MSE.OGL.PopMatrix;

    MSE.OGL.PushMatrix;
      MSE.OGL.Translate(-0.09, ((RopeLen-2)/2)+0.45, 0.09);
      MSE.OGL.RotateX(90);
      MSE.Draw.Cylinder(0.01, (RopeLen-2)+0.1, 0);
    MSE.OGL.PopMatrix;

    MSE.OGL.PushMatrix;
      MSE.OGL.Translate(-0.09, ((RopeLen-2)/2)+0.45, -0.09);
      MSE.OGL.RotateX(90);
      MSE.Draw.Cylinder(0.01, (RopeLen-2)+0.1, 0);
    MSE.OGL.PopMatrix;

    MSE.OGL.PushMatrix;
      MSE.OGL.Translate(0.09, ((RopeLen-2)/2)+0.45, -0.09);
      MSE.OGL.RotateX(90);
      MSE.Draw.Cylinder(0.01, (RopeLen-2)+0.1, 0);
    MSE.OGL.PopMatrix;
  MSE.OGL.SetColor(1, 1, 1);
  MSE.OGL.PopMatrix;

  //����
  {for i:=0 to Rope.Count do
  begin
    MSE.OGL.PushMatrix;
    MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(Rope.Body[i]));

    Box.X := 0;
    Box.Y := 0;
    Box.Z := 0;
    Box.W := 0.1;
    Box.H := Rope.SectionLen;
    Box.D := 0.1;
    MSE.OGL.SetColor(0, 1, 0);
    MSE.Draw.Box(Box, 1);
    MSE.OGL.SetColor(1, 1, 1);
    MSE.OGL.PopMatrix;
  end; }
  {}

  //����� 1
  for i:=1 to 4 do
  begin
    MSE.OGL.PushMatrix;
    MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(Cars.Opora_1_Body[i]));
    B_Opora_1[i].Matrix := MSE.Phys.GetObjectOrientation(Cars.Opora_1_Body[i]);
    MSE.ObjLoad.Draw(B_Opora_1[i]);

    Box.X := 0;
    Box.Y := 0;
    Box.Z := 0;
    Box.W := B_Opora_1[i].BBox.W;
    Box.H := B_Opora_1[i].BBox.H;
    Box.D := B_Opora_1[i].BBox.D;
    MSE.OGL.SetColor(0, 1, 0);
    //MSE.Draw.BoxLine(Box, 1);
    MSE.OGL.SetColor(1, 1, 1);
    MSE.OGL.PopMatrix;
  end;

  //����� 2
  for i:=1 to 4 do
  begin
    MSE.OGL.PushMatrix;
    MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(Cars.Opora_2_Body[i]));
    B_Opora_2[i].Matrix := MSE.Phys.GetObjectOrientation(Cars.Opora_2_Body[i]);
    MSE.ObjLoad.Draw(B_Opora_2[i]);

    Box.X := 0;
    Box.Y := 0;
    Box.Z := 0;
    Box.W := B_Opora_2[i].BBox.W;
    Box.H := B_Opora_2[i].BBox.H;
    Box.D := B_Opora_2[i].BBox.D;
    MSE.OGL.SetColor(0, 1, 0);
    //MSE.Draw.BoxLine(Box, 1);
    MSE.OGL.SetColor(1, 1, 1);
    MSE.OGL.PopMatrix;
  end;

  //������
  for i:=1 to 4 do
  begin
    MSE.OGL.PushMatrix;
    MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(Cars.Lever_Body[i]));
    B_Lever.Matrix := MSE.Phys.GetObjectOrientation(Cars.Lever_Body[i]);
    MSE.ObjLoad.Draw(B_Lever);

    Box.X := 0;
    Box.Y := 0;
    Box.Z := 0;
    Box.W := B_Lever.BBox.W;
    Box.H := B_Lever.BBox.H;
    Box.D := B_Lever.BBox.D;
    MSE.OGL.SetColor(0, 1, 0);
    //MSE.Draw.BoxLine(Box, 1);
    MSE.OGL.SetColor(1, 1, 1);
    MSE.OGL.PopMatrix;
  end;

  MSE.ObjLoad.FrustOff := False;
end;


end.
