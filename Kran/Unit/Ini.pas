unit Ini;

interface

uses
  Windows, SysUtils, IniFiles;

type
  TIniVar = record
    VideoMode, Map: string;
    VSync: Boolean;
    Phys: Integer;
    AA: Integer;
  end;

var
  IniVar: TIniVar;

//procedure IniSave;
procedure IniLoad;

implementation

uses E_Engine;

//��������� �������� � ��� ����
{procedure IniSave;
var
  Ini: Tinifile;
  i: Integer;
begin
  //������� ���� � ���������� ���������
  Ini := TiniFile.Create(Form1.Dir+'Options.ini');
    Ini.WriteString('Video', 'Mode', Form1.ComboBoxVideoMode.Items[Form1.ComboBoxVideoMode.ItemIndex]);
    Ini.WriteBool('Video', 'VSync', Form1.CheckBoxVSync.Checked);
  Ini.Free;
end;}

procedure IniLoad;
var
  Ini: Tinifile;
  i: Integer;
begin
  //��������� ����
  Ini:=TiniFile.Create(MSE.System.GetProgDir+'Options.ini');
    IniVar.VideoMode := Ini.ReadString('Video', 'Mode', '800*600*16 60��');
    IniVar.VSync := Ini.ReadBool('Video', 'VSync', False);
    IniVar.AA := Ini.ReadInteger('Video', 'AA', 0);

    IniVar.Map := Ini.ReadString('Common', 'Map', '1');
    IniVar.Phys := Ini.ReadInteger('Common', 'Phys', 20);
  Ini.Free;
end;

end.
