unit Obj;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, E_Engine, E_Type, sSkinManager, sGroupBox,
  sButton, sPanel, sComboBox, sDialogs, sLabel, Spin, Menus, ComCtrls, sTrackBar,
  Buttons, sBitBtn, ImgList, sSpeedButton, ToolWin, sToolBar, sSkinProvider,
  sRadioButton, sCheckBox, ExtDlgs, OpenGL20;

type
  TForm2 = class(TForm)
    sPanel1: TsPanel;
    sOpenDialog1: TsOpenDialog;
    sSaveDialog1: TsSaveDialog;
    sButton3: TsButton;
    sLabel2: TsLabel;
    TrackBarAlpha: TsTrackBar;
    LabelTextureName: TsLabel;
    sLabel3: TsLabel;
    TrackBarRed: TsTrackBar;
    TrackBarGreen: TsTrackBar;
    sLabel4: TsLabel;
    TrackBarBlue: TsTrackBar;
    sLabel5: TsLabel;
    sPanel2: TsPanel;
    TrackBarCameraSpeed: TsTrackBar;
    sLabel6: TsLabel;
    ImageList1: TImageList;
    sPanel3: TsPanel;
    sComboBox1: TsComboBox;
    sButton1: TsButton;
    sButton2: TsButton;
    sToolBar1: TsToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    sSkinProvider1: TsSkinProvider;
    sGroupBox1: TsGroupBox;
    sRadioButton1: TsRadioButton;
    sRadioButton2: TsRadioButton;
    sRadioButton0: TsRadioButton;
    CheckBoxTransparency: TsCheckBox;
    OpenDialogTexture: TsOpenPictureDialog;
    CheckBox1: TCheckBox;
    Edit1: TEdit;
    Button1: TButton;
    Edit2: TEdit;
    CheckBox2: TCheckBox;
    procedure sButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sButton3Click(Sender: TObject);
    procedure sComboBox1Change(Sender: TObject);
    procedure TrackBarAlphaChange(Sender: TObject);
    procedure TrackBarRedChange(Sender: TObject);
    procedure TrackBarGreenChange(Sender: TObject);
    procedure TrackBarBlueChange(Sender: TObject);
    procedure TrackBarCameraSpeedChange(Sender: TObject);
    procedure sButton2Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure CheckBoxTransparencyClick(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure sRadioButton0Click(Sender: TObject);
    procedure sRadioButton2Click(Sender: TObject);
    procedure sRadioButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form2: TForm2;
  map: TModel;

implementation

uses Unit1;

{$R *.dfm}

procedure SaveObj(NameObj: string);
var
  f: file of Byte;
  i, j: Integer;
  s1, s2: PAnsiChar;
  NameSize: Word;
  Phys, Transparency: Byte;
begin
with map do
begin
  if ExtractFileExt(NameObj)='' then
    NameObj := NameObj+'.obj';

  AssignFile(f, NameObj);
  Rewrite(f);

  BlockWrite(f, map.ObjCount, SizeOf(map.ObjCount));

  for i := 0 to map.ObjCount-1 do
  begin
    //��������
    NameSize := Length(map.Obj[i].Name);
    GetMem(s1, NameSize+1);
    s1 := PAnsiChar(AnsiString(map.Obj[i].Name));
    BlockWrite(f, NameSize, SizeOf(Word));
    BlockWrite(f, s1^, NameSize+1);
    //��������
    NameSize := Length(map.Obj[i].TextureName);
    GetMem(s2, NameSize+1);
    s2 := PAnsiChar(AnsiString(map.Obj[i].TextureName));
    BlockWrite(f, NameSize, SizeOf(Word));
    BlockWrite(f, s2^, NameSize+1);

    //�����
    BlockWrite(f, map.Obj[i].Alpha, SizeOf(map.Obj[i].Alpha));

    //����
    BlockWrite(f, map.Obj[i].Color, SizeOf(map.Obj[i].Color));

    //������
    //if Form2.sRadioButton0.Checked then Phys := 0;
    //if Form2.sRadioButton1.Checked then Phys := 1;
    //if Form2.sRadioButton2.Checked then Phys := 2;
    BlockWrite(f, map.Obj[i].Phys, SizeOf(map.Obj[i].Phys));

    //������������
    BlockWrite(f, map.Obj[i].Transparency, SizeOf(map.Obj[i].Transparency));

    BlockWrite(f, Obj[i].VertexCount, SizeOf(Obj[i].VertexCount));
    BlockWrite(f, Obj[i].FacesCount, SizeOf(Obj[i].FacesCount));
    BlockWrite(f, Obj[i].TexCoordCount, SizeOf(Obj[i].TexCoordCount));

    for j:=0 to Obj[i].VertexCount-1 do begin//�����
      BlockWrite(f, Obj[i].vertex[j].x, SizeOf(Obj[i].vertex[j].x));
      BlockWrite(f, Obj[i].vertex[j].y, SizeOf(Obj[i].vertex[j].y));
      BlockWrite(f, Obj[i].vertex[j].z, SizeOf(Obj[i].vertex[j].z));
    end;

    for j:=0 to Obj[i].FacesCount-1 do//�����
    begin
      BlockWrite(f, Obj[i].VF[j][0], SizeOf(Obj[i].VF[j][0]));
      BlockWrite(f, Obj[i].VF[j][1], SizeOf(Obj[i].VF[j][1]));
      BlockWrite(f, Obj[i].VF[j][2], SizeOf(Obj[i].VF[j][2]));
    end;

    for j:=0 to Obj[i].FacesCount*3-1 do begin//�������
      BlockWrite(f, Obj[i].normal[j].x, SizeOf(Obj[i].normal[j].x));
      BlockWrite(f, Obj[i].normal[j].y, SizeOf(Obj[i].normal[j].y));
      BlockWrite(f, Obj[i].normal[j].z, SizeOf(Obj[i].normal[j].z));
      //Result.Obj[j].normal[i] := MSE.Math.NormalizeVector(Result.Obj[j].normal[i]);
    end;

    for j:=0 to Obj[i].TexCoordCount-1 do//���������� ����������
    begin
      BlockWrite(f, Obj[i].TexCoord[j].x, SizeOf(Obj[i].TexCoord[j].x));
      BlockWrite(f, Obj[i].TexCoord[j].y, SizeOf(Obj[i].TexCoord[j].y));
    end;

    for j:=0 to Obj[i].FacesCount-1 do//���������� �����
    begin
      BlockWrite(f, Obj[i].TF[j][0], SizeOf(Obj[i].TF[j][0]));
      BlockWrite(f, Obj[i].TF[j][1], SizeOf(Obj[i].TF[j][1]));
      BlockWrite(f, Obj[i].TF[j][2], SizeOf(Obj[i].TF[j][2]));
    end;
  end;

  CloseFile(f);
end;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
  Left := 0;
  Top := 0;

  Form1.Left := 200;
  Form1.Top := 0;
end;

procedure TForm2.sButton1Click(Sender: TObject);
var
  i: Integer;
begin
  if sOpenDialog1.Execute then
  begin
    MapFileName := sOpenDialog1.FileName;
    sPanel1.Enabled := True;

    ChDir(Dir);
    map := MSE.ObjLoad.Load(sOpenDialog1.FileName);
    sComboBox1.Clear;
    for i := 0 to map.ObjCount-1 do
      sComboBox1.Items.Add(map.Obj[i].Name);

    sComboBox1.ItemIndex := 0;
    sComboBox1Change(sComboBox1);
  end;
end;

procedure TForm2.sButton2Click(Sender: TObject);
begin
  map.Obj[sComboBox1.ItemIndex].TextureName := '';
  map.Obj[sComboBox1.ItemIndex].Texture := 0;
  LabelTextureName.Caption := '';
end;

procedure TForm2.sButton3Click(Sender: TObject);
var
  i: Integer;
begin
  if OpenDialogTexture.Execute then
  begin
    ChDir(Dir);

    if Edit2.Text<>'' then
    begin
      for i := 0 to map.ObjCount-1 do
      if (Pos(Edit2.Text, map.Obj[i].Name)>0)
         and (((i mod 2=0) and (Random(2)=1)) or not CheckBox2.Checked)
      then
      begin
        map.Obj[i].TextureName := OpenDialogTexture.FileName;
        Delete(map.Obj[i].TextureName, Pos(Dir, map.Obj[i].TextureName), Length(Dir));

        map.Obj[i].Texture := MSE.Texture.Load(map.Obj[sComboBox1.ItemIndex].TextureName);
      end;
    end
    else
    begin
      map.Obj[sComboBox1.ItemIndex].TextureName := OpenDialogTexture.FileName;
      Delete(map.Obj[sComboBox1.ItemIndex].TextureName, Pos(Dir, map.Obj[sComboBox1.ItemIndex].TextureName), Length(Dir));
      map.Obj[sComboBox1.ItemIndex].Texture := MSE.Texture.Load(map.Obj[sComboBox1.ItemIndex].TextureName);
    end;

    LabelTextureName.Caption := map.Obj[sComboBox1.ItemIndex].TextureName;
  end;
end;

procedure TForm2.Button1Click(Sender: TObject);
var
  i, j: Integer;
begin
  for i := 0 to map.ObjCount-1 do
  begin
    for j:=0 to map.Obj[i].VertexCount-1 do begin//�����
      map.Obj[i].vertex[j].x := map.Obj[i].vertex[j].x*StrToFloat(Edit1.Text);
      map.Obj[i].vertex[j].y := map.Obj[i].vertex[j].y*StrToFloat(Edit1.Text);
      map.Obj[i].vertex[j].z := map.Obj[i].vertex[j].z*StrToFloat(Edit1.Text);
    end;
  end;
end;

procedure TForm2.CheckBox1Click(Sender: TObject);
begin
  MSE.Texture.Clamp := CheckBox1.Checked;
end;

procedure TForm2.CheckBoxTransparencyClick(Sender: TObject);
begin
  if CheckBoxTransparency.Checked then
    map.Obj[sComboBox1.ItemIndex].Transparency := 1
  else
    map.Obj[sComboBox1.ItemIndex].Transparency := 0;
end;

procedure TForm2.sComboBox1Change(Sender: TObject);
begin
  LabelTextureName.Caption := map.Obj[sComboBox1.ItemIndex].TextureName;

  TrackBarRed.Position := Round(map.Obj[sComboBox1.ItemIndex].Color.R*255);
  TrackBarGreen.Position := Round(map.Obj[sComboBox1.ItemIndex].Color.G*255);
  TrackBarBlue.Position := Round(map.Obj[sComboBox1.ItemIndex].Color.B*255);
  TrackBarAlpha.Position := map.Obj[sComboBox1.ItemIndex].Alpha;

  if map.Obj[sComboBox1.ItemIndex].Phys=0 then
    sRadioButton0.Checked := True;
  if map.Obj[sComboBox1.ItemIndex].Phys=1 then
    sRadioButton1.Checked := True;
  if map.Obj[sComboBox1.ItemIndex].Phys=2 then
    sRadioButton2.Checked := True;

  if map.Obj[sComboBox1.ItemIndex].Transparency=0 then
    CheckBoxTransparency.Checked := False
  else
    CheckBoxTransparency.Checked := True;

  Form1.Caption := IntToStr(map.Obj[sComboBox1.ItemIndex].Phys);
end;

procedure TForm2.sRadioButton0Click(Sender: TObject);
begin
  map.Obj[sComboBox1.ItemIndex].Phys := 0;
end;

procedure TForm2.sRadioButton1Click(Sender: TObject);
begin
  map.Obj[sComboBox1.ItemIndex].Phys := 2;
end;

procedure TForm2.sRadioButton2Click(Sender: TObject);
begin
  map.Obj[sComboBox1.ItemIndex].Phys := 1;
end;

procedure TForm2.TrackBarCameraSpeedChange(Sender: TObject);
begin
  sLabel6.Caption := '�������� ������='+IntToStr(TrackBarCameraSpeed.Position);
end;

procedure TForm2.TrackBarAlphaChange(Sender: TObject);
begin
  map.Obj[sComboBox1.ItemIndex].Alpha := TrackBarAlpha.Position;
  sLabel2.Caption := '������������='+IntToStr(TrackBarAlpha.Position);
end;

procedure TForm2.ToolButton1Click(Sender: TObject);
begin
  SaveObj(MapFileName);
end;

procedure TForm2.ToolButton2Click(Sender: TObject);
begin
  if sSaveDialog1.Execute then
  begin
    SaveObj(sSaveDialog1.FileName);
  end;
end;

procedure TForm2.TrackBarBlueChange(Sender: TObject);
var
  i: Integer;
begin
  if Edit2.Text<>'' then
  begin
    for i := 0 to map.ObjCount-1 do
    if Pos(Edit2.Text, map.Obj[i].Name)>0 then

    begin
      map.Obj[i].Color.B := TrackBarBlue.Position/255;
    end;
  end
  else
    map.Obj[sComboBox1.ItemIndex].Color.B := TrackBarBlue.Position/255;

  sLabel5.Caption := 'B='+IntToStr(TrackBarBlue.Position);
end;

procedure TForm2.TrackBarGreenChange(Sender: TObject);
var
  i: Integer;
begin
  if Edit2.Text<>'' then
  begin
    for i := 0 to map.ObjCount-1 do
    if Pos(Edit2.Text, map.Obj[i].Name)>0 then

    begin
      map.Obj[i].Color.G := TrackBarGreen.Position/255;
    end;
  end
  else
    map.Obj[sComboBox1.ItemIndex].Color.G := TrackBarGreen.Position/255;
  sLabel4.Caption := 'G='+IntToStr(TrackBarGreen.Position);
end;

procedure TForm2.TrackBarRedChange(Sender: TObject);
var
  i: Integer;
begin
  if Edit2.Text<>'' then
  begin
    for i := 0 to map.ObjCount-1 do
    if Pos(Edit2.Text, map.Obj[i].Name)>0 then

    begin
      map.Obj[i].Color.R := TrackBarRed.Position/255;
    end;
  end
  else
    map.Obj[sComboBox1.ItemIndex].Color.R := TrackBarRed.Position/255;
  sLabel3.Caption := 'R='+IntToStr(TrackBarRed.Position);
end;

end.
