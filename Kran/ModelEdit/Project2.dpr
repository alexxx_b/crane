program Project2;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  Obj in 'Obj.pas',
  E_Engine in '..\..\Engine\E_Engine.pas',
  E_Window in '..\..\Engine\E_Window.pas',
  E_OGL in '..\..\Engine\OGL\E_OGL.pas',
  OpenGL20 in '..\..\Engine\OGL\OpenGL20.pas',
  E_Math in '..\..\Engine\E_Math.pas',
  E_Type in '..\..\Engine\E_Type.pas',
  E_Log in '..\..\Engine\E_Log.pas',
  E_Draw in '..\..\Engine\E_Draw.pas',
  E_Texture in '..\..\Engine\Texture\E_Texture.pas',
  E_System in '..\..\Engine\E_System.pas',
  E_ObjLoad in '..\..\Engine\E_ObjLoad.pas',
  moduleloader in '..\..\Engine\Phys\moduleloader.pas',
  ODEImport in '..\..\Engine\Phys\ODEImport.pas',
  E_Phys in '..\..\Engine\Phys\E_Phys.pas',
  E_Input in '..\..\Engine\E_Input.pas',
  E_Camera in '..\..\Engine\E_Camera.pas',
  E_FrustumCulling in '..\..\Engine\E_FrustumCulling.pas',
  E_SMD in '..\..\Engine\SMD\E_SMD.pas',
  modelki in '..\..\Engine\SMD\modelki.pas',
  _strman in '..\..\Engine\SMD\_strman.pas',
  MatrixMaths in '..\..\Engine\SMD\MatrixMaths.pas',
  openil in '..\..\Engine\Texture\DevIL\openil.pas',
  openilu in '..\..\Engine\Texture\DevIL\openilu.pas',
  E_Map in '..\..\Engine\E_Map.pas',
  E_Volume in '..\..\Engine\E_Volume.pas',
  E_Kran in '..\Unit\E_Kran.pas',
  E_Shaders in '..\..\Engine\E_Shaders.pas',
  E_Particles in '..\..\Engine\E_Particles.pas',
  E_Sound in '..\..\Engine\Sound\E_Sound.pas',
  OSTypes in '..\..\Engine\Sound\OSTypes.pas',
  VorbisFile in '..\..\Engine\Sound\VorbisFile.pas',
  Codec in '..\..\Engine\Sound\Codec.pas',
  OpenAL in '..\..\Engine\Sound\OpenAL.pas',
  Alut in '..\..\Engine\Sound\Alut.pas',
  AlTypes in '..\..\Engine\Sound\AlTypes.pas',
  AlcTypes in '..\..\Engine\Sound\AlcTypes.pas',
  Alc in '..\..\Engine\Sound\Alc.pas',
  Ogg in '..\..\Engine\Sound\Ogg.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
