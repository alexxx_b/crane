program Project1;

uses
  Windows,
  MMSystem,
  SysUtils,
  Math,
  E_Engine in '..\Engine\E_Engine.pas',
  E_Window in '..\Engine\E_Window.pas',
  E_OGL in '..\Engine\OGL\E_OGL.pas',
  OpenGL20 in '..\Engine\OGL\OpenGL20.pas',
  E_Math in '..\Engine\E_Math.pas',
  E_Type in '..\Engine\E_Type.pas',
  E_Log in '..\Engine\E_Log.pas',
  E_Draw in '..\Engine\E_Draw.pas',
  E_Texture in '..\Engine\Texture\E_Texture.pas',
  E_System in '..\Engine\E_System.pas',
  E_ObjLoad in '..\Engine\E_ObjLoad.pas',
  moduleloader in '..\Engine\Phys\moduleloader.pas',
  ODEImport in '..\Engine\Phys\ODEImport.pas',
  E_Phys in '..\Engine\Phys\E_Phys.pas',
  E_Input in '..\Engine\E_Input.pas',
  E_Camera in '..\Engine\E_Camera.pas',
  E_FrustumCulling in '..\Engine\E_FrustumCulling.pas',
  E_SMD in '..\Engine\SMD\E_SMD.pas',
  modelki in '..\Engine\SMD\modelki.pas',
  _strman in '..\Engine\SMD\_strman.pas',
  MatrixMaths in '..\Engine\SMD\MatrixMaths.pas',
  openil in '..\Engine\Texture\DevIL\openil.pas',
  openilu in '..\Engine\Texture\DevIL\openilu.pas',
  E_Map in '..\Engine\E_Map.pas',
  E_Volume in '..\Engine\E_Volume.pas',
  E_Kran in 'Unit\E_Kran.pas',
  E_Shaders in '..\Engine\E_Shaders.pas',
  E_Particles in '..\Engine\E_Particles.pas',
  E_Sound in '..\Engine\Sound\E_Sound.pas',
  OSTypes in '..\Engine\Sound\OSTypes.pas',
  VorbisFile in '..\Engine\Sound\VorbisFile.pas',
  Codec in '..\Engine\Sound\Codec.pas',
  OpenAL in '..\Engine\Sound\OpenAL.pas',
  Alut in '..\Engine\Sound\Alut.pas',
  AlTypes in '..\Engine\Sound\AlTypes.pas',
  AlcTypes in '..\Engine\Sound\AlcTypes.pas',
  Alc in '..\Engine\Sound\Alc.pas',
  Ogg in '..\Engine\Sound\Ogg.pas',
  E_Perlin in '..\Engine\E_Perlin.pas',
  Ini in 'Unit\Ini.pas';

//type
//  TVector4f = array [0..3] of Single;
type
  Tproc = procedure;
  Tp = record
    a: Integer;
    Pos: TVector3f;
  end;

const
  n=1;
  c_m=0;
  ShadowMapSize=1024;
  RTTSize=1024;
  p_c = 50;

  PS: TVector4fa = (1, 0, 0, 0);
  PT: TVector4fa = (0, 1, 0, 0);
  PR: TVector4fa = (0, 0, 1, 0);
  PQ: TVector4fa = (0, 0, 0, 1);

var
  CameraType: Integer;
  TimerId1, TimerId2: UINT;

var
  GameClose: Boolean=False;
  GamePause: Boolean=False;
  matrix: TMatrix;
  Distance: Single=8;
var
  Load: Boolean=False;
  FontTexture: TFont;
  Sky: TSkyBox;
  ShadowMap, FBOTex, BoxTex, ZastavkaTex: TTex;
  ShadowFBO, FBO: Cardinal;
  Light: TVector4fa = (100, 100, 0, 0);
  ShadowEnable: Boolean=False;
  PosV, RotV, PosKran: TVector3f;
  Smoke: TSmoke;
  Help: Boolean=False;
  p: array[1..p_c] of Tp;
  ip: Integer=0;
  i, j, k: Integer;
  Music: array[1..10] of TSound;
  B: array[1..10, 1..10, 1..10] of TBody;
  G: array[1..10, 1..10, 1..10] of PdxGeom;

procedure MsgShow(fail: string; Proc: Pointer);
var
  f: TextFile;
  s, s2: string;
  i: Integer;
begin
  s := '';
  AssignFile(f, fail);
  Reset(f);
  repeat
    Readln(f, s2);
    s := s+s2;
  until Eof(f);

  ShowWindow(MSE.Window.p1, SW_SHOW);
  ShowWindow(MSE.Window.b1, SW_SHOW);

  SetWindowText(MSE.Window.m1, s);

  MSE.Window.MsgProc := Proc;

  MSE.Window.CursorVisible := True;

  for i:=0 to 10 do
  begin
    Sleep(10);
    MSE.RenderProc;
  end;
  MSE.Pause := True;
end;

procedure Quit;
begin
  MSE.Close := True;
end;

procedure Render;
var
  i, j, k, c: Integer;
  sx, sy, sz, cx, cy, cz: Double;
  zzz: Single;
begin
  if not Load then
  begin
    glViewport(0, 0, MSE.Window.Width, MSE.Window.Height);
    MSE.OGL.Clear(True, True, True);
    MSE.OGL.Set2DRender;

    glEnable(GL_TEXTURE_2D);
    MSE.Texture.Use(ZastavkaTex);
    glBegin(GL_QUADS);
      glTexCoord2f(0, 0); glVertex2f(-1, -0.8);
      glTexCoord2f(1, 0); glVertex2f( 1, -0.8);
      glTexCoord2f(1, 1); glVertex2f( 1,  0.8);
      glTexCoord2f(0, 1); glVertex2f(-1,  0.8);
    glEnd;

    MSE.OGL.SwapBuffer;
    Exit;
  end;

  if MSE.Pause then Exit;
  if MSE.Phys.Blocking then Exit;

  PosV := MSE.Phys.GetBodyPos(Cars.Phys[0]);
  PosKran := PosV;
  //150, 100, -100
  Light[0] := (PosV.X/10)+100;
  Light[1] := (PosV.Y/10)+50;
  Light[2] := (PosV.Z/10)+100;

  glViewport(0, 0, MSE.Window.Width, MSE.Window.Height);
  MSE.OGL.Clear(True, True, True);

  MSE.OGL.Set3DRender;
  glLoadIdentity;

  MSE.OGL.PushMatrix;
    glLoadIdentity;
    MSE.OGL.RotateX(MSE.Camera.GetRot.X);
    MSE.OGL.RotateY(MSE.Camera.GetRot.Y);
    MSE.OGL.RotateZ(MSE.Camera.GetRot.Z);
    MSE.Draw.SkyBox(Sky);
    MSE.Light.Position(1, -Light[0], Light[1], -Light[2]);
  MSE.OGL.PopMatrix;

  if CameraType<>3 then
  begin
    MSE.OGL.RotateX(MSE.Camera.GetRot.X);
    MSE.OGL.RotateY(MSE.Camera.GetRot.Y);
    MSE.OGL.RotateZ(MSE.Camera.GetRot.Z);
  end;

  MSE.OGL.RotateY(180);
  if CameraType=1 then
  begin
    Posv := MSE.Phys.GetBodyPos(Cars.Camera_Body[1]);
  end;
  if CameraType=2 then
  begin
    Posv := MSE.Phys.GetBodyPos(Cars.Camera_Body[2]);
  end;
  if CameraType=3 then
  begin
    MSE.OGL.Translate(0, 0, Distance);
    MSE.OGL.RotateX(-MSE.Camera.GetRot.X);
    MSE.OGL.RotateY(MSE.Camera.GetRot.Y);
    MSE.OGL.RotateZ(-MSE.Camera.GetRot.Z);
  end;

  MSE.OGL.Translate(-PosV.X, -PosV.Y, -PosV.Z);

  if ShadowEnable=False then
  MSE.ObjLoad.ShadowEnable := True;

  glEnable(GL_TEXTURE_2D);

  {for i:=1 to 3 do
  for j:=1 to 3 do
  for k:=1 to 3 do
  begin
    MSE.OGL.PushMatrix;
      MSE.Math.SetTransform(MSE.Phys.GetObjectOrientation(B[i, j, k]));
      MSE.Draw.Box(1, 1, 1, BoxTex);
    MSE.OGL.PopMatrix;
  end;}

  Kran.Draw;
  MSE.Map.Draw(map);
  MSE.ObjLoad.Render;

  MSE.Light.Position(1, 0, 3000, -2000);


  MSE.OGL.Set2DRender;
//  MSE.Draw.DrawText(FontTexture, IntToStr(MSE.OGL.GetFPS), 0, 0);
//  MSE.Draw.DrawText(FontTexture, IntToStr(MSE.ObjLoad.TriangleRender), 0, 1);
  //MSE.Draw.DrawText(FontTexture, FloatToStr(Cars.v), 0, 1);

  MSE.Draw.DrawText(FontTexture, 'F12    | ��������/������ ������', 0, 0);
  if Help then
  begin
    MSE.Draw.DrawText(FontTexture, 'F1     | ������ �� ����� ��������', 0, 1);
    MSE.Draw.DrawText(FontTexture, 'F2     | ������ �� ����� ��������� ������', 0, 2);
    MSE.Draw.DrawText(FontTexture, 'F3     | ������ � ��������� ���������', 0, 3);
    MSE.Draw.DrawText(FontTexture, '�������| �������� ���������', 0, 4);
    MSE.Draw.DrawText(FontTexture, 'Space  | ������', 0, 5);
    MSE.Draw.DrawText(FontTexture, 'Q/A    | ������� ���������', 0, 6);
    MSE.Draw.DrawText(FontTexture, 'W/S    | �������� ������ ������/�����', 0, 7);
    MSE.Draw.DrawText(FontTexture, 'E/D    | ��������/��������� ������', 0, 8);
    MSE.Draw.DrawText(FontTexture, 'R/F    | ��������/��������� �����', 0, 9);
    MSE.Draw.DrawText(FontTexture, 'Enter  | ������� �����', 0, 10);
    MSE.Draw.DrawText(FontTexture, '+/-    | ���������� � ������� ������� ���', 0, 11);
    MSE.Draw.DrawText(FontTexture, 'Scroll | ����������� ������', 0, 12);
    MSE.Draw.DrawText(FontTexture, 'Esc    | �����', 0, 13);
  end;

  MSE.OGL.SwapBuffer;

  for j:=0 to map.ModelCount - 1 do
  begin
    for i:=0 to 15 do
      map.Model[j].Matrix[i] := 0;
    map.Model[j].Matrix[0] := 1;
    map.Model[j].Matrix[5] := 1;
    map.Model[j].Matrix[10] := 1;
    map.Model[j].Matrix[15] := 1;

    for k:=0 to map.Model[j].ObjCount - 1 do
    if map.Model[j].Obj[k].Body<>nil then
    begin
      map.Model[j].Pos := MSE.Phys.GetBodyPos(map.Model[j].Obj[k].Body);
      map.Model[j].Obj[k].BBox.X := MSE.Phys.GetBodyPos(map.Model[j].Obj[k].Body).X;
      map.Model[j].Obj[k].BBox.Y := MSE.Phys.GetBodyPos(map.Model[j].Obj[k].Body).Y;
      map.Model[j].Obj[k].BBox.Z := MSE.Phys.GetBodyPos(map.Model[j].Obj[k].Body).Z;
    end;
  end;

  //������� ���������
  if MSE.Phys.Finish<0 then
  begin
    MSE.Pause := True;
    MsgShow('map\'+IniVar.Map+'_2.txt', @Quit);
  end;
end;

procedure UpdateProc;
var
  MouseWheel, j: Integer;
  x, y, z: Single;
begin
  if MSE.Pause then Exit;

  //repeat until (MSE.Phys.Blocking=False);
  //if MSE.Phys.Blocking then Exit;

  //�����
  if MSE.Input.KeyDown(27) then MSE.Close := True;
  if MSE.Input.KeyDown(123) then begin Help := not Help; MSE.Input.KeySetValue(123, False) end;

  //����� ������
  if MSE.Input.KeyDown(112) then CameraType := 1;
  if MSE.Input.KeyDown(113) then CameraType := 2;
  if MSE.Input.KeyDown(114) then CameraType := 3;

  if GamePause=False then
  begin
    MSE.Camera.InputMouse;
    Kran.KranUpDate;
    MSE.Particles.Update;
    MSE.Sound.Update;

    //�����
    MouseWheel := MSE.Input.GetMouseWheel;
    if (MouseWheel=1) and (Distance>5) then Distance := Distance-Distance/10;
    if (MouseWheel=-1) and (Distance<30) then Distance := Distance+Distance/10;

    //��� �� ��� �����

    if Random(5)=1 then
    begin
      for j:=1 to p_c do
      begin
        //if Random(100)=1 then
          p[j].a := p[j].a-1;
        if p[j].a<0 then
          p[j].a := 0;
      end;
    end;
  end;
end;

procedure Update(uTimerID, uMessage: UINT;dwUser, dw1, dw2: DWORD) stdcall;
var
  Vel: PdVector3;
  Rot: PdMatrix3;
  Rot2: TdMatrix3;
  R: TdMatrix3;
begin
  if MSE.Pause or MSE.Phys.Blocking then Exit;

  Vel := dBodyGetAngularVel(Cars.Hook_Body);
  Vel[0] := Vel[0]*0.0001;
  Vel[1] := Vel[1]*0.0001;
  Vel[2] := Vel[2]*0.0001;
  dBodySetAngularVel(Cars.Hook_Body, Vel[0], Vel[1], Vel[2]);

  if (Cars.Hook_Joint2<>0) then
  begin
    Vel := dBodyGetAngularVel(Cars.p1);
    Vel[0] := Vel[0]*0.1;
    Vel[1] := Vel[1]*0.1;
    Vel[2] := Vel[2]*0.1;
    dBodySetAngularVel(Cars.p1, Vel[0], Vel[1], Vel[2]);
  end;
      //dRSetIdentity(Rot2);
      //dRFromAxisAndAngle(Rot2, 0, 1, 0, Rad*2*pi);
      Rot2 := (dBodyGetRotation(Cars.Hook_Body)^);
      Rot2[11] := dBodyGetRotation(Cars.Phys[7])[11];
      dBodySetRotation(Cars.Hook_Body, Rot2);

  Cars.Hook_Contact := False;

  MSE.Phys.Update;
end;


begin
  MSE := TEngine.Create(0);

  MSE.Log.Enable := True;
  MSE.Log.New('Log');

  IniLoad;

  MSE.Phys.Init(0, -9.8, 0, IniVar.Phys);

  MSE.Log.Write('<b>������������� ����������:</b> '+MSE.System.GetCPUVendor, Msg_Good);
  MSE.Log.Write('<b>�������� ����������:</b> '+MSE.System.GetCPUName, Msg_Good);
  MSE.Log.Write('<b>�������� ������� ����������:</b> '+IntToStr(MSE.System.GetCPUFrequency)+'���', Msg_Good);
  MSE.Log.Write('<b>������������ �������:</b> '+MSE.System.GetOSName, Msg_Good);
  MSE.Log.Write('<b>����� ���������� ������:</b> '+IntToStr(Round(MSE.System.GetTotalPhys/(1024*1024)))+'Mb', Msg_Good);

  MSE.Window.CursorVisible := False;

  if (IniVar.AA>0) then
    MSE.OGL.SetAntiAliasingValue(IniVar.AA);

  MSE.Window.Start;
  MSE.Window.SetCaption('��������');

  MSE.OGL.VSync(IniVar.VSync);
  MSE.OGL.SetCullFace(CF_Back);
  MSE.System.SetMode(IniVar.VideoMode);
  MSE.Window.Style := WS_POPUP;
  MSE.Window.Width := MSE.System.ModeGetW(IniVar.VideoMode);
  MSE.Window.Height := MSE.System.ModeGetH(IniVar.VideoMode);

  MSE.Window.p1 := CreateWindow('STATIC', '', WS_CHILD or WS_VISIBLE or BS_PUSHBUTTON,
        Round(MSE.Window.Width/2)-150, Round(MSE.Window.Height/2)-200, 300, 410, MSE.Window.Handle, 0, hInstance, nil);

  MSE.Window.m1 := CreateWindow('edit', 'Press me', WS_CHILD or WS_VISIBLE or BS_PUSHBUTTON or ES_MULTILINE or WS_VSCROLL,
        10, 10, 280, 350, MSE.Window.p1, 0, hInstance, nil);

  MSE.Window.b1 := CreateWindow('button', 'OK', WS_VISIBLE or WS_CHILD,
        Round(MSE.Window.Width/2)-140, 370+Round(MSE.Window.Height/2)-200, 280, 25, MSE.Window.Handle, 100, 0, nil);

  ShowWindow(MSE.Window.p1, SW_HIDE);
  ShowWindow(MSE.Window.b1, SW_HIDE);

//  ZastavkaTex := MSE.Texture.Load('Texture\zastavka.jpg'); Render;

  MSE.Pause := False;

  MSE.SetRenderProc(@Render);
  MSE.SetUpdateProc(@UpdateProc);

  MSE.Light.Enable(1);
  MSE.Light.Position(1, 0, 3000, -2000);
  MSE.Light.Diffuse(1, 1, 1, 1, 1);
  MSE.Light.Ambient(0.4, 0.4, 0.4, 1);

  //�����
  //MSE.Draw.FogVisible(True);
  //MSE.Draw.SetFog(1, 1, 1, 1, 1000);

  MSE.Camera.Enable(True);
  MSE.Camera.FOV := 70;
  MSE.Camera.zFar := 0.1;
  MSE.Camera.zNear := 5000;
  MSE.Camera.Sens := 0.02;
  MSE.Camera.SetSpeed(30000);

  FontTexture := MSE.Draw.CreateFont('Texture\font.tga', 0.05, 0.05);
  Sky := MSE.Draw.CreateSkyBox('Texture\SkyBox\sky');
  BoxTex := MSE.Texture.Load('Texture\concrete_3.dds');


  //�������
  //MSE.Shaders.Shader1 := MSE.Shaders.Load('Shaders\V.txt', 'Shaders\F.txt');

  MSE.Camera.Init;
  MSE.Draw.Init;
  Kran := TKran.Create;

  map := MSE.Map.Load('map\'+IniVar.Map+'.map');
  MSE.Phys.AddMap(map);

  CameraType := 3;

  MSE.Input.SetCursorPosCenter;

  MSE.Sound.Init('dll\OpenAl32.dll', nil);

  MSE.Pause := False;
  MSE.Phys.Blocking := False;
  //MsgShow('map\'+IniVar.Map+'_1.txt', nil);

  {for i:=1 to 3 do
  for j:=1 to 3 do
  for k:=1 to 3 do
  begin
    MSE.Phys.CreateBox(20+i*2, 1.5+k*2, -20+j*2, 1, 1, 1, 0.001, B[i, j, k], G[i, j, k]);
  end;}

  MSE.Camera.Rot.X := 30;
  MSE.Camera.Rot.y := 30;
  MSE.Camera.Pos.X := 1000;


  Load := True;
  TimerID1 := timeSetEvent(Round(1000/IniVar.Phys), 1, @Update, 0, TIME_PERIODIC);

  MsgShow('map\'+IniVar.Map+'_1.txt', nil);

  MSE.Run;

  //�������
  timeKillEvent(TimerID1); {}
end.
