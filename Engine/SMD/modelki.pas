unit modelki;

interface

uses
  _strman;

type
  //TVolume2 = TVolume;

  TPovorot = array[0..2] of Single;
  TPeremesh = array[0..2] of Single;

  TVershina = record
    Sustav: Integer;
    Koordinata: array[0..2] of Single;
    Normal: array[0..2] of Single;
    TexKoordinata: array[0..1] of Single;
  end;

  TPoligon = record
    Tex: String;
    TeksGL: Integer;
    Vershini: array[0..2] of TVershina;
  end;

  TKadrAnimacii = record
    Povorot: array of TPovorot;
    Peremesh: array of TPeremesh;
  end;

  TModelka = object
    Svaz: array of Integer;
    OdinKadr: TKadrAnimacii;
    Poligon: array of TPoligon;
    procedure Load(Name: String);
  end;

  TAnimacia = object
    Kadri: array of TKadrAnimacii;
    procedure Load(Name: String);
  end;

implementation

uses E_Log, SysUtils, E_Engine;

procedure TModelka.Load(Name: String);
var
  f: TextFile;
  s: String;
  i,j: Integer;
begin
  AssignFile(f, Name);
  Reset(f);

  repeat
    Readln(f, s);
  until s='nodes';

  i := 0;
  SetLength(Svaz, i);

  repeat
    Readln(f, s);
    if s <> 'end' then
    begin
      SetLength(Svaz, i+1);
      Svaz[i] := StrToInt(StringWordGet(s, '"', 3));
      Inc(i);
    end;
  until s='end';

  repeat
    Readln(f, s);
  until s='time 0';

  i := 0;
  SetLength(OdinKadr.Peremesh, i);
  SetLength(OdinKadr.Povorot, i);

  repeat
    Readln(f, s);
    if s <> 'end' then
    begin
      SetLength(OdinKadr.Peremesh, i+1);
      SetLength(OdinKadr.Povorot, i+1);

      OdinKadr.Peremesh[i][0] := StrToFloatDef(StringWordGet(Trim(s), ' ', 2), 0);
      OdinKadr.Peremesh[i][1] := StrToFloatDef(StringWordGet(Trim(s), ' ', 3), 0);
      OdinKadr.Peremesh[i][2] := StrToFloatDef(StringWordGet(Trim(s), ' ', 4), 0);
      OdinKadr.Povorot[i][0] := StrToFloatDef(StringWordGet(Trim(s), ' ', 5), 0);
      OdinKadr.Povorot[i][1] := StrToFloatDef(StringWordGet(Trim(s), ' ', 6), 0);
      OdinKadr.Povorot[i][2] := StrToFloatDef(StringWordGet(Trim(s), ' ', 7), 0);
      Inc(i);
    end;
  until s='end';

  repeat
    Readln(f, s);
  until s='triangles';

  i:=0;
  SetLength(Poligon, i);

  repeat
    Readln(f, s);

    if s<>'end' then
    begin
      SetLength(Poligon, i+1);
      Delete(Name, Pos('\', Name)+1, length(name));
      Poligon[i].TeksGL := MSE.Texture.Load(name+s);
      for j:=0 to 2 do
      begin
        Readln(f, poligon[i].vershini[j].sustav,
                  poligon[i].vershini[j].koordinata[0],
                  poligon[i].vershini[j].koordinata[1],
                  poligon[i].vershini[j].koordinata[2],
                  poligon[i].vershini[j].normal[0],
                  poligon[i].vershini[j].normal[1],
                  poligon[i].vershini[j].normal[2],
                  poligon[i].vershini[j].TexKoordinata[0],
                  poligon[i].vershini[j].TexKoordinata[1]);
      end;
      Inc(i);
    end;
  until s='end';

  CloseFile(f);
end;

procedure TAnimacia.Load(Name: String);
var
  f: TextFile;
  s: String;
  i, j: Integer;
begin
  AssignFile(f, Name);
  Reset(f);

  repeat
    Readln(f, s);
  until s='skeleton';

  repeat
    Readln(f, s);
    if s='end' then
      Break;

    if StringWordGet (s, ' ', 1) = 'time' then
    begin
      i := 0;
      Inc(j);
      SetLength(Kadri, j);
    end
    else
    begin
      Inc(i);
      SetLength(Kadri[j-1].Povorot, i);
      SetLength(Kadri[j-1].Peremesh, i);

      with Kadri[j-1] do
      begin
        Peremesh[i-1][0] := StrToFloatDef(StringWordGet(Trim(s), ' ', 2), 0);
        Peremesh[i-1][1] := StrToFloatDef(StringWordGet(Trim(s), ' ', 3), 0);
        Peremesh[i-1][2] := StrToFloatDef(StringWordGet(Trim(s), ' ', 4), 0);
        Povorot[i-1][0] := StrToFloatDef(StringWordGet(Trim(s), ' ', 5), 0);
        Povorot[i-1][1] := StrToFloatDef(StringWordGet(Trim(s), ' ', 6), 0);
        Povorot[i-1][2] := StrToFloatDef(StringWordGet(Trim(s), ' ', 7), 0);
      end;
  end;

  until False;

  CloseFile(f);
end;

end.
