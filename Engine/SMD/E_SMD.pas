unit E_SMD;

interface

uses
  windows, OpenGL20, modelki, MatrixMaths, Math, SysUtils, E_Type;

type
  TLoadSMD = class
    public
      function Load(Way: string): TSMDModel;//��������� SMD
      procedure Draw(var Model: TSMDModel);//���������� SMD
      procedure AddAnim(var Model: TSMDModel; Way: string);
      procedure SetAnimNumber(var Model: TSMDModel; Value: Integer);//������������� ����� ��������� ������������� ��������
      function GetAnimNumber(Model: TSMDModel): Integer;//����� ����� ������� ������������� ��������
      function GetMaxAnimeCount(Model: TSMDModel): Integer;//����� ������� ����� �������� � ��� ������
      procedure SetScale(var Model: TSMDModel; Value: Single);
      function GetScale(Model: TSMDModel): Single;
    private
  end;

implementation

uses E_Texture, E_Camera, E_Engine;

function TLoadSMD.Load(Way: string): TSMDModel;
var
  i, j, k: Integer;
  t: Cardinal;
begin
with Result do
begin
  AnimCount := 0;
  AnimNumber := -1;
  Scale := 1;

  Kadr := 0;
  TimeKadr := 0;
  FormatSettings.DecimalSeparator:='.';

  Model.Load(Way);

  setlength(svernuli,length(Model.poligon));

  for i:=0 to length(Model.poligon)-1 do
    svernuli[i] := Model.poligon[i];


  SetLength(Sustavi, Length(Model.Svaz));

  for i:=0 to length(Model.svaz)-1 do
  begin
    sustavi[i].tochka[0]:=0.0;
    sustavi[i].tochka[1]:=0.0;
    sustavi[i].tochka[2]:=0.0;

    sustavi[i].otnosit:=Xrot(-Model.odinkadr.povorot[i][0]);
    CConcatMatrix(Yrot(-Model.odinkadr.povorot[i][1]), sustavi[i].otnosit);
    CConcatMatrix(Zrot(-Model.odinkadr.povorot[i][2]), sustavi[i].otnosit);

    sustavi[i].otnosit[3,0]:=Model.odinkadr.peremesh[i][0];
    sustavi[i].otnosit[3,1]:=Model.odinkadr.peremesh[i][1];
    sustavi[i].otnosit[3,2]:=Model.odinkadr.peremesh[i][2];

    if Model.svaz[i]<>-1 then
      sustavi[i].absolut:=ConcatMatrix(sustavi[Model.svaz[i]].absolut,
                                       sustavi[i].otnosit)
    else
      sustavi[i].absolut:=sustavi[i].otnosit;
  end;

  for i:=0 to length(svernuli)-1 do
    for j:=0 to 2 do
      InvApplyMatrix(Vertex(svernuli[i].vershini[j].koordinata),
                     sustavi[ svernuli[i].vershini[j].sustav ].absolut);

  for i:=0 to length(Model.poligon)-1 do
    Model.poligon[i]:=svernuli[i];
end;
end;

procedure TLoadSMD.AddAnim(var Model: TSMDModel; Way: string);
begin
with Model do
begin
  Inc(AnimCount);
  SetLength(Anim, AnimCount);
  Anim[AnimCount-1].Load(Way);
end;
end;

procedure TLoadSMD.Draw(var Model: TSMDModel);
procedure RenderModel;
var
  i, j: integer;
  Last: Integer;
begin
with Model do
begin
  glRotatef(-90, 1, 0, 0);

    for i:=0 to length(Model.poligon)-1 do
    begin
  MSE.Texture.Use(Model.Poligon[i].TeksGL);

  glBegin(GL_TRIANGLES);
      for j:=0 to 2 do
    begin
      glTexCoord2f(Model.poligon[i].vershini[j].TexKoordinata[0],
                   Model.poligon[i].vershini[j].TexKoordinata[1]);
      glNormal3fv(@Model.poligon[i].vershini[j].normal);
      glVertex3f(Model.poligon[i].vershini[j].koordinata[0],
                 Model.poligon[i].vershini[j].koordinata[1],
                 Model.poligon[i].vershini[j].koordinata[2]);
    end;
  glEnd;
    end;
end;
end;

var
  i, j, n: integer;
  db     : array [0..2] of Real;
  a      : array [0..2] of Real;
  b      : array [0..2] of Real;
begin
with Model do
begin
glPushMatrix;
glScalef(Scale, Scale, Scale);
  if AnimNumber=-1 then
  begin
    RenderModel;
    glPopMatrix;
    Exit;
  end;

  SetLength(Sustavi, Length(Model.Svaz));

  for i:=0 to length(Model.svaz)-1 do
  begin

  for n:=0 to 2 do
   begin
    //��������� � �������� ����, ����� �� ��� ���� ��� x,y � z
    Anim[AnimNumber].kadri[kadr].povorot[i][n] := Anim[AnimNumber].kadri[kadr].povorot[i][n];
    a[n] := Anim[AnimNumber].kadri[kadr].povorot[i][n];
    b[n] := Anim[AnimNumber].kadri[sledKadr].povorot[i][n];
    // �������� �� ���� ���� ���������� �� ������, ��� � ���
   if abs(radtodeg(b[n]) - radtodeg(a[n])) <= abs(radtodeg(b[n]) - 360 - radtodeg(a[n])) then
     begin
       if abs(radtodeg(b[n]) - radtodeg(a[n])) <= abs(radtodeg(b[n]) + 360 - radtodeg(a[n])) then
        db[n]:= b[n]-a[n]
        else
        db[n]:= (b[n] + DegToRad (360)) - a[n];
     end
    else
       db[n]:= (b[n] - DegToRad (360)) - a[n];
    db[n]:=0;
   end;

    sustavi[i].tochka[0]:=0.0;
    sustavi[i].tochka[1]:=0.0;
    sustavi[i].tochka[2]:=0.0;


    TempPovorot[0]:=a[0]+db[0]*timeKadr;
    TempPovorot[1]:=a[1]+db[1]*timeKadr;
    TempPovorot[2]:=a[2]+db[2]*timeKadr;

    TempPeremesh[0]:=anim[1].kadri[kadr].peremesh[i][0]+(anim[1].kadri[sledKadr].peremesh[i][0]-anim[1].kadri[kadr].peremesh[i][0])*timeKadr;
    TempPeremesh[1]:=anim[1].kadri[kadr].peremesh[i][1]+(anim[1].kadri[sledKadr].peremesh[i][1]-anim[1].kadri[kadr].peremesh[i][1])*timeKadr;
    TempPeremesh[2]:=anim[1].kadri[kadr].peremesh[i][2]+(anim[1].kadri[sledKadr].peremesh[i][2]-anim[1].kadri[kadr].peremesh[i][2])*timeKadr;

    sustavi[i].otnosit:=Xrot(-TempPovorot[0]);
    CConcatMatrix(Yrot(-TempPovorot[1]), sustavi[i].otnosit);
    CConcatMatrix(Zrot(-TempPovorot[2]), sustavi[i].otnosit);

    sustavi[i].otnosit[3,0]:=TempPeremesh[0];
    sustavi[i].otnosit[3,1]:=TempPeremesh[1];
    sustavi[i].otnosit[3,2]:=TempPeremesh[2];

       if Model.svaz[i]<>-1 then
         sustavi[i].absolut:=ConcatMatrix(sustavi[Model.svaz[i]].absolut,
                                          sustavi[i].otnosit)
         else
        sustavi[i].absolut:=sustavi[i].otnosit;

   end;

  for i:=0 to length(Model.svaz)-1 do
   ApplyMatrix(sustavi[i].tochka,
               sustavi[i].absolut);


  for i:=0 to length(Model.poligon)-1 do
    Model.poligon[i]:=svernuli[i];

  for i:=0 to length(svernuli)-1 do
   for j:=0 to 2 do
 ApplyMatrix(Vertex(Model.poligon[i].vershini[j].koordinata),
             sustavi[Model.poligon[i].vershini[j].sustav ].absolut);

     RenderModel;

     time1 := time2;
     time2 := getTickCount;

     timeKadr := timeKadr+1*(time2-time1)/40;

     if timeKadr>=1 then
      begin
       inc(kadr);
       timeKadr := 0;
      end;

     if kadr>length(anim[1].kadri)-1 then kadr:=0;

     if kadr=length(anim[1].kadri)-1 then sledKadr:=0
      else
       sledKadr:=kadr+1;
glPopMatrix;
end;
end;

procedure TLoadSMD.SetAnimNumber(var Model: TSMDModel; Value: Integer);
begin
  Model.AnimNumber := Value-1;
end;

function TLoadSMD.GetMaxAnimeCount(Model: TSMDModel): Integer;
begin
  Result := Model.AnimNumber;
end;

function TLoadSMD.GetAnimNumber(Model: TSMDModel): Integer;
begin
  Result := Model.AnimNumber;
end;

procedure TLoadSMD.SetScale(var Model: TSMDModel; Value: Single);
begin
  Model.Scale := Value;
end;

function TLoadSMD.GetScale(Model: TSMDModel): Single;
begin
  Result := Model.Scale;
end;


end.
