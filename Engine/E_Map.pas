unit E_Map;

interface

uses
  Windows, OpenGL20, E_Texture, E_Type;

type
  TMapLoad = class
    private
    public
      constructor Create;
      function Load(Path: string): TMap;
      procedure Draw(Map: TMap);
  end;

implementation

uses E_Engine;

constructor TMapLoad.Create;
begin

end;

function TMapLoad.Load(Path: string): TMap;
var
  f: file of Byte;
  i, j: Integer;
  ps: PAnsiChar;
  NameSize: Word;
  s: string;
begin
with Result do
begin
  AssignFile(f, Path);
  Reset(f);

  BlockRead(f, ModelCount, SizeOf(ModelCount));
  SetLength(Model, ModelCount);

  for i := 0 to ModelCount-1 do
  begin
    BlockRead(f, NameSize, SizeOf(NameSize));
    NameSize := NameSize+1;
    GetMem(ps, NameSize);
    BlockRead(f, ps^, NameSize);
    s := AnsiString(ps);
    FreeMem(ps);

    Model[i] := MSE.ObjLoad.Load(s);
    Model[i].Path := s;
  end;

  //�������
  for i := 0 to ModelCount-1 do
    BlockRead(f, Model[i].Pos, SizeOf(Model[i].Pos));

  //����������
  for i := 0 to ModelCount-1 do
    BlockRead(f, Model[i].Rot, SizeOf(Model[i].Rot));

  //����
  for i := 0 to ModelCount-1 do
    BlockRead(f, Model[i].TypeObj, SizeOf(Model[i].TypeObj));

  for i := 0 to ModelCount-1 do
  begin
    for j := 0 to Model[i].ObjCount-1 do
    begin
      Model[i].Obj[j].Pos := Model[i].Pos;
      Model[i].Obj[j].Rot := Model[i].Rot;

      Model[i].Obj[j].Pos := Model[i].Pos;
      Model[i].Obj[j].Rot := Model[i].Rot;
    end;
  end;

  CloseFile(f);
end;
end;

procedure TMapLoad.Draw(Map: TMap);
var
  i: Integer;
begin
  for i:=0 to Map.ModelCount - 1 do
  begin
    MSE.ObjLoad.Draw(Map.Model[i]);
  end;
end;


end.
