﻿unit E_Shaders;

interface

uses Windows, OpenGL20, Classes, SysUtils, E_Type;

type
  TShaders = class
    public
      function Load(PathVS, PathFS: string): TShader;
      procedure Use(Shader: TShader);
    private
  end;

var
  u_angle: GLuint;   // адрес uniform-переменной
  a_phase: GLuint;   // адрес attribute-переменной
  u_texture: GLuint; // адрес uniform-переменной

implementation

uses E_Engine;

function FileLoad(Path: string): string;
var
  f: TextFile;
  s: string;
begin
  AssignFile(f, Path);
  Reset(f);

  Result := '';
  repeat
    Readln(f, s);
    Result := Result+s;
  until Eof(f);

  CloseFile(f);
end;

function Error(Handle: TShader; Param: DWORD): Boolean;
var
  Status : Integer;
begin
  glGetObjectParameterivARB(Handle, Param, @Status);
  Result := Status <> 1;
end;

function TShaders.Load(PathVS, PathFS: string): TShader;
var
  vs: GLhandleARB;   // вершинный шейдер
  fs: GLhandleARB;   // фрагментный шейдер
  src: PAnsiChar; // PGLcharARB = PChar;
begin
  glEnable(GL_VERTEX_SHADER_ARB);
  glEnable(GL_FRAGMENT_SHADER_ARB);

  vs := glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
  fs := glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);

  src := PAnsiChar(AnsiString(FileLoad(PathVS)));
  glShaderSourceARB(vs, 1, @src, nil);

  src := PAnsiChar(AnsiString(FileLoad(PathFS)));
  glShaderSourceARB(fs, 1, @src, nil);

  glCompileShaderARB(vs);
  glCompileShaderARB(fs);

  Result := glCreateProgramObjectARB;

  glAttachObjectARB(Result, vs);
  glAttachObjectARB(Result, fs);

  glLinkProgramARB(Result);
  if Error(Result, GL_OBJECT_LINK_STATUS_ARB) then
    MSE.Log.Write('Error while compiling', Msg_Error);
end;


procedure TShaders.Use(Shader: TShader);
begin
  glUseProgramObjectARB(Shader);
end;

end.
