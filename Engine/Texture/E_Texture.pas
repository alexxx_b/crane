(*
|> ������ �������� ������� �������
|> �������� ������� �������� ������, ����������� �� ��������� ������ ���� � ���� ��������
*)

unit E_Texture;

interface

uses Windows, OpenGL20, E_Type, OpenIL, OpenILU;

type
  TTextureInfo = record
    Index: Cardinal;
    Width,Height: Integer;
    Detail, FileType: Byte;
  end;
  
  TManager = Record
    Work : Boolean;
    Name : String;
    Value: Cardinal;
  end;

  TTexture = class
    private
      MaxCount: Integer;
      LastTexture: TTex;

      RenderTTWidth, RenderTTHeight: Integer;
      RenderTex: TTex;
      Manager: array of TManager;
      _Clamp: Boolean;
      //function CreateRenderToTexture(Width, Height : integer): TTex;
    procedure SetClamp(const Value: Boolean);
    public
      constructor Create;
      destructor Destroy;
      function Load(Files: String): Cardinal;
      procedure Free(Tex: Cardinal);
      procedure Use(Tex: TTex);
      procedure ScreenShot(Dir: PChar);

      //procedure BeginRenderToTexture(Width, Height: Integer);
      //function EndRenderToTexture: TTex;

      function CreateShadowMap(Size : Integer): TTex;

      function FBOShadowInit(Size: Integer): Cardinal;
      procedure FBOShadowSet(FBO: Cardinal; Tex: TTex);

      function FBOInit(Size: Integer): Cardinal;
      procedure FBOSet(FBO: Cardinal; Tex: TTex);

      property Clamp: Boolean read _Clamp write SetClamp;
  end;

var
  k: Integer=0;

implementation

uses E_Log, SysUtils, E_Draw, E_Engine;

constructor TTexture.Create;
begin
  MaxCount := -1;
  Clamp := False;

  ilInit;
  iluInit;

  ilSetInteger(IL_ORIGIN_MODE, IL_ORIGIN_LOWER_LEFT);
  ilEnable(IL_ORIGIN_SET);
end;

destructor TTexture.Destroy;
var
  i: Integer;
begin
  for i:=0 to MaxCount do
  begin
    if Manager[i].Work then
    begin
      MSE.Log.Write('�������� "'+Manager[i].Name+'" ������� �������', 0);
      Free(Manager[i].Value);
    end;
  end;
end;

procedure SwapRGB(Data: Pointer; Size: Integer);
asm
  mov ebx, eax
  mov ecx, size

@@loop :
  mov al,[ebx+0]
  mov ah,[ebx+2]
  mov [ebx+2],al
  mov [ebx+0],ah
  add ebx,3
  dec ecx
  jnz @@loop
end;

procedure SwapRGBA(data : Pointer; size : Integer);
asm
  mov ebx, eax
  mov ecx, edx

@@loop :
  mov al,[ebx+0]
  mov ah,[ebx+2]
  mov    [ebx+2],al
  mov    [ebx+0],ah

  add ebx,4
  dec ecx
  jnz @@loop
end;

function TTexture.Load(Files: string): Cardinal;
var
  i: Integer;
  M: Boolean;
  Err, format: Integer;
  StrError: string;
  Tipa, Tipa2: Integer;// ��� �������� ������
  TempRam, CopyData: Pointer;
  bpp, Width, Height, Size: Integer;
  pVertex: ^Byte;
begin
  Result := 0;

  Files := Trim(Files);
  if Files='' then Exit;

  if FileExists(Files) = False then
  begin
    MSE.Log.Write('������! ��������� "'+Files+'" �� �������', Msg_Error);
    Result := 0;
    Exit;
  end;

  Result := 0;
  M := False;

  //���� ����� � �������� ���� ��������� ������ �� ������ �������� ������ �� ��
  for i:=0 to MaxCount do
  begin
    if Manager[i].Name = Files then
    begin
      Result := Manager[i].Value;
      Exit;
    end;
  end;

  ilLoad(IL_TYPE_UNKNOWN, PAnsiChar(AnsiString(Files)));

  if (ilGetError <> 0) then
  begin
    MSE.Log.Write('������! ��������� "'+Files+'" �� ���������', Msg_Error);
    Result := 0;
    Exit;
  end;

  Width := ilGetInteger(IL_IMAGE_WIDTH);
  Height := ilGetInteger(IL_IMAGE_HEIGHT);
  bpp := ilGetInteger(IL_IMAGE_BYTES_PER_PIXEL);

  Size := Width*Height*bpp;
  getMem(TempRam, Size);

  format := ilGetInteger(IL_IMAGE_FORMAT);
  if(format = IL_BGR) or (format = IL_BGRA) then
    iluSwapColours();


  CopyData := ilGetData();

  Move(copyData^, tempRAM^, Size);

  case (bpp) of
    1: Tipa := GL_RGB8;
    3: Tipa := GL_RGB;
    4: Tipa := GL_RGBA;
  end;

  Tipa2 := $80E0;

  glGenTextures(1, @Result);
  glBindTexture(GL_TEXTURE_2D, Result);
  if MSE.Texture.Clamp then
  begin
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  end;

  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

  gluBuild2DMipmaps(GL_TEXTURE_2D, Tipa, Width,  Height, Tipa, GL_UNSIGNED_BYTE, tempRAM);

  freeMem(tempRAM);

  //�������� � �������� ������ ��������� ���������� ��������
  for i:=0 to MaxCount do
  begin
    if Manager[i].Work = False then
    begin
      Manager[i].Work  := True;
      Manager[i].Name  := Files;
      Manager[i].Value := Result;

      M := True;
    end;
  end;

  //���� ����� ��������� ������ ��������� ������, �� ������ ���
  if M = False then
  begin
    MaxCount := MaxCount+1;
    SetLength(Manager, MaxCount+1);

    Manager[MaxCount].Work  := True;
    Manager[MaxCount].Name  := files;
    Manager[MaxCount].Value := Result;
  end;

  if Result<>0 then
    MSE.Log.Write('�������� "'+Files+'" ������� ���������', 0)
end;

procedure TTexture.Free(Tex: Cardinal);
begin
  glDeleteTextures(1, @PGLuint(Tex));
  MSE.Log.Write('�������� "'+MSE.Math.IntToStr(Tex)+'" ������� �������', 0);
end;

procedure TTexture.Use(Tex: TTex);
begin
  if Tex <> LastTexture then
  begin
    LastTexture := Tex;
    glBindTexture(GL_TEXTURE_2D, Tex);
  end;  
end;

{------------------------------------------------------------------------------}

{function TextureCreate(C, B: Integer; Width, Height: Integer; Data: Pointer): DWORD;
begin
  glGenTextures(1, @Result);
  glBindTexture(GL_TEXTURE_2D, Result);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

  if MSE.Texture.Clamp then
  begin
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  end;

  //glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  //glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16);

  gluBuild2DMipmaps(GL_TEXTURE_2D, C, Width, Height, B, GL_UNSIGNED_BYTE, Data);
end; }

procedure TTexture.ScreenShot(Dir: PChar);
var
  Image: Pointer;
  Bi: BITMAPINFOHEADER;
  Bf: BITMAPFILEHEADER;
  f: file of Byte;
begin
  Inc(k);
  AssignFile(f, 'Scrin/123'+IntToStr(k)+'.bmp');
  Rewrite(f);
  GetMem(Image, MSE.Window.Width*MSE.Window.Height*3);

  glReadPixels(0, 0, MSE.Window.Width, MSE.Window.Height, GL_BGR, GL_UNSIGNED_BYTE, Image);

  Bf.bfType := $4D42;
  Bf.bfSize := sizeof(bf)+sizeof(bi)+MSE.Window.Width*MSE.Window.Height*3;
  Bf.bfOffBits := sizeof(bf)+sizeof(bi);
  Bi.biSize := sizeof(bi);
  Bi.biWidth := MSE.Window.Width;
  Bi.biHeight := MSE.Window.Height;
  Bi.biPlanes := 1;
  Bi.biBitCount := 24;
  bi.biCompression:=BI_RGB;
  Bi.biSizeImage := MSE.Window.Width*MSE.Window.Height*3;

  BlockWrite(f, Bf, SizeOf(Bf));
  BlockWrite(f, Bi, SizeOf(Bi));
  BlockWrite(f, Image^, MSE.Window.Width*MSE.Window.Height*3);

  FreeMem(Image, MSE.Window.Width*MSE.Window.Height*3);

  CloseFile(f);
end;

{function TTexture.CreateRenderToTexture(Width, Height : integer): TTex;
var
  pData: Pointer;
begin
  GetMem(pData, Width*Height*3);
//  Result := TextureCreate(3, GL_RGB, Width, Height, pData);
end;

procedure TTexture.BeginRenderToTexture(Width, Height: Integer);
begin
  MSE.OGL.Clear(True, True, False);

  if (RenderTTWidth <> Width) or (RenderTTHeight <> Height) then
  begin
    Free(RenderTex);
    RenderTTWidth  := Width;
    RenderTTHeight := Height;
    RenderTex    := CreateRenderToTexture(Width, Height);
  end;

  glViewport(0, 0, RenderTTWidth, RenderTTHeight);

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  //gluPerspective(CCamera.FOV, RenderTTWidth/RenderTTHeight, CCamera.zNear, CCamera.zFar);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
end;

function TTexture.EndRenderToTexture: TTex;
begin
  glBindTexture(GL_TEXTURE_2D, RenderTex);

  glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 0, 0, RenderTTWidth, RenderTTHeight);

//  glViewport(0, 0, CWindow.GetWidth, CWindow.GetHeight);

  Result := RenderTex;
end;}

function TTexture.CreateShadowMap(Size : Integer): TTex;
begin
  glClearDepth(1.0);

  glGenTextures(1, @Result);
  glBindTexture(GL_TEXTURE_2D, Result);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, Size, Size, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, nil);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

  //����� ������ ���������� ��� �������:
  glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE_ARB, GL_LUMINANCE);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE_ARB, GL_COMPARE_R_TO_TEXTURE_ARB);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC_ARB, GL_LEQUAL);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FAIL_VALUE_ARB, 0.5);

  glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
  glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
  glTexGeni(GL_R, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);
  glTexGeni(GL_Q, GL_TEXTURE_GEN_MODE, GL_EYE_LINEAR);

  glPolygonOffset(2, 2);
end;

procedure TTexture.SetClamp(const Value: Boolean);
begin
  _Clamp := Value;
end;

function TTexture.FBOShadowInit(Size: Integer): Cardinal;
begin
  glGenFramebuffersEXT(1, @Result);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, Result);
  glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT24_ARB, Size, Size);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
end;

procedure TTexture.FBOShadowSet(FBO: Cardinal; Tex: TTex);
begin
  if FBO<>0 then
  begin
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, FBO);
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, Tex, 0);

    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    if glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT) <> GL_FRAMEBUFFER_COMPLETE_EXT then
    begin
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
      MSE.Log.Write('������! FBO �� ������ � �������������', Msg_Error);
    end;
  end
  else
  begin
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, 0, 0);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  end;
end;

function TTexture.FBOInit(Size: Integer): Cardinal;
var
  RenderBuffer: Cardinal;
begin
  glGenFramebuffersEXT(1, @Result);
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, Result);

  glGenRenderbuffersEXT(1, @RenderBuffer);
  glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, RenderBuffer);
  glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT24_ARB, Size, Size);
  glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, RenderBuffer);

  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
end;

procedure TTexture.FBOSet(FBO: Cardinal; Tex: TTex);
begin
  if FBO<>0 then
  begin
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, FBO);
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, Tex, 0);

    //glDrawBuffer(GL_FRONT);
    //glReadBuffer(GL_FRONT);

    if glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT) <> GL_FRAMEBUFFER_COMPLETE_EXT then
    begin
      glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
      MSE.Log.Write('������! FBO �� ������ � �������������', Msg_Error);
    end;
  end
  else
  begin
    glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, 0, 0);
    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  end;
end;


end.
