unit MSERender;

interface

uses
  SysUtils, Classes, MMSystem, Windows;

type
  TMSERender = class(TComponent)
  private
    TimerId: UINT;
    FInterval: Integer;
    FOnTimer: TNotifyEvent;
    procedure SetOnTimer(Value: TNotifyEvent);
  protected
  public
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    procedure Loaded; override;
  published
    property Interval: Integer read FInterval write FInterval;
    property OnTimer: TNotifyEvent read FOnTimer write SetOnTimer;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('MSE', [TMSERender]);
end;

{ TMSERender }

constructor TMSERender.Create(AOwner: TComponent);
begin
  inherited;
  Interval := 40;
end;

destructor TMSERender.Destroy;
begin

  inherited;
end;

procedure TMSERender.SetOnTimer(Value: TNotifyEvent) stdcall;
begin
//
end;

procedure TMSERender.Loaded;
begin
  inherited;
//  TimerID := timeSetEvent(Interval, 1, @SetOnTime, 0, TIME_PERIODIC);
end;


end.
