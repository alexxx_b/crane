unit E_Window;

interface

uses
  Windows, Messages, SysUtils;

type
  TWindow = class
    private
      _Width, _Height, FLeft, FTop: Integer;
      _Style: DWORD;
      _Handle: HWND;
      _CursorVisible: Boolean;
    procedure SetWidth(const Value: Integer);
    procedure SetHeight(const Value: Integer);
    procedure SetStyle(const Value: DWORD);
    procedure SetCursorVisible(const Value: Boolean);
    public
      p1, m1, b1: HWND;
      MsgProc: procedure;

      procedure Start;
      procedure Free;

      procedure SetCaption(Caption: PWideChar);

      property Handle: HWND read _Handle;
      property Width: Integer read _Width write SetWidth;
      property Height: Integer read _Height write SetHeight;
      property Left: Integer read FLeft write FLeft;
      property Top: Integer read FTop write FTop;
      property Style: DWORD read _Style write SetStyle;
      property CursorVisible: Boolean read _CursorVisible write SetCursorVisible;
  end;

implementation

uses E_Engine, E_OGL, E_Type, E_Math, OpenGL20{, E_Maps};

function WndProc(HWND: HWND; Msg: Cardinal; wParam: Integer; lParam: Integer): Integer; stdcall;
var
  HDC1: HDC;
  Paint: TPaintStruct;
  i: Integer;
begin
  Result := 0;

  case Msg of
    WM_PAINT:
    begin
      HDC1 := BeginPaint(MSE.Window.p1, Paint);
        RoundRect(HDC1, 0, 0, 300, 410, 0, 0);
      EndPaint(MSE.Window.p1, Paint);
    end;

    WM_COMMAND:
    begin
      if (LOWORD(wParam)=100) then
      begin
        MSE.Window.CursorVisible := False;
        MSE.Pause := False;

        ShowWindow(MSE.Window.p1, SW_HIDE);
        ShowWindow(MSE.Window.b1, SW_HIDE);
        if @MSE.Window.MsgProc<>nil then
          MSE.Window.MsgProc;
      end;
    end;

    WM_KEYDOWN://������� �� ������� ���������
    begin
      MSE.Input.Key[wParam] := True;
    end;

    WM_KEYUP://���������� ������� ����������
    begin
      MSE.Input.Key[wParam] := False;
    end;

    WM_LBUTTONDOWN://������ �� ����� ������ ����
    begin
      MSE.Input.MClickL := True;
    end;

    WM_LBUTTONUP://�������� ����� ������ ����
    begin
      MSE.Input.MClickL := False;
    end;

    WM_RBUTTONDOWN://������ �� ������ ������ ����
    begin
      MSE.Input.MClickR := True;
    end;
    WM_RBUTTONUP://��������� ������ ������ ����
    begin
      MSE.Input.MClickR := False;
    end;

    WM_MOUSEWHEEL://������ ������� ����
    begin
      MSE.Input.MouseWheel := SmallInt(HIWORD(wParam)) div 120;
    end;

    WM_KILLFOCUS://���� �������� �����
    begin
      MSE.Pause := True;
    end;

    WM_SETFOCUS://������� ������ �� ����
    begin
      MSE.Pause := False;
    end;

    WM_DESTROY:
    begin
      MSE.Close := True;
      PostQuitMessage(0);
      Exit;
    end;

    MSG_LOAD:
    begin
      //Map.LoadVBO;
    end;

    MSG_SAVE:
    begin
      //Map.DeleteVBO;
    end;

    MSG_SHOW_CURSOR:
    begin
      MSE.Window.CursorVisible := True;
    end;

    MSG_HIDE_CURSOR:
    begin
      MSE.Window.CursorVisible := False;
    end;
  end;

  Result := DefWindowProc(HWND, Msg, wParam, lParam);
end;

procedure TWindow.Start;
var
  wc: WNDCLASSEX;
  PixelFormat: Integer;
begin
if MSE.Handle=0 then
begin
  _Width := 500;
  _Height := 500;
  FLeft := 0;
  _Style := WS_POPUP;

  ZeroMemory(@wc, SizeOf(wc));
  with wc do
  begin
    cbSize        := SizeOf(WNDCLASSEX);
    style         := CS_HREDRAW or CS_VREDRAW or CS_OWNDC;
    lpfnWndProc   := @WndProc;
    cbClsExtra    := 0;
    cbWndExtra    := 0;
    hInstance     := 0;
    hCursor       := LoadCursor(0, IDC_ARROW);
    hbrBackground := HBRUSH(GetStockObject(WHITE_BRUSH));
    lpszMenuName  := nil;
    lpszClassName := 'MSE';
    hIcon := LoadIcon(hInstance, IDI_APPLICATION);
  end;

  if RegisterClassEx(wc) = 0 then
  begin
    MSE.Log.Write('������! ����������� ������ ����', Msg_Error);
    Exit;
  end
  else
    MSE.Log.Write('����������� ������ ����', Msg_Good);

  _Handle := CreateWindowEx(WS_OVERLAPPED,   //WS_EX_APPWINDOW
                           PWideChar('MSE'),
                           PWideChar('����� ������'),
                           _Style,
                           FLeft, FTop,
                           Width, Height,
                           0, 0, 0, nil );

  if Handle = 0 then
    MSE.Log.Write('������! ���� �� ��������', Msg_Error)
  else
    MSE.Log.Write('���� ��������, ������ ����='+MSE.Math.IntToStr(Handle), Msg_Good);
end
else
begin
  _Handle := MSE.Handle;
  _Width := 1000;
  _Height := 700;
end;

//****************************************************************************//
  MSE.OGL.Init(0);

  glGetIntegerv(GL_MAX_SAMPLES_EXT, @MSE.OGL.Info.MaxSamples);
  MSE.OGL.Info.Renderer   := glGetString(GL_RENDERER);
  MSE.OGL.Info.Vendor     := glGetString(GL_VENDOR);
  MSE.OGL.Info.Version    := glGetString(GL_VERSION);
  MSE.OGL.Info.Extensions := glGetString(GL_EXTENSIONS);
  glGetIntegerv(GL_MAX_TEXTURE_SIZE, @MSE.OGL.Info.MaxTextureSize);

  MSE.Log.Write('����� �����: '+MSE.OGL.Info.Renderer, Msg_Good);
  MSE.Log.Write('������������� ����� �����: '+MSE.OGL.Info.Vendor, Msg_Good);
  MSE.Log.Write('OpenGL ������: '+MSE.OGL.Info.Version, Msg_Good);
  MSE.Log.Write('���������� ����� �����: '+MSE.OGL.Info.Extensions, Msg_Good);
  MSE.Log.Write('������������ ��������������: '+MSE.Math.IntToStr(MSE.OGL.Info.MaxSamples), Msg_Good);
  MSE.Log.Write('������������ ������ �������: '+MSE.Math.IntToStr(MSE.OGL.Info.MaxTextureSize), Msg_Good);

  if MSE.OGL.AAValue <> 0 then
  begin
    PixelFormat := MSE.OGL.InitMultiSample(MSE.OGL.AAValue);

    Free;
    _Handle := CreateWindowEx(WS_EX_APPWINDOW,
                             PWideChar('MSE'),
                             PWideChar('����� ������'),
                             _Style,
                             Left, Top,
                             Width, Height,
                             0, 0, 0, nil );
    if Handle = 0 then
      MSE.Log.Write('������! ���� �� ��������', Msg_Error)
    else
      MSE.Log.Write('���� ��������, ������ ����='+MSE.Math.IntToStr(Handle), Msg_Good);

    MSE.OGL.Init(PixelFormat);
    MSE.Close := False;
  end;
//****************************************************************************//
  ShowWindow(Handle, SW_SHOW);
  UpdateWindow(Handle);
end;

procedure TWindow.Free;
begin
  DestroyWindow(Handle);
end;

//������������� ����� ��������� ����
procedure TWindow.SetCaption(Caption: PWideChar);
begin
  SetWindowText(_Handle, Caption);
end;

procedure TWindow.SetWidth(const Value: Integer);
var
  Rect: TRect;
begin
  _Width := Value;

  Rect.Left   := FLeft;
  Rect.Top    := FTop;
  Rect.Right  := Width;
  Rect.Bottom := Height;
  AdjustWindowRect(Rect, Style, False);
  SetWindowPos(Handle, 0, FLeft, FTop, Rect.Right - Rect.Left, Rect.Bottom - Rect.Top, SWP_FRAMECHANGED or SWP_NOOWNERZORDER);

  //glViewport(0, 0, _Width, _Height);
end;

procedure TWindow.SetHeight(const Value: Integer);
var
  Rect  : TRect;
begin
  _Height := Value;

  Rect.Left   := FLeft;
  Rect.Top    := FTop;
  Rect.Right  := Width;
  Rect.Bottom := Height;
  AdjustWindowRect(Rect, Style, False);
  SetWindowPos(Handle, 0, FLeft, FTop, Rect.Right - Rect.Left, Rect.Bottom - Rect.Top, SWP_FRAMECHANGED or SWP_NOOWNERZORDER);

  //glViewport(0, 0, Width, Height);
end;

procedure TWindow.SetStyle(const Value: DWORD);
var
  Rect  : TRect;
  dm: DevMode;
  hr: HRESULT;
begin
  _Style := WS_POPUP;

  SetWindowLong(Handle, GWL_STYLE, _Style or WS_VISIBLE);

  Rect.Left   := FLeft;
  Rect.Top    := FTop;
  Rect.Right  := Width;
  Rect.Bottom := Height;

  AdjustWindowRect(Rect, _Style, False);
  SetWindowPos(Handle, 0, FLeft, FTop, Rect.Right - Rect.Left, Rect.Bottom - Rect.Top, SWP_FRAMECHANGED or SWP_NOOWNERZORDER);
end;

procedure TWindow.SetCursorVisible(const Value: Boolean);
begin
  _CursorVisible := Value;
  ShowCursor(_CursorVisible);
end;


end.
