unit E_ObjLoad;

interface

uses Windows, OpenGL20, E_Texture, E_Type;

type
  TObjLoad = class
    private
      _TriangleRender: Integer;
    public
      Stack: array of TModel;
      StackCount: Integer;
      ShadowEnable: Boolean;
      Redaktor: Boolean;
      FrustOff: Boolean;
      constructor Create;
      function Load(ObjName: String): TModel;
      procedure Draw(Model: TModel);
      procedure Free(Model: TModel);
      procedure Render;
      property TriangleRender: Integer read _TriangleRender;
  end;

implementation

uses SysUtils, E_Log, E_Engine, ODEImport, E_Phys, Math, E_Math;

constructor TObjLoad.Create;
begin
  StackCount := -1;
  ShadowEnable := False;
  Redaktor := False;
  FrustOff := False;
end;

function TObjLoad.Load(ObjName: string): TModel;
type
  TVert = record
    c, n : TVector3f;
    t: TVector2f;
  end;
var
  f: file of Byte;
  f2: TextFile;
  i, j, k: Integer;
  min, max, mino, maxo: TVector3f;
  {}
  ii, jj, idx : Integer;
  c, n: TVector3f;
  t: TVector2f;
  v: array of TVert;
  Count : Integer;
  NameSize: Word;
  s: PAnsiChar;
begin
with Result do
begin
  Pos.X := 0;
  Pos.Y := 0;
  Pos.Z := 0;

  Rot.X := 0;
  Rot.Y := 0;
  Rot.Z := 0;

  Result.Pos.X := 0;
  Result.Pos.Y := 0;
  Result.Pos.Z := 0;

  maxo.X := -100000;
  maxo.Y := -100000;
  maxo.Z := -100000;
  mino.X :=  100000;
  mino.Y :=  100000;
  mino.Z :=  100000;

  AssignFile(f, ObjName);
  Reset(f);

  BlockRead(f, ObjCount, SizeOf(ObjCount));
  SetLength(Obj, ObjCount);//������ ������ ��� �������

  for j:=0 to Result.ObjCount-1 do
  begin
    //�������� ����� ������
    BlockRead(f, NameSize, SizeOf(NameSize));
    NameSize := NameSize+1;
    GetMem(s, NameSize);
    BlockRead(f, s^, NameSize);
    Obj[j].Name := AnsiString(s);
    FreeMem(s);

    //��������
    BlockRead(f, NameSize, SizeOf(NameSize));
    NameSize := NameSize+1;
    GetMem(s, NameSize);
    BlockRead(f, s^, NameSize);
    Obj[j].TextureName := AnsiString(s);
    FreeMem(s);
    Obj[j].Texture := MSE.Texture.Load(Obj[j].TextureName);

    //�����
    BlockRead(f, Obj[j].Alpha, SizeOf(Obj[j].Alpha));

    //����
    BlockRead(f, Obj[j].Color, SizeOf(Obj[j].Color));

    //������
    BlockRead(f, Obj[j].Phys, SizeOf(Obj[j].Phys));

    //������������
    BlockRead(f, Obj[j].Transparency, SizeOf(Obj[j].Transparency));

    BlockRead(f, Obj[j].VertexCount, SizeOf(Obj[j].VertexCount));
    BlockRead(f, Obj[j].FacesCount, SizeOf(Obj[j].FacesCount));
    BlockRead(f, Obj[j].TexCoordCount, SizeOf(Obj[j].TexCoordCount));

    GetMem(Obj[j].Vertex, SizeOf(TVector3f)*Obj[j].VertexCount);
    GetMem(Obj[j].Normal, SizeOf(TVector3f)*3*3*Obj[j].FacesCount);
    GetMem(Obj[j].TexCoord, SizeOf(TVector2f)*Obj[j].TexCoordCount);
    GetMem(Obj[j].TF, SizeOf(TFaces_w)*Obj[j].FacesCount);
    GetMem(Obj[j].VF, SizeOf(TFaces_w)*Obj[j].FacesCount);

    BlockRead(f, Obj[j].Vertex[0],   SizeOf(Obj[j].Vertex)*3*(Obj[j].VertexCount));
    BlockRead(f, Obj[j].VF[0],       SizeOf(Obj[j].VF[0])*Obj[j].FacesCount);
    BlockRead(f, Obj[j].Normal[0],   SizeOf(Obj[j].Normal)*3*3*(Obj[j].FacesCount));
    BlockRead(f, Obj[j].TexCoord[0], SizeOf(Obj[j].TexCoord)*2*(Obj[j].TexCoordCount));
    BlockRead(f, Obj[j].TF[0],       SizeOf(Obj[j].TF[0])*Obj[j].FacesCount);
{----------------------------------}//����������� ���������� BBox
  max.X := -100000;
  max.Y := -100000;
  max.Z := -100000;
  min.X :=  100000;
  min.Y :=  100000;
  min.Z :=  100000;

  Obj[j].Body := nil;

  for i:=0 to Result.Obj[j].VertexCount-1 do begin
    if Obj[j].vertex[i].X > max.X then max.X := Obj[j].vertex[i].X;
    if Obj[j].vertex[i].Y > max.Y then max.Y := Obj[j].vertex[i].Y;
    if Obj[j].vertex[i].Z > max.Z then max.Z := Obj[j].vertex[i].Z;

    if Obj[j].vertex[i].X < min.X then min.X := Obj[j].vertex[i].X;
    if Obj[j].vertex[i].Y < min.Y then min.Y := Obj[j].vertex[i].Y;
    if Obj[j].vertex[i].Z < min.Z then min.Z := Obj[j].vertex[i].Z;
  end;

  if min.X < mino.X then mino.X := min.X;
  if min.Y < mino.Y then mino.Y := min.Y;
  if min.Z < mino.Z then mino.Z := min.Z;

  if max.X > maxo.X then maxo.X := max.X;
  if max.Y > maxo.Y then maxo.Y := max.Y;
  if max.Z > maxo.Z then maxo.Z := max.Z;

  Obj[j].BBox.X := (max.X+min.X)/2;
  Obj[j].BBox.Y := (max.Y+min.Y)/2;
  Obj[j].BBox.Z := (max.Z+min.Z)/2;

  Obj[j].BBox.W := (max.X-min.X);
  Obj[j].BBox.H := (max.Y-min.Y);
  Obj[j].BBox.D := (max.Z-min.Z);
{-----------------------------------------------------}//����������� �� ������
  //ObjNew[j] := Obj[j];
{if (World<>'obj\Kran\Body.obj') and (World<>'obj\Kran\Tower.obj') then
begin
  ObjNew[j].VertexCount := Obj[j].VertexCount;
  ObjNew[j].FacesCount := Obj[j].FacesCount;
  ObjNew[j].TexCoordCount := Obj[j].TexCoordCount;

  GetMem(ObjNew[j].Vertex, SizeOf(TVector3f)*ObjNew[j].VertexCount);
  GetMem(ObjNew[j].Normal, SizeOf(TVector3f)*ObjNew[j].VertexCount);
  GetMem(ObjNew[j].TexCoord, SizeOf(TVector2f)*ObjNew[j].TexCoordCount);
  GetMem(ObjNew[j].TF, SizeOf(TFaces)*ObjNew[j].FacesCount);
  GetMem(ObjNew[j].VF, SizeOf(TFaces)*ObjNew[j].FacesCount);

  Count := 0;
  SetLength(v, ObjNew[j].VertexCount);
  for ii := 0 to ObjNew[j].FacesCount-1 do
  for jj := 0 to 2 do
  begin
    c := Obj[j].Vertex[Obj[j].VF[ii][jj]];
    n := Obj[j].Normal[Obj[j].VF[ii][jj]];
    t := Obj[j].TexCoord[Obj[j].TF[ii][jj]];

    idx := 0;
    while idx<Count do
      if (v[idx].c.X = c.X) and (v[idx].c.Y = c.Y) and (v[idx].c.Z = c.Z) and
         (v[idx].n.X = n.X) and (v[idx].n.Y = n.Y) and (v[idx].n.Z = n.Z) and
         (v[idx].t.X = t.X) and (v[idx].t.Y = t.Y)
      then
        Break
      else
        Inc(idx);
    if idx = Count then
    begin
      if Length(v) = Count then
      begin
        SetLength(v, Length(v)+128);
      end;

      v[Count].c := c;
      v[Count].n := n;
      v[Count].t := t;
      Inc(Count);
    end;

    ObjNew[j].VF[ii][jj] := idx;
  end;

  ObjNew[j].VertexCount := Count;
  ObjNew[j].TexCoordCount := Count;
  Obj[j].NewVertexCount := Count;
  Obj[j].NewTexCoordCount := Count;

  GetMem(ObjNew[j].Vertex, SizeOf(TVector3f)*ObjNew[j].VertexCount);
  GetMem(ObjNew[j].Normal, SizeOf(TVector3f)*ObjNew[j].VertexCount);
  GetMem(ObjNew[j].TexCoord, SizeOf(TVector3f)*ObjNew[j].VertexCount);

  for ii := 0 to ObjNew[j].VertexCount-1 do
  begin
    ObjNew[j].Vertex[ii] := v[ii].c;
    ObjNew[j].Normal[ii] := v[ii].n;
    ObjNew[j].TexCoord[ii] := v[ii].t;
  end;
end;
{---------------------������ ���������� ������----------------}
if ObjName='floor' then
begin

end;
{-------------------------------------------------------------}//������� ����
Obj[j].List := glGenLists(1);

glNewList(Obj[j].List, GL_COMPILE);
  glBegin(GL_TRIANGLES);
  ii := 0;
  for i:=0 to Result.Obj[j].FacesCount-1 do
  begin

    for jj := 0 to 2 do
    begin
      glNormal3fv(@Obj[j].Normal[i * 3 + jj]);

      if Obj[j].TF[i][jj]<>65000 then
        glTexCoord2fv(@Obj[j].TexCoord[Obj[j].TF[i][jj]])
      else
        begin
          glTexCoord2f(0, 0);
          glTexCoord2f(0, 0);
        end;

      glVertex3fv(@Obj[j].Vertex[Obj[j].VF[i][jj]]);
		end;
  end;
  glEnd;
glEndList;
{-------------------------------------------------------------}//��������� � VBO
  {glGenBuffersARB(1, @Obj[j].VBOv);
  glBindBufferARB(GL_ARRAY_BUFFER_ARB, Obj[j].VBOv);
  glBufferDataARB(GL_ARRAY_BUFFER_ARB,
                 (Obj[j].VertexCount*2*SizeOf(TVector3f))+
                 ((Obj[j].TexCoordCount)*SizeOf(TVector2f)),
                 nil, GL_STATIC_DRAW_ARB);

  glBufferSubDataARB(GL_ARRAY_BUFFER_ARB,
                     0, Obj[j].VertexCount*SizeOf(TVector3f), @Obj[j].Vertex[0]);

  glBufferSubDataARB(GL_ARRAY_BUFFER_ARB,
                     Obj[j].VertexCount*SizeOf(TVector3f),
                     Obj[j].VertexCount*SizeOf(TVector3f), @Obj[j].Normal[0]);

  glBufferSubDataARB(GL_ARRAY_BUFFER_ARB,
                    Obj[j].VertexCount*2*SizeOf(TVector3f),
                    Obj[j].TexCoordCount*SizeOf(TVector2f), @Obj[j].TexCoord[0]);

  glGenBuffersARB(1, @Obj[j].VBOf);
  glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, Obj[j].VBOf);
  glBufferDataARB(GL_ELEMENT_ARRAY_BUFFER_ARB,
                  Obj[j].FacesCount*Sizeof(TFaces),
                  @Obj[j].VF[0], GL_STATIC_DRAW_ARB);
{-------------------------------------------------------------}
end;
  CloseFile(f);

  BBox.X := (maxo.X+mino.X)/2;
  BBox.Y := (maxo.Y+mino.Y)/2;
  BBox.Z := (maxo.Z+mino.Z)/2;
  BBox.W := (maxo.X-mino.X)/1;
  BBox.H := (maxo.Y-mino.Y)/1;
  BBox.D := (maxo.Z-mino.Z)/1;
end;
end;

procedure TObjLoad.Render;
label 1;
var
  i, j, ii, jj, kk, Number: Integer;
  Posl, Dist: Single;
  max: Single;
  Point: TVector3f;
  t1, t2: GLuint;
  Box: TBBox;
  Distance: array of Integer;
begin
  _TriangleRender := 0;

  if StackCount=0 then Exit;

  for j:=0 to StackCount do
  begin
    glPushMatrix;
      if not Redaktor then MSE.Math.SetTransform(Stack[j].Matrix);
      MSE.OGL.Translate(Stack[j].Pos.X, Stack[j].Pos.Y, Stack[j].Pos.Z);
      MSE.OGL.RotateX(Stack[j].Rot.X);
      MSE.OGL.RotateY(Stack[j].Rot.Y);
      MSE.OGL.RotateZ(Stack[j].Rot.Z);

      for i:=0 to Stack[j].ObjCount-1 do
      if (Stack[j].Obj[i].Transparency=0) and (Stack[j].Obj[i].Alpha>0) then
      begin
        if Stack[j].Obj[i].Texture > 0 then
          glEnable(GL_TEXTURE_2D)
        else
          glDisable(GL_TEXTURE_2D);
        MSE.OGL.SetColor(Stack[j].Obj[i].Color);
        MSE.OGL.Alpha := Stack[j].Obj[i].Alpha-Stack[j].Obj[i].Alpha2;

        if Stack[j].Obj[i].Texture > 0 then
          MSE.Texture.Use(Stack[j].Obj[i].Texture);

        glCallList(Stack[j].Obj[i].List);

    {glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glBindBufferARB(GL_ARRAY_BUFFER_ARB, Stack[j].Obj[i].VBOv);

    glVertexPointer(3, GL_FLOAT, 0, nil);
    glNormalPointer(GL_FLOAT, 0, Pointer((Stack[j].Obj[i].VertexCount)*SizeOf(TVector3f)));
    glTexCoordPointer(2, GL_FLOAT, 0, Pointer((Stack[j].Obj[i].VertexCount)*2*SizeOf(TVector3f)));

    glBindBufferARB(GL_ELEMENT_ARRAY_BUFFER_ARB, Stack[j].Obj[i].VBOf);
    glDrawElements(GL_TRIANGLES, Stack[j].Obj[i].FacesCount*3, GL_UNSIGNED_SHORT, 0);
    }

        MSE.Texture.Use(0);

        _TriangleRender := _TriangleRender+Stack[j].Obj[i].FacesCount;
      end;
    glPopMatrix;

    MSE.OGL.SetColor(1, 1, 1);
    MSE.OGL.Alpha := 255;
  end;

//���������� �������
//MSE.FC.ExtractFrustum;
glEnable(GL_TEXTURE_2D);
Posl := 100000;
MSE.OGL.SetCullFace(CF_None);
for j:=0 to StackCount do
begin
  max := -100000;
  ii := 0;
  k := 0;

    for jj:=0 to StackCount do
    for i:=0 to Stack[jj].ObjCount-1 do
    if Stack[jj].Obj[i].Draw=False then
    if (Stack[jj].Obj[i].Transparency=1) and (Stack[jj].Obj[i].Alpha>0) then
    begin
      Point.X := Stack[jj].Obj[i].BBox.X+Stack[jj].Obj[i].Pos.X;
      Point.Y := Stack[jj].Obj[i].BBox.Y+Stack[jj].Obj[i].Pos.Y;
      Point.Z := Stack[jj].Obj[i].BBox.Z+Stack[jj].Obj[i].Pos.Z;
      Dist := MSE.FC.PointInFrustum(Point.X, Point.Y, Point.Z);
      if (Dist >= max) and
         (Dist <= (Posl))
      then
      begin
        max := Dist;
        ii := jj;
        k := i;
      end;
    end;
    Posl := max;
    if max=-100000 then goto 1;


  glPushMatrix;
  if (Redaktor=False) then
  begin
    MSE.Math.SetTransform(Stack[ii].Matrix);
    MSE.OGL.Translate(Stack[ii].Pos.X, Stack[ii].Pos.Y, Stack[ii].Pos.Z);
    MSE.OGL.RotateX(Stack[ii].Rot.X);
    MSE.OGL.RotateY(Stack[ii].Rot.Y);
    MSE.OGL.RotateZ(Stack[ii].Rot.Z);
  end;

    MSE.OGL.SetColor(Stack[ii].Obj[k].Color);
    MSE.OGL.Alpha := Stack[ii].Obj[k].Alpha;

    glDepthMask(False);
    MSE.Texture.Use(Stack[ii].Obj[k].Texture);

    glCallList(Stack[ii].Obj[k].List);
    Stack[ii].Obj[k].Draw := True;

    _TriangleRender := _TriangleRender+Stack[ii].Obj[k].FacesCount;

    glDepthMask(True);
    MSE.Texture.Use(0);

  glPopMatrix;

  1:
  MSE.OGL.SetColor(1, 1, 1);
  MSE.OGL.Alpha := 255;
  glDepthMask(True);
end;


  if Redaktor then
    StackCount := 0
  else
    StackCount := -1;

  SetLength(Stack, StackCount+1);
  glEnable(GL_TEXTURE_2D);
  MSE.OGL.SetCullFace(CF_Back);
end;

procedure TObjLoad.Draw(Model: TModel);
var
  Box: TBBox;
  i, a: Integer;
begin
  if FrustOff then
  begin
    Inc(StackCount);
    SetLength(Stack, StackCount+1);
    Stack[StackCount] := Model;

    for i:=0 to Stack[StackCount].ObjCount-1 do
      if ((Stack[StackCount].Obj[i].Transparency=0)) then
        Stack[StackCount].Obj[i].Draw := True
      else
        Stack[StackCount].Obj[i].Draw := False;

    Exit;
  end;

  MSE.FC.ExtractFrustum;
  Box.X := Model.BBox.X+Model.Pos.X;
  Box.Y := Model.BBox.Y+Model.Pos.Y;
  Box.Z := Model.BBox.Z+Model.Pos.Z;
  Box.W := MSE.Math.GetMax3(Model.BBox.W, Model.BBox.H, Model.BBox.D);
  Box.H := MSE.Math.GetMax3(Model.BBox.W, Model.BBox.H, Model.BBox.D);
  Box.D := MSE.Math.GetMax3(Model.BBox.W, Model.BBox.H, Model.BBox.D);

  if (Model.TypeObj<>1) then
    if MSE.FC.BoxInFrustum(Box)=False then Exit;

  Inc(StackCount);
  SetLength(Stack, StackCount+1);
  Stack[StackCount] := Model;

  {if MSE.FC.PointInFrustum(Box.X, Box.Y, Box.Z)>10 then
  begin
//    a := MSE.FC.PointInFrustum(Box.X, Box.Y, Box.Z)-100;
  //  if a>255 then a := 255;

    for i:=0 to Stack[StackCount].ObjCount do
    begin
      Stack[StackCount].Obj[i].Alpha2 := 0;
      Stack[StackCount].Obj[i].Alpha2 := Stack[StackCount].Obj[i].Alpha2+Round(MSE.FC.PointInFrustum(Box.X, Box.Y, Box.Z)-10);
    end;
  end;}

  for i:=0 to Stack[StackCount].ObjCount-1 do
  begin
    Box.X := Model.Obj[i].BBox.X+Model.Obj[i].Pos.X;
    Box.Y := Model.Obj[i].BBox.Y+Model.Obj[i].Pos.Y;
    Box.Z := Model.Obj[i].BBox.Z+Model.Obj[i].Pos.Z;
    Box.W := MSE.Math.GetMax3(Model.Obj[i].BBox.W, Model.Obj[i].BBox.H, Model.Obj[i].BBox.D);
    Box.H := MSE.Math.GetMax3(Model.Obj[i].BBox.W, Model.Obj[i].BBox.H, Model.Obj[i].BBox.D);
    Box.D := MSE.Math.GetMax3(Model.BBox.W, Model.BBox.H, Model.BBox.D);

    if (Model.TypeObj=1) then
      Stack[StackCount].Obj[i].Draw := False
    else
    begin
      if ((MSE.FC.BoxInFrustum(Box)=False) and (Stack[StackCount].Obj[i].Transparency=0)) then
        Stack[StackCount].Obj[i].Draw := True
      else
        Stack[StackCount].Obj[i].Draw := False;
    end;
  end;
end;

procedure TObjLoad.Free(model: TModel);
var
  i: Integer;
begin
  for i:=0 to model.ObjCount-1 do begin
    glDeleteBuffers(1, PGLuint(model.Obj[i].VBOv));
    glDeleteBuffers(1, PGLuint(model.Obj[i].VBOf));
    //glDeleteTextures(1, PGLuint(model.Obj[i].Texture));
    {SetLength(Model.Obj[i].Vertex, 0);
    SetLength(Model.Obj[i].Normal, 0);
    SetLength(Model.Obj[i].TexCoord, 0);
    SetLength(Model.Obj[i].VF, 0);
    SetLength(Model.Obj[i].TF, 0);
    SetLength(Model.Obj, 0);}
  end;

  Model.ObjCount := 0;
end;


end.

