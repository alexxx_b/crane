unit E_Engine;

interface

uses
  Windows, Messages, MMSystem, E_Window, E_Math, E_OGL, E_Log, E_Texture, E_Draw,
  E_System, E_Type, E_ObjLoad, E_Phys, E_Input, E_Camera, E_FrustumCulling,
  E_SMD, E_Map, E_Sound, E_Shaders, E_Particles, SysUtils, E_Perlin;

type
  TEngine = class
    private
      _Close, _Pause: Boolean;
      _Handle: HWND;
      //PhysEnable: Boolean;
      TimerId, TimerSys, TimerPhys: UINT;
      procedure SetHandle(const Value: HWND);
    public
      RenderProc, UpdateProc: procedure;
      Window: TWindow;
      OGL: TOGL;
      Light: TLight;
      Math: TMath;
      Log: TLog;
      Texture: TTexture;
      Draw: TDraw;
      System: TSystem;
      ObjLoad: TObjLoad;
      Phys: TPhys;
      Input: TInput;
      Camera: TCamera;
      FC: TFC;
      SMD: TLoadSMD;
      Map: TMapLoad;
      Sound: TSounds;
      Shaders: TShaders;
      Particles: TParticles;
      Perlin: TPerlin;

      Delta: Single;
      IsRender: Boolean;
      RenderBlock, ThreadClose: Boolean;
      constructor Create(Value: HWND);
      destructor Destroy;

      procedure Run;
      property Close: Boolean read _Close write _Close;
      property Pause: Boolean read _Pause write _Pause;
      function GetTime: DWORD;
      procedure SetRenderProc(Proc: Pointer);
      procedure SetUpdateProc(Proc: Pointer);

      property Handle: HWND read _Handle write SetHandle;
  end;

var
  MSE: TEngine;

implementation

uses ODEImport;

constructor TEngine.Create(Value: HWND);
begin
  FormatSettings.DecimalSeparator := '.';
  _Handle := Value;

  Pause := True;

  RenderProc := nil;
  UpdateProc := nil;

  Window := TWindow.Create;
  OGL := TOGL.Create;
  Light := TLight.Create;
  Math := TMath.Create;
  Log := TLog.Create;
  Texture := TTexture.Create;
  Draw := TDraw.Create;
  System := TSystem.Create;
  ObjLoad := TObjLoad.Create;
  Phys := TPhys.Create;
  Input := TInput.Create;
  Camera := TCamera.Create;
  //FC := FC.Create;
  SMD := TLoadSMD.Create;
  Map := TMapLoad.Create;
  Sound := TSounds.Create;
  Shaders := TShaders.Create;
  Particles := TParticles.Create;
  Perlin := TPerlin.Create;

  IsRender := False;
  RenderBlock := False;
end;

destructor TEngine.Destroy;
begin

end;

function TEngine.GetTime: DWORD;
var
  C, F: Int64;
begin
  QueryPerformanceFrequency(F);
  QueryPerformanceCounter(C);
  Result := round(trunc(1000*1000*C/F)/1000);
end;

procedure Update(uTimerID, uMessage: UINT;dwUser, dw1, dw2: DWORD) stdcall;
begin
  if @MSE.UpdateProc <> nil then
    MSE.UpdateProc;
end;

procedure Thread;
begin
  repeat
    Sleep(30);
    if @MSE.UpdateProc <> nil then
      MSE.UpdateProc;
  until MSE.Close;
end;

procedure TEngine.Run;
var
  h1: Cardinal;
var
  Msg: TMsg;
  Quit: Boolean;
begin
  Camera.Init;
  Draw.Init;
  TimerID := timeSetEvent(20, 1, @Update, 0, TIME_PERIODIC);
  //CreateThread(nil, 128, @Thread, self, 0, h1);

  while not MSE.Close {or not ThreadClose} do
  begin
    if ({MSE.Pause and }PeekMessage(Msg, MSE.Window.Handle, 0, 0, PM_REMOVE)) //or
       //(MSE.Pause and GetMessage(Msg, 0, 0, 0))
    then
    begin
      TranslateMessage(Msg);
      DispatchMessage(Msg);
    end
    else
    begin
      if @MSE.RenderProc <> nil then
      begin
        IsRender := True;
        MSE.RenderProc;
        IsRender := False;
      end;
    end;
  end;

  timeKillEvent(MSE.TimerID);
  MSE.Log.Write('���������� ������', Msg_Good);
end;

procedure TEngine.SetHandle(const Value: HWND);
begin
  _Handle := Value;
end;

procedure TEngine.SetRenderProc(Proc: Pointer);
begin
  RenderProc := Proc;
end;

procedure TEngine.SetUpdateProc(Proc: Pointer);
begin
  UpdateProc := Proc;
end;


end.
