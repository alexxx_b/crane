unit MSEOpenGL;

interface

uses
  SysUtils, Classes, Windows, OpenGL20, Forms, Controls, StdCtrls;

type
  TMSEOpenGL = class(TComponent)
  private
    FAction: Boolean;
    FDC: HDC;
    FRC: HGLRC;
    FColorBits: Byte;
    FDepthBits: Byte;
    FStencilBits: Byte;
    FDisplay: TWinControl;
    procedure SetDCPixelFormat;
  protected
  public
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
    Procedure Loaded; override;
    property DC: HDC read FDC;
  published
    property Action: Boolean read FAction write FAction;
    property ColorBits: Byte read FColorBits write FColorBits;
    property DepthBits: Byte read FDepthBits write FDepthBits;
    property StencilBits: Byte read FStencilBits write FStencilBits;
    property Display: TWinControl read FDisplay write FDisplay;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('MSE', [TMSEOpenGL]);
end;


{ TMSEOpenGL }

constructor TMSEOpenGL.Create(AOwner: TComponent);
begin
  inherited;
  Action := True;
  ColorBits := 32;
  DepthBits := 24;
  StencilBits := 8;
end;

destructor TMSEOpenGL.Destroy;
begin

  inherited;
end;

procedure TMSEOpenGL.Loaded;
begin
  inherited;
  InitOpenGL();

  FDC := GetDC(Display.Handle);

  SetDCPixelFormat;

	FRC := wglCreateContext(DC);
	wglMakeCurrent(DC, FRC);

 	glClearColor(0.0, 0.0, 0.0, 0.0);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_DEPTH_TEST);

  glEnable(GL_LIGHTING);
  glEnable(GL_COLOR_MATERIAL);

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
end;

procedure TMSEOpenGL.SetDCPixelFormat;
var
  pfd: TPixelFormatDescriptor;
  nPixelFormat: Integer;
begin
  FillChar (pfd, SizeOf (pfd), 0);

  with pfd do
  begin
    nSize        := sizeof(PIXELFORMATDESCRIPTOR);
    nVersion     := 1;
    dwFlags      := PFD_DRAW_TO_WINDOW or
                    PFD_SUPPORT_OPENGL or
                    PFD_DOUBLEBUFFER;
    iPixelType   := PFD_TYPE_RGBA;
    cColorBits   := ColorBits;
    cDepthBits   := DepthBits;
    cStencilBits := StencilBits;
    iLayerType   := PFD_MAIN_PLANE;
  end;

  nPixelFormat := ChoosePixelFormat (DC, @pfd);

  SetPixelFormat(DC, nPixelFormat, @pfd);
end;


end.
