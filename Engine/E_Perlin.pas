﻿unit E_Perlin;

interface

uses
  Windows, MMSystem, SysUtils, E_Type;

type

  TPerlin = class
    private
    public
      function Noise2D(x, y: integer): Single;
      function Cosine_Interpolate(a, b, x: Single): Single;
      function SmoothedNoise2D(x, y: Integer): Single;
      function CompileNoise(x, y: Single): Single;
      function PerlinNoise_2D(x, y, Factor: Single): Integer;
  end;

implementation

function TPerlin.Noise2D(x, y: integer): Single;
var
  n: Integer;
begin
  n := x + y * 57;
  n := (n shl 13) xor n;
  Result := ( 1.0 - ( (n * (n * n * 15731 + 789221) + 1376312589) and $7fffffff) / 1073741824.0);
end;

function TPerlin.Cosine_Interpolate(a, b, x: Single): Single;
var
  f, ft: Single;
begin
  ft := x * Pi;
  f := (1 - cos(ft)) * 0.5;
  Result := a*(1-f) + b*f;
end;

function TPerlin.SmoothedNoise2D(x, y: Integer): Single;
var
  corners, sides, center: Single;
begin
    corners := ( Noise2D(x-1, y-1)+Noise2D(x+1, y-1)+
         Noise2D(x-1, y+1)+Noise2D(x+1, y+1) ) / 16;
    sides   := ( Noise2D(x-1, y)  +Noise2D(x+1, y)  +
         Noise2D(x, y-1)  +Noise2D(x, y+1) ) /  8;
    center  :=  Noise2D(x, y) / 4;
    Result  := corners + sides + center;
end;

function TPerlin.CompileNoise(x, y: Single): Single;
var
  int_X, int_Y: Integer;
  fractional_X, fractional_Y: Single;
  v1, v2, v3, v4: Single;
  i1, i2: Single;
begin
  int_X := trunc(x);
  fractional_X := x - int_X;

  int_Y := trunc(y);
  fractional_Y := y - int_Y;

  v1 := SmoothedNoise2D(int_X,     int_Y);
  v2 := SmoothedNoise2D(int_X + 1, int_Y);
  v3 := SmoothedNoise2D(int_X,     int_Y + 1);
  v4 := SmoothedNoise2D(int_X + 1, int_Y + 1);

  i1 := Cosine_Interpolate(v1 , v2 , fractional_X);
  i2 := Cosine_Interpolate(v3 , v4 , fractional_X);

  Result := Cosine_Interpolate(i1 , i2 , fractional_Y);
end;

function TPerlin.PerlinNoise_2D(x, y, Factor: Single): Integer;
const
  NUM_OCTAVES = 10;

var
  total: Single;
  persistence: Single;
  frequency: Single;
  amplitude: Single;
  i: Integer;
begin
  total := 0;
  persistence := 0.5;
  frequency := 0.01;//масштаб, на сколько все будет растянуто
  amplitude := 0.5;//на сколько плоский выйдет ландшафт

  x:= x+factor; y:= y + factor;

  for i:=0 to NUM_OCTAVES - 1 do begin
    total := total + CompileNoise(x*frequency, y*frequency) * amplitude;
    amplitude := amplitude*persistence;
    frequency := frequency*2;
  end;
  total:=abs(total);

  Result:=trunc(total*127.0);
end;


end.
