unit E_Log;

interface

uses
  Windows, SysUtils;

type
  TLog = class
    private
      _Enable: Boolean;
      Number: Integer;
      FileName: String;
    public
      constructor Create;
      procedure New(Value: string);
      procedure Write(Msg: String; MsgType: Integer);

      property Enable: Boolean read _Enable write _Enable;
  end;

implementation

uses E_Type, E_Math, E_Engine;

constructor TLog.Create;
begin
  Enable := False;
  FileName := '';
end;

procedure TLog.New(Value: string);
var
  f: TextFile;
begin
  FileName := Value+'.html';
  Number := 0;

  AssignFile(f, FileName);
  Rewrite(f);
  Writeln(f, '<table border="0" cellpadding="0" cellspacing="0" width=100% style="border: 1px solid rgb(204, 204, 204); background-color: rgb(238, 238, 238);">');
  CloseFile(f);
end;

procedure TLog.Write(Msg: String; MsgType: Integer);
var
  f: TextFile;
  Color: string;
  dop: string;
begin
  if Enable and (FileName<>'') then
  begin
    AssignFile(f, FileName);
    Append(f);

    case MsgType of
      Msg_Good: Color := '#000000';
      Msg_Warning: Color := '#0000AA';
      Msg_Error: Color := '#AA0000';
    end;

    Inc(Number);

    dop := '';
    {if Number mod 2 = 0 then
      dop := ' style="background-color: rgb(208, 208, 208);"';}

    Writeln(f, '<tr'+dop+'><td width=50 align="left" valign="top"><b>'+MSE.Math.IntToStr(Number)+
    '</b></td><td width=70 align="left" valign="top" style="font-size: 12px; color:#555588;">'+FormatDateTime( 'hh:nn:ss:zzz', Time)+'</td><td style="color:'+Color+';" align="left" valign="top">'+Msg+'</td></tr>');

    CloseFile(f);
  end;  
end;


end.
