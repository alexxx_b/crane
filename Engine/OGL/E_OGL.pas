unit E_OGL;

interface

uses
  Windows, OpenGL20, E_Type;

type
  TLight = class
    public
      procedure Enable(Id: Integer);
      procedure Disable(Id: Integer);
      procedure Position(Id: Integer; Pos: TVector3f); overload;
      procedure Position(Id: Integer; X, Y, Z: Single); overload;
      procedure Diffuse(Id: Integer; Color: TRGBA); overload;
      procedure Diffuse(Id: Integer; R, G, B, A: Single); overload;
      procedure Ambient(Color: TRGBA); overload;
      procedure Ambient(R, G, B, A: Single); overload;
  end;

  TOGLInfo = record
    MaxSamples: Integer;
    Renderer: String;
    Vendor: String;
    Version: String;
    Extensions: String;
    MaxTextureSize: Integer;
  end;

  TOGL = class
    private
      DC: HDC;
      RC: HGLRC;
      FPSTime: DWORD;
      FPS, FPSInc: Cardinal;
      Color: TRGB;
      procedure SetDCPixelFormat;
      procedure SetAlpha(const Value: Byte);
    public
      Light: TLight;
      AAValue: Integer;
      Info: TOGLInfo;
      constructor Create;
      function InitMultiSample(NumberAA: Integer): Integer;
      procedure Init(NumberAA: Integer);
      procedure VSync(Active: Boolean);
      procedure Clear(Color, Depth, Stencil: Boolean);
      procedure SwapBuffer;
      function GetFPS: Cardinal;
      procedure Set2DRender;
      procedure Set3DRender;
      procedure SetColor(R, G, B: Single); overload;
      procedure SetColor(Color: TRGB); overload;
      procedure Translate(X, Y, Z: Single); overload;
      procedure Translate(Vector: TVector3f); overload;
      procedure RotateX(Angle: Single);
      procedure RotateY(Angle: Single);
      procedure RotateZ(Angle: Single);
      function SupportExt(Ext: PChar): Boolean;
      procedure SetCullFace(Param: Integer);
      procedure SetAntiAliasingValue(Value: Integer);
      function GetMaxMultiSample: Integer;
      procedure ClearStencil(const Value: Integer);
      procedure PushMatrix;
      procedure PopMatrix;
      procedure SetPolygonMode(AMode: Cardinal);
      function GetRGB(R, G, B: Byte): TRGB;
      function GetRGBA(R, G, B, A: Single): TColor4f;

      property Alpha: Byte write SetAlpha;
      function GetOGLCords(x, y : Integer): TVector3d;
      procedure SetClearColor(AColor: TColor4f);
  end;

implementation

uses E_Engine, openil, openilu;

procedure TLight.Enable(Id: Integer);
begin
  glEnable(GL_LIGHT0+Id);
end;

procedure TLight.Disable(Id: Integer);
begin
  glDisable(GL_LIGHT0+Id);
end;

procedure TLight.Position(Id: Integer; Pos: TVector3f);
begin
  Position(Id, Pos.X, Pos.Y, Pos.Z);
end;

procedure TLight.Position(Id: Integer; X, Y, Z: Single);
var
  P: array [0..3] of Single;
begin
  P[0] := X;
  P[1] := Y;
  P[2] := Z;
  P[3] := 1.0;
  glLightfv(GL_LIGHT0 + Id, GL_POSITION, @P);
end;

procedure TLight.Diffuse(Id: Integer; Color: TRGBA);
begin
  Diffuse(Id, Color.R, Color.G, Color.B, Color.A);
end;

procedure TLight.Diffuse(Id: Integer; R, G, B, A: Single);
var
  Value: array [0..3] of Single;
begin
  Value[0] := R;
  Value[1] := G;
  Value[2] := B;
  Value[3] := A;
  glLightfv(GL_LIGHT0 + Id, GL_DIFFUSE, @Value);
end;

procedure TLight.Ambient(Color: TRGBA);
begin
  Ambient(Color.R, Color.G, Color.B, Color.A);
end;

procedure TLight.Ambient(R, G, B, A: Single);
var
  Value: array [0..3] of Single;
begin
  Value[0] := R;
  Value[1] := G;
  Value[2] := B;
  Value[3] := A;
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, @Value);
end;

constructor TOGL.Create;
begin
  {if InitOpenGL() then
    MSE.Log.Write('OpenGL2.0 ���������������', Msg_Good)
  else
    MSE.Log.Write('������!, OpenGL2.0 �� ���������������', Msg_Error);}

  InitOpenGL();

  FPS := 0;
  FPSInc := 0;
  FPSTime := MSE.GetTime;
  AAValue := 0;
end;

function TOGL.InitMultiSample(NumberAA: Integer): Integer;
const
  fAttributes: array [0..1] of Single = (0.0, 0.0);
var
  PixelFormat: Integer;
  NumFormats: UINT;
  iAttributes: array [0..17] of Integer;
begin
  iAttributes[0]  := WGL_DRAW_TO_WINDOW_ARB;
  iAttributes[1]  := 1;
  iAttributes[2]  := WGL_SUPPORT_OPENGL_ARB;
  iAttributes[3]  := 1;
  iAttributes[4]  := WGL_SAMPLE_BUFFERS_ARB;
  iAttributes[5]  := 1;
  iAttributes[6]  := WGL_SAMPLES_ARB;
  iAttributes[7]  := NumberAA;
  iAttributes[8]  := WGL_DOUBLE_BUFFER_ARB;
  iAttributes[9]  := 1;
  iAttributes[10] := WGL_COLOR_BITS_ARB;
  iAttributes[11] := 32;
  iAttributes[12] := WGL_DEPTH_BITS_ARB;
  iAttributes[13] := 24;
  iAttributes[14] := WGL_STENCIL_BITS_ARB;
  iAttributes[15] := 8;
  iAttributes[16] := 0;
  iAttributes[17] := 0;

  wglChoosePixelFormatARB(DC,
                          @iAttributes,
                          @fAttributes,
                          1,
                          @PixelFormat,
                          @NumFormats);

  Result := PixelFormat;
end;

procedure TOGL.PushMatrix;
begin
  glPushMatrix;
end;

procedure TOGL.SetDCPixelFormat;
var
  pfd: TPixelFormatDescriptor;
  nPixelFormat: Integer;
begin
  FillChar (pfd, SizeOf (pfd), 0);

  with pfd do
  begin
    nSize        := sizeof(PIXELFORMATDESCRIPTOR);
    nVersion     := 1;
    dwFlags      := PFD_DRAW_TO_WINDOW or
                    PFD_SUPPORT_OPENGL or
                    PFD_DOUBLEBUFFER;
    iPixelType   := PFD_TYPE_RGBA;
    cColorBits   := 32;
    cDepthBits   := 24;
    cStencilBits := 8;
    iLayerType   := PFD_MAIN_PLANE;
  end;
  
  nPixelFormat := ChoosePixelFormat (DC, @pfd);

  if SetPixelFormat(DC, nPixelFormat, @pfd) then
     MSE.Log.Write('������ �������� ����������', Msg_Good)
  else
    MSE.Log.Write('������!, ������ �������� �� ����������', Msg_Error);
end;

procedure TOGL.SetPolygonMode(AMode: Cardinal);
begin
  glPolygonMode(GL_FRONT_AND_BACK, AMode);
end;

procedure TOGL.Init(NumberAA: Integer);
var
  pfd: TPixelFormatDescriptor;
begin
  DC := GetDC(MSE.Window.Handle);

  if NumberAA = 0 then
  begin
    SetDCPixelFormat;
  end
  else
  begin
    if SetPixelFormat(DC, NumberAA, @pfd) then
      MSE.Log.Write('����������� '+MSE.Math.IntToStr(AAValue)+' ����������', Msg_Good)
    else
      MSE.Log.Write('������! ����������� '+MSE.Math.IntToStr(AAValue)+' �� ����������' , Msg_Error);
  end;

	RC := wglCreateContext(DC);
	if wglMakeCurrent(DC, RC) then
    MSE.Log.Write('�������� ���������� OpenGL ������', Msg_Good)
  else
    MSE.Log.Write('������!, �������� ���������� OpenGL �� ������', Msg_Error);

  ReadExtensions;

 	glClearColor(0.0, 0.0, 0.0, 0.0);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_DEPTH_TEST);

  glEnable(GL_LIGHTING);
  glEnable(GL_COLOR_MATERIAL);

  glEnable(GL_SMOOTH);

  Color.R := 1;
  Color.G := 1;
  Color.B := 1;

  {glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_NORMAL_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);}
end;

procedure TOGL.VSync(Active: Boolean);
begin
  if Active then
  begin
    if Assigned(@wglSwapIntervalEXT) then
      wglSwapIntervalEXT(1);
  end  
  else
  begin
    if Assigned(@wglSwapIntervalEXT) then
      wglSwapIntervalEXT(0);
    if Assigned(@wglGetSwapIntervalEXT) then
      wglGetSwapIntervalEXT;
  end;                      
end;

procedure TOGL.Clear(Color, Depth, Stencil: Boolean);
var
  Param: DWORD;
begin
  Param := 0;
  if Color   then Param := Param or GL_COLOR_BUFFER_BIT;
  if Depth   then Param := Param or GL_DEPTH_BUFFER_BIT;
  if Stencil then Param := Param or GL_STENCIL_BUFFER_BIT;
  glClear(param);
end;

procedure TOGL.SwapBuffer;
begin  
  SwapBuffers(DC);

  //��������� ���������� ������ � �������
  Inc(FPSInc);
  if MSE.GetTime-FPSTime > 1000 then
  begin
    FPS := FPSInc;
    FPSInc := 0;
    FPSTime := MSE.GetTime;
  end;
end;

//���������� ������� fps
function TOGL.GetFPS: Cardinal;
begin
  Result := FPS;
end;

procedure TOGL.Set2DRender;
begin
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_LIGHTING);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  glOrtho(-1, 1, -1, 1, -1, 1);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
end;

procedure TOGL.Set3DRender;
begin
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity;
  gluPerspective(MSE.Camera.FOV, MSE.Window.Width/MSE.Window.Height, MSE.Camera.zFar, MSE.Camera.zNear);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity;
end;

procedure TOGL.SetAlpha(const Value: Byte);
begin
  glEnable(GL_BLEND);
  glColor4f(Color.R, Color.G, Color.B, Value/255);

  glEnable(GL_ALPHA_TEST);
  glAlphaFunc(GL_GEQUAL, 0.0);

  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
end;

procedure TOGL.SetColor(R, G, B: Single);
begin
  glColor3f(R, G, B);
  Color.R := R;
  Color.G := G;
  Color.B := B;
end;

procedure TOGL.SetClearColor(AColor: TColor4f);
begin
  with AColor do
    glClearColor(R/255, G/255, B/255, A/255);
end;

procedure TOGL.SetColor(Color: TRGB);
begin
  SetColor(Color.R, Color.G, Color.B);
end;

procedure TOGL.Translate(X, Y, Z: Single);
begin
  glTranslatef(X, Y, Z);
end;

procedure TOGL.Translate(Vector: TVector3f);
begin
  Translate(Vector.X, Vector.Y, Vector.Z);
end;

procedure TOGL.RotateX(Angle: Single);
begin
  glRotatef(Angle, 1, 0, 0);
end;

procedure TOGL.RotateY(Angle: Single);
begin
  glRotatef(Angle, 0, 1, 0);
end;

procedure TOGL.RotateZ(Angle: Single);
begin
  glRotatef(Angle, 0, 0, 1);
end;

function TOGL.SupportExt(Ext: PChar): Boolean;
begin
  Result := False;
  if Pos(Ext, glGetString(GL_EXTENSIONS)) <> 0 then
    Result := True;
end;

//������������ ����� ������ �������� ����������
procedure TOGL.SetCullFace(Param: Integer);
begin
  glEnable(GL_CULL_FACE);

  case Param of
    CF_None          : glDisable(GL_CULL_FACE);
    CF_Front         : glCullFace(GL_FRONT);
    CF_Back          : glCullFace(GL_BACK);
    CF_Front_and_Back: glCullFace(GL_FRONT_AND_BACK);
  end;
end;

procedure TOGL.SetAntiAliasingValue(Value: Integer);
begin
  AAValue := Value;
end;

function TOGL.GetMaxMultiSample: Integer;
begin
  Result := Info.MaxSamples;
end;

function TOGL.GetRGB(R, G, B: Byte): TRGB;
begin
  Result.R := R;
  Result.G := G;
  Result.B := B;
end;

function TOGL.GetRGBA(R, G, B, A: Single): TColor4f;
begin
  Result.R := R;
  Result.G := G;
  Result.B := B;
  Result.A := A;
end;

procedure TOGL.PopMatrix;
begin
  glPopMatrix;
end;

procedure TOGL.ClearStencil(const Value: Integer);
begin
  glClearStencil(Value);
end;

function TOGL.GetOGLCords(X, Y: Integer): TVector3d;
var
	viewport:   TGLVectori4;
	modelview:  TGLMatrixd4;
	projection: TGLMatrixd4;
	winZ,winY:  Single;
begin
	glGetDoublev(GL_MODELVIEW_MATRIX, @modelview );                          // Get the Current Modelview matrix
	glGetDoublev(GL_PROJECTION_MATRIX, @projection );                        // Get the Current Projection Matrix
	glGetIntegerv(GL_VIEWPORT, @viewport );                                  // Get the Current Viewport

  winY := viewport[3] - y;                                                 //Change from Win32 to OpenGL coordinate system

	glReadPixels(X, Round(winY), 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, @winZ );//Read the Depth value at the current X and Y position
	gluUnProject(X, winY, winZ,
              @modelview, @projection, @viewport,                             // Get the vector for the current mouse position
			        Result.X, Result.Y, Result.Z);                         // And Return it from the function
end;


end.
