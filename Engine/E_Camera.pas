unit E_Camera;

interface

uses
  Windows, E_Type, E_Math, ODEImport, SysUtils;

type
  TCamera = class
    private
      Kd: Single;
      _FOV, _zNear, _zFar: Single;
    procedure SetFOV(const Value: Single);
    procedure SetzFar(const Value: Single);
    procedure SetzNear(const Value: Single);
    public
      p: TVector3f;
      Phys: TBody;
      PhysGeom: PdxGeom;
      Speed, Sens: Single;
      Rot, Pos: Tvector3f;
      Jump: Integer;
      JumpBool: Boolean;//�������� �� ���������� ��������� � �������, ����� �� ������� �������
      CameraEnable: Boolean;
      constructor Create;
      procedure Init;
      procedure InputKeyBord;
      procedure InputMouse;
      procedure Enable(Value: Boolean);
      function GetEnable: Boolean;
      function GetPos: TVector3f;
      function GetRot: TVector3f;
      procedure SetSpeed(cSpeed: Single);
      function GetSpeed: Single;

      property FOV: Single read _FOV write SetFOV;
      property zNear: Single read _zNear write SetzNear;
      property zFar: Single read _zFar write SetzFar;

      procedure GoForward;
      procedure GoBack;
      procedure GoLeft;
      procedure GoRight;

 end;

implementation

uses E_Engine, E_Phys;

constructor TCamera.Create;
begin
  Pos.X := 0;
  Pos.Y := 10.7;
  Pos.Z := 0;
  Rot.X := 0;
  Rot.Y := 0;
  Rot.Z := 0;
  Speed := 1;
  Sens := 0.1;
  CameraEnable := False;
  Jump := 0;
  JumpBool := False;
end;

procedure TCamera.InputMouse;//������ ������ � ������� ����
var
  m: TPoint;
begin
  GetCursorPos(m);

  if m.x > round(MSE.Window.Width/2) then
    Rot.y := Rot.y+(Sens*(m.x-round(MSE.Window.Width/2)));

  if m.x < round(MSE.Window.Width/2) then
    Rot.y := Rot.y-(Sens*(round(MSE.Window.Width/2)-m.x));

  if m.y > round(MSE.Window.Height/2) then begin
    Rot.x := Rot.x+(Sens*(m.y-round(MSE.Window.Height/2)));
    if Rot.X > 90 then
      Rot.X := 90;
  end;

  if m.y < round(MSE.Window.Height/2) then begin
    Rot.x:=Rot.x-(Sens*(round(MSE.Window.Height/2)-m.y));
    if Rot.X<-90 then
      Rot.X:=-90;
  end;

  MSE.Input.SetCursorPosCenter;
end;

procedure TCamera.Init;
var
  Mass: TdMass;
  BodySpace: PdxSpace;
begin
  Exit;
  if PhysGeom = nil then
  begin
    Phys := dBodyCreate(MSE.Phys.World);

    dBodySetPosition(Phys, 0, 15, 0);

    //PhysGeom := dCreateBox(nil, 0.5, 0.5, 1.7);
    //dMassSetBox(Mass, 100, 0.5, 0.5, 1.7);
    PhysGeom := dCreateCapsule(nil, 0.8, 2.0);
    dMassSetCapsule(Mass, 0.07, 1, 0.8, 2.0);
    //PhysGeom := dCreateCylinder(nil, 0.5, 1.7);
    //dMassSetCylinder(Mass, 70, 1, 0.5, 1.7);
    //PhysGeom := dCreateSphere(nil, 0.8);
    //dMassSetSphere(mass, 70, 0.8);

    dMassAdjust(Mass, 0.07);
    dBodySetMass(Phys, @Mass);

    dGeomSetBody(PhysGeom, Phys);
    BodySpace := dSimpleSpaceCreate(MSE.Phys.Space);
    dSpaceSetCleanup(BodySpace, 0);
    dSpaceAdd(BodySpace, PhysGeom);

    dBodySetFiniteRotationMode(Phys, 1);
    dBodySetFiniteRotationAxis(Phys, 0.0, 1.0, 0.0);
  end;
end;

procedure TCamera.InputKeyBord;
var
  Tr, Tr1: PdVector3;
begin
  Kd := 5;
  if MSE.Input.KeyDown(65) then
  begin
    pos.x := pos.x-(Speed*(cos(Rot.y/360*2*pi)));
    pos.z := pos.z-(Speed*(sin(Rot.y/360*2*pi)));
  end;
  if MSE.Input.KeyDown(68) then begin
    pos.x := pos.x+(Speed*(cos(Rot.y/360*2*pi)));
    pos.z := pos.z+(Speed*(sin(Rot.y/360*2*pi)));
  end;
  if MSE.Input.KeyDown(87) then begin
    pos.z := pos.z-(Speed*(cos(Rot.y/360*2*pi)));
    pos.x := pos.x+(Speed*(sin(Rot.y/360*2*pi)));
  end;
  if MSE.Input.KeyDown(83) then begin
    pos.z := pos.z+(Speed*(cos(Rot.y/360*2*pi)));
    pos.x := pos.x-(Speed*(sin(Rot.y/360*2*pi)));
  end;

  if MSE.Input.KeyDown(17) then begin
    Pos.Y := Pos.Y - Speed/5;
  end;

  if MSE.Input.KeyDown(16) then begin
    Pos.Y := Pos.Y + Speed/5;
  end;
  Exit;


  //Tr := dBodyGetLinearVel(Phys);
  //dBodySetLinearVel(Phys, 0, Tr[1], 0);

  Tr := dBodyGetPosition(Phys);
  //dGeomCylinderGetParams(PhysGeom, rc, lc);
  Pos.X := Tr[0];
  Pos.Y := Tr[1];
  Pos.Z := Tr[2];

  p.X := 0;
  p.Y := 0;
  p.Z := 0;

  {Tr := dBodyGetLinearVel(CPhys.Obj[Phys.Body]);
  if Tr[0] <> 0 then
  begin
    dBodyAddForce(CPhys.Obj[Phys.Body], -Tr[0]*5, 0, 0);
  end;
  if Tr[2] <> 0 then
  begin
    dBodyAddForce(CPhys.Obj[Phys.Body], 0, 0, -Tr[2]*5);
  end;}

  if not JumpBool then
  begin
    if MSE.Input.KeyDown(65) then begin
      p.X := p.X - (Kd*cos(Rot.y/Rad))/1.7;
      p.Z := p.Z - (Kd*sin(Rot.y/Rad))/1.7;
    end;
    if MSE.Input.KeyDown(68) then begin
      p.X := p.X + (Kd*cos(Rot.y/Rad))/1.7;
      p.Z := p.Z + (Kd*sin(Rot.y/Rad))/1.7;
    end;
    if MSE.Input.KeyDown(87) then begin
      p.X := p.X + (Kd*sin(Rot.y/Rad));
      p.Z := p.Z - (Kd*cos(Rot.y/Rad));
    end;
    if MSE.Input.KeyDown(83) then begin
      p.X := p.X - (Kd*sin(Rot.y/Rad))/1.7;
      p.Z := p.Z + (Kd*cos(Rot.y/Rad))/1.7;
    end;
  end;

  //Tr := dBodyGetForce(Phys);
  //dBodyAddForce(Phys, p.X, Tr[1], p.Z);
  Tr := dBodyGetLinearVel(Phys);
  dBodySetLinearVel(Phys, p.X, Tr[1], p.Z);
end;


procedure TCamera.Enable(Value: Boolean);
begin
  CameraEnable := Value;
end;

function TCamera.GetPos: TVector3f;
begin
  Result := Pos;
end;

function TCamera.GetRot: TVector3f;
begin
  Result := Rot;
end;

procedure TCamera.SetFOV(const Value: Single);
begin
  _FOV := Value;
end;

function TCamera.GetEnable: Boolean;
begin
  Result := CameraEnable;
end;

function TCamera.GetSpeed: Single;
begin
  Result := Speed;
end;

procedure TCamera.GoBack;
begin
  pos.x := pos.x+(Speed*(cos(Rot.y/360*2*pi)));
  pos.z := pos.z+(Speed*(sin(Rot.y/360*2*pi)));
end;

procedure TCamera.GoForward;
begin
  pos.x := pos.x-(Speed*(cos(Rot.y/360*2*pi)));
  pos.z := pos.z-(Speed*(sin(Rot.y/360*2*pi)));
end;

procedure TCamera.GoLeft;
begin
  pos.z := pos.z-(Speed*(cos(Rot.y/360*2*pi)));
  pos.x := pos.x+(Speed*(sin(Rot.y/360*2*pi)));
end;

procedure TCamera.GoRight;
begin
  pos.z := pos.z+(Speed*(cos(Rot.y/360*2*pi)));
  pos.x := pos.x-(Speed*(sin(Rot.y/360*2*pi)));
end;

procedure TCamera.SetSpeed(cSpeed: Single);
begin
  Speed := cSpeed/1;
end;

procedure TCamera.SetzFar(const Value: Single);
begin
  _zFar := Value;
end;

procedure TCamera.SetzNear(const Value: Single);
begin
  _zNear := Value;
end;

end.
