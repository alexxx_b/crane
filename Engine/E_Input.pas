unit E_Input;

interface

uses
  Windows, E_Type;

type
  TInput = class
    public
      MousePos: TVector2i;
      MClickL, MClickR: Boolean;
      MouseWheel: Integer;
      Key: array[0..255] of Boolean;
      constructor Create;
      function MousePresent: Boolean;
      function MouseScrollPresent: Boolean;
      //function KeyBordPresent: Boolean;
      function KeyDown(k: Integer): Boolean;
      procedure KeySetValue(K: Word; Value: Boolean);
      function GetCursorXY: TVector2i;
      procedure SetCursorXY(X, Y: Integer);
      procedure SetCursorPosCenter;
      function GetMouseClickL: Boolean;
      function GetMouseClickR: Boolean;
      function GetMouseWheel: Integer;
      function GetMousePosOGL: TVector2f;
    private
  end;

implementation

uses E_Engine, Types;

constructor TInput.Create;
var
  i: Integer;
begin
  for i:=0 to 255 do
    Key[i] := False;
end;

function TInput.MousePresent: Boolean;
begin
  if GetSystemMetrics(SM_MOUSEPRESENT) <> 0 then
    Result := true
  else
    Result := false;
end;

function TInput.MouseScrollPresent: Boolean;
//var
  //m:
begin
end;

function TInput.KeyDown(k: Integer): Boolean;
begin
  if Key[k] then
    Result := True
  else
    Result := False;
end;

procedure TInput.KeySetValue(K: Word; Value: Boolean);
begin
  Key[k] := Value;
end;

function TInput.GetCursorXY: TVector2i;
var
  m: TPoint;
begin
  GetCursorPos(m);
  MSE.Input.MousePos.X := m.X;
  MSE.Input.MousePos.Y := m.Y;
  Result.X := m.X;
  Result.Y := m.Y;
end;

procedure TInput.SetCursorXY(x, y: Integer);
begin
  SetCursorPos(x, y);
end;

procedure TInput.SetCursorPosCenter;
var
  Rect: TRect;
  Pos: Windows.TPoint;
  m_delta: TVector2f;
begin
  GetWindowRect(MSE.Window.Handle, Rect);
  GetCursorPos(Pos);
  m_delta.X := Pos.X - Rect.Left - (Rect.Right - Rect.Left) div 2;
  m_delta.Y := Pos.Y - Rect.Top  - (Rect.Bottom - Rect.Top) div 2;
  SetCursorPos(Rect.Left + (Rect.Right - Rect.Left) div 2, Rect.Top + (Rect.Bottom - Rect.Top) div 2);
end;

function TInput.GetMouseClickL: Boolean;
begin
  Result := MClickL;
end;

function TInput.GetMouseClickR: Boolean;
begin
  Result := MClickR;
end;

function TInput.GetMouseWheel: Integer;
begin
  Result := MouseWheel;
  MouseWheel := 0;
end;

function TInput.GetMousePosOGL: TVector2f;
begin
  Result.X := ((MSE.Input.GetCursorXY.X/MSE.Window.Width)-0.5)*2;
  Result.Y := ((MSE.Input.GetCursorXY.Y/MSE.Window.Height)-0.5)*-2;
end;

end.
