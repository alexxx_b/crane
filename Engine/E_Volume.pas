unit E_Volume;

interface

uses
  E_Type;

Type
  TMesh = class
    Vertices: array of TVector3f;
    Faces: array of array[0..2] of integer;
    Procedure SetVCount(VCount: integer);
    Function GetVCount: integer;
    Property VCount: integer read GetVCount write SetVCount;
    Procedure SetFCount(FCount: integer);
    Function GetFCount: integer;
    Property FCount: integer read GetFCount write SetFCount;
    Function GetVertex(V: TVector3f): integer;
    Property Property_GetVertex[index: TVector3f]: integer read GetVertex; default;
    Procedure Render;
  end;

  TEdge = packed record
    v: array[0..1] of integer;
    f: array[0..1] of integer;
    r: array[0..1] of boolean;
    c: boolean;
  end;

  TPlane = class
    Normal: TVector3f;
    Dist: single;
    Constructor Create(v1, v2, v3: TVector3f);
    Function GetDist(V: TVector3f): single;
  end;

Type
TVolume = class
  Mesh: TMesh;
  Vertices: array of TVector3f;
  Faces: array of array[0..2] of integer;
  Edges: array of TEdge;
  Constructor Create(Model: TObj);
  Destructor Destroy;
  Procedure CalculateEdges;
  Procedure Build(Light: TVector3f);
  Procedure Render;
  Procedure SetVCount(VCount: integer);
  Function GetVCount: integer;
  Property VCount: integer read GetVCount write SetVCount;
  Procedure SetFCount(FCount: integer);
  Function GetFCount: integer;
  Property FCount: integer read GetFCount write SetFCount;
  Procedure SetECount(ECount: integer);
  Function GetECount: integer;
  Property ECount: integer read GetECount write SetECount;
end;

implementation

uses
  OpenGL20, E_Math, E_Engine;

Procedure TMesh.SetVCount(VCount: integer);
begin
  SetLength(Vertices, VCount);
end;

Function TMesh.GetVCount: integer;
begin
  Result := Length(Vertices);
end;

Procedure TMesh.SetFCount(FCount: integer);
begin
  SetLength(Faces, FCount);
end;

Function TMesh.GetFCount: integer;
begin
  Result := Length(Faces);
end;

Function TMesh.GetVertex(V: TVector3f): integer;
  Var
  i: integer;
begin
    for i := 0 to VCount - 1 do
      If MSE.Math.Distance3D(Vertices[i], V) < 1e-8 then begin
        Result := i;
        exit;
      end;
  VCount := VCount + 1;
  Vertices[VCount - 1] := V;
  Result := VCount - 1;
end;

Procedure TMesh.Render;
  Var
  i, j: integer;
begin
  glBegin(GL_TRIANGLES);
    for i := 0 to FCount - 1 do
      for j := 0 to 2 do
        glVertex3fv(@Vertices[Faces[i][j]]);
  glEnd;
end;

Constructor TPlane.Create(v1, v2, v3: TVector3f);
begin
  Normal := MSE.Math.NormalizeVector(
              MSE.Math.CrossVector(MSE.Math.SubVector(v2, v1),
              MSE.Math.SubVector(v3, v1)));
  Dist := - MSE.Math.Dot3d(V1, Normal);
end;

Function TPlane.GetDist(V: TVector3f): Single;
begin
  Result := MSE.Math.Dot3D(Normal, V) + Dist;
end;

Constructor TVolume.Create(Model: TObj);
  Var
  i, j: integer;
  V: TVector3f;
begin
  Mesh := TMesh.Create;
  Mesh.FCount := Model.FacesCount;
    for i := 0 to Mesh.FCount-1 do
      for j := 0 to 2 do
      begin
        V.X := Model.Vertex[Model.VF[i][j]].X+Model.Pos.X;
        V.Y := Model.Vertex[Model.VF[i][j]].Y+Model.Pos.Y;
        V.Z := Model.Vertex[Model.VF[i][j]].Z+Model.Pos.Z;
        Mesh.Faces[i][j] := Mesh[V];
      end;
  CalculateEdges;
end;

Destructor TVolume.Destroy;
begin
  Mesh.Destroy;
end;

procedure TVolume.CalculateEdges;
var
  i, j, m, n: integer;
  v1: array[0..1] of integer;
  v2: array[0..1] of integer;
procedure AddEdge(f1, f2, v11, v21, v12, v22: integer);
begin
  ECount := ECount + 1;
  With Edges[ECount - 1] do
  begin
    v[0] := v11;
    v[1] := v21;
    r[0] := false;
    if v11 > v21 then
    begin
      v[0] := v21;
      v[1] := v11;
      r[0] := true;
    end;
    if v12 > v22 then
      r[1] := true;

    edges[ECount - 1].f[0] := f1;
    edges[ECount - 1].f[1] := f2;
  end;
end;
begin
  ECount := 0;
    for i := 0 to Mesh.FCount - 1 do
      for j := i + 1 to Mesh.FCount - 1 do
        If i <> j then
          for n := 0 to 2 do
            for m := 0 to 2 do
            begin
              v1[0] := Mesh.Faces[i][n];
              v1[1] := Mesh.Faces[i][(n + 1) mod 3];
              v2[0] := Mesh.Faces[j][m];
              v2[1] := Mesh.Faces[j][(m + 1) mod 3];
                If (v1[0] = v2[0])and(v1[1] = v2[1]) then
                  addEdge(i, j, v1[0], v1[1], v2[0], v2[1]);
                If (v1[0] = v2[1])and(v1[1] = v2[0]) then
                  addEdge(i, j, v1[0], v1[1], v2[0], v2[1]);
            end;
  Build(MSE.Math.GetVector(0, 100, 0));
end;

Procedure TVolume.Build(Light: TVector3f);
  Var
  i, j, m, n: integer;

  Edge: TVector3f;
  Vertex1, Vertex2, Vertex3, Vertex4: TVector3f;
  Normal1, Normal2: TVector3f;
  Plane1, Plane2: TPlane;

  ia, ib: integer;
  
  Procedure AddQuad(v1, v2, v3, v4: TVector3f);
  begin
    VCount := VCount + 4;
    Vertices[VCount - 4] := v1;
    Vertices[VCount - 3] := v2;
    Vertices[VCount - 2] := v3;
    Vertices[VCount - 1] := v4;
    FCount := FCount + 2;
    Faces[FCount - 2][0] := VCount - 4;
    Faces[FCount - 2][1] := VCount - 3;
    Faces[FCount - 2][2] := VCount - 1;
    Faces[FCount - 1][0] := VCount - 3;
    Faces[FCount - 1][1] := VCount - 2;
    Faces[FCount - 1][2] := VCount - 1;
  end;
begin

  // 0. Initialize
  VCount := 0;
  FCount := 0;

  // 2. Build quads
    for i := 0 to ECount - 1 do begin
      Vertex1 := Mesh.Vertices[Mesh.Faces[edges[i].f[0]][0]];
      Vertex2 := Mesh.Vertices[Mesh.Faces[edges[i].f[0]][1]];
      Vertex3 := Mesh.Vertices[Mesh.Faces[edges[i].f[0]][2]];
      Plane1 := TPlane.Create(Vertex1, Vertex2, Vertex3);
      Vertex1 := Mesh.Vertices[Mesh.Faces[edges[i].f[1]][0]];
      Vertex2 := Mesh.Vertices[Mesh.Faces[edges[i].f[1]][1]];
      Vertex3 := Mesh.Vertices[Mesh.Faces[edges[i].f[1]][2]];
      Plane2 := TPlane.Create(Vertex1, Vertex2, Vertex3);
        If Plane1.GetDist(Light) * Plane2.GetDist(Light) < 0 then begin
          //AddQuad(Vertex4, Vertex3, Vertex2, Vertex1);
            If ( Plane1.GetDist(Light) > 0 ) then
              If ( edges [i].r[0] ) then begin           // whether order of points differs from f1
                  ia := edges [i].v[1];
                  ib := edges [i].v[0];
              end else begin
                  ia := edges [i].v[0];
                  ib := edges [i].v[1];
              end
          else                                // faces [edges [i].f2] is frontFacing
              if ( edges [i].r[1] ) then begin
                  ia := edges [i].v[1];
                  ib := edges [i].v[0];
              end else begin
                  ia := edges [i].v[0];
                  ib := edges [i].v[1];
              end;
          Vertex1 := Mesh.Vertices[ia];
          Vertex2 := Mesh.Vertices[ib];
          Edge := MSE.Math.SubVector(Vertex2, Light);
          Vertex3 := MSE.Math.AddVector(Vertex2, MSE.Math.VextorMultConst(Edge, 500));
          Edge := MSE.Math.SubVector(Vertex1, Light);
          Vertex4 := MSE.Math.AddVector(Vertex1, MSE.Math.VextorMultConst(Edge, 500));
          AddQuad(Vertex1, Vertex2, Vertex3, Vertex4);
          Edges[i].c := true
        end else
          Edges[i].c := false;
      Plane1.Destroy;
      Plane2.Destroy;
    end;
end;

Procedure TVolume.Render;
var
  i, j: Integer;
begin
  glEnable(GL_DEPTH_CLAMP_NV);//��� ��������� ����� ������� ���������� ���������
  glDisable(GL_TEXTURE_2D);
  glColorMask(False, False, False, False);
  glDepthMask(False);

  //setup stencil
  glEnable(GL_STENCIL_TEST);
  glStencilFunc(GL_ALWAYS, 0, high(TGLuint));
  glStencilOp(GL_KEEP,            // stencil test fail
              GL_KEEP,            // depth test fail
              GL_INCR);           // depth test pass

  // draw all front-faced shadow polys
  glEnable(GL_CULL_FACE);
  glCullFace(GL_FRONT);
  glBegin(GL_TRIANGLES);
    for i := 0 to FCount - 1 do
    for j := 0 to 2 do
      glVertex3fv(@Vertices[Faces[i][j]]);
  glEnd;

  // setup stencil
  glStencilOp(GL_KEEP,            // stencil test fail
              GL_KEEP,            // depth test fail
              GL_DECR);           // depth test pass

  // draw all back-faced shadow polys
  glCullFace(GL_BACK);
  glBegin(GL_TRIANGLES);
    for i := 0 to FCount - 1 do
    for j := 0 to 2 do
      glVertex3fv(@Vertices[Faces[i][j]]);
  glEnd;
  glDisable(GL_CULL_FACE);

  //������ �� ����� ������� ������������ ���� � ����������
  glColorMask(True, True, True, True);
  glDepthFunc(GL_LESS);
  glStencilFunc(GL_NOTEQUAL, 0, high(TGLuint));
  glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
  glDepthMask(True);

  glPushMatrix;
    glMatrixMode(GL_PROJECTION);
    glPushMatrix;
      MSE.OGL.Set2DRender;
      glLoadIdentity;

      glDisable(GL_DEPTH_TEST);
      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
      glColor4f(0.0, 0.0, 0.0, 0.5);

      glBegin(GL_QUADS);
        glVertex2f(-1, -1);
        glVertex2f(MSE.Window.Width, 0);
        glVertex2f(MSE.Window.Width, MSE.Window.Height);
        glVertex2f(0, MSE.Window.Height);
      glEnd;
      glColor4f(1, 1, 1, 1);
      glDisable(GL_BLEND);

      glMatrixMode(GL_PROJECTION);
    glPopMatrix;
    glMatrixMode(GL_MODELVIEW);
  glPopMatrix;

  glEnable(GL_DEPTH_TEST);
  glDisable(GL_STENCIL_TEST);
  glEnable(GL_TEXTURE_2D);
  glDisable(GL_DEPTH_CLAMP_NV);
end;

Procedure TVolume.SetVCount(VCount: integer);
begin
  SetLength(Vertices, VCount);
end;

Function TVolume.GetVCount: integer;
begin
  Result := Length(Vertices);
end;

Procedure TVolume.SetFCount(FCount: integer);
begin
  SetLength(Faces, FCount);
end;

Function TVolume.GetFCount: integer;
begin
  Result := Length(Faces);
end;

Procedure TVolume.SetECount(ECount: integer);
begin
  SetLength(Edges, ECount);
end;

Function TVolume.GetECount: integer;
begin
  Result := Length(Edges);
end;

end.
