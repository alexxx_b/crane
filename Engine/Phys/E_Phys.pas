unit E_Phys;

interface

uses
  Windows, ODEImport, E_Type, MMSystem, E_ObjLoad, E_Map, SysUtils, Math;

type
  TPhys = class
    private
      TimerPhys: UINT;
    public
      UPS: Integer;
      t: Single;
      FPS: Integer;
      sFPS: String;
      InitBool: Boolean;
      World: PdxWorld;
      ContactGroup: TdJointGroupID;
      Space: PdxSpace;

      MeshV: PdVector3Array;
      MeshF: PdIntegerArray;
      CountV, CountF: Integer;

      Blocking: Boolean;
      Finish: Integer;

      constructor Create;
      procedure Init(gX, gY, gZ: Single; _UPS: Integer);
      procedure Update;
      function CreateSphere(X, Y, Z, Radius, Mas: Single): TBody;
      procedure CreateBox(X, Y, Z, lx, ly, lz, Mas: Single; var B: TBody; var G: PdxGeom); overload;
      procedure CreateBox(BBox: TBBox; Mas: Single; var B: TBody; var G: PdxGeom); overload;
      function CreateCylinder(X, Y, Z, R, L, Mas: Single): TBody;
      function CreateBoxStatic(X, Y, Z, lx, ly, lz: Single): PdxGeom; overload;
      function CreateBoxStatic(BBox: TBBox): PdxGeom; overload;
      function CreateCapsule(X, Y, Z, R, L, Mas: Single): TBody;
      function CreateCapsuleBox(X, Y, Z, R, L, Mas: Single): TBody;
      function GetObjectOrientation(Body: TBody): TMatrix;
      procedure CreateTriMesh;
      function GetFPS: string;
      procedure AddObj(var Obj: TObj; Metod: Integer; Pos, Rot: TVector3f);
      procedure AddModel(Model: TModel; Metod: Integer);
      procedure AddMap(Map: TMap);
      function GetBodyPos(Body: TBody): TVector3f;
      function GetBodyRot(Body: TBody): TVector3f;
      function GetBodyQuaternion(Body: TBody): TVector4;
      function GetBodyLinearVel(Body: TBody): TVector3f;
      procedure GeomSetRotatioY(var Geom: PdxGeom; Angle: Single);
  end;

const
  win_t = 2000;

var
  obj_c, obj_c_yes: Integer;
  Cont, b: Boolean;

var
  Mesh: array[1..100] of PdxTriMeshData;
  MeshGeom: array[1..100] of PdxGeom;
  Meshi: Integer=0;

implementation

uses E_Math, E_Engine, Ini, E_Kran;

constructor TPhys.Create;
begin
  InitBool := False;
  t := MSE.GetTime;
  FPS := 0;
  Blocking := False;
  Finish := win_t;
end;

procedure nearCallback(data: pointer; o1, o2: PdxGeom); cdecl;
const
  cN = 20;
  MaxVel=100;
var
  b1, b2: PdxBody;
  c: TdJointID;
  Contact: array[0..cN-1] of TdContact;
  i, i1, i2, j1, j2, Numc: integer;
  a: PdVector3;
  Vel: PdVector3;
  Rot: PdMatrix3;
  Rot2: TdMatrix3;
begin
  b1 := dGeomGetBody(o1);
  b2 := dGeomGetBody(o2);

  if (Assigned(b1) and Assigned(b2) and (dAreConnected(b1,b2)<>0)) then
    Exit;

  Numc := dCollide(o1, o2, cN, Contact[0].geom, SizeOf(TdContact));
  if (Numc > 0) then
  begin
    for i := 0 to Numc-1 do
    begin
      //���������� ���������� �������, ���� ��� ����� ���������� �� ��������
      for i1:=0 to Map.ModelCount-1 do
      if (map.Model[i1].TypeObj=1) then
      begin
        b := True;
        for i2:=0 to Map.Model[i1].ObjCount-1 do
        if (Contact[i].geom.g1=Map.Model[i1].Obj[i2].Geom) or (Contact[i].geom.g2=Map.Model[i1].Obj[i2].Geom) then
        begin
          for j1:=0 to Map.ModelCount-1 do
          if (map.Model[j1].TypeObj=2) then
          begin
            for j2:=0 to Map.Model[j1].ObjCount-1 do
            if (Contact[i].geom.g1=Map.Model[j1].Obj[j2].Geom) or (Contact[i].geom.g2=Map.Model[j1].Obj[j2].Geom) then
              begin
                if b=True then
                  map.Model[i1].cont_yes := True;
                b := False;
              end;
          end;
        end;
      end;

      contact[i].surface.mode := dContactApprox1;

      Contact[i].surface.mu := 7;
      //Contact[i].surface.mu2 := 0;
      Contact[i].surface.slip1 := 0.1;
      Contact[i].surface.slip2 := 0.1;
      Contact[i].surface.bounce := 0.01;
      Contact[i].surface.soft_erp := 0.5;
      //contact[i].surface.bounce_vel := 0.1;
      Contact[i].surface.soft_cfm := 0.001;

      if (Contact[i].geom.g1=Cars.Hook_Geom) or (Cars.p1=dGeomGetBody(Contact[i].geom.g1)) then
      begin
        Cars.Hook_Contact := True;
        Cars.Hook_Contact_Geom := Contact[i].geom.g2;
      end
      else
      begin
        if (Contact[i].geom.g2=Cars.Hook_Geom) or (Cars.p1=dGeomGetBody(Contact[i].geom.g2)) then
        begin
          Cars.Hook_Contact := True;
          Cars.Hook_Contact_Geom := Contact[i].geom.g1;
        end;
      end;

      //���� �������� ����
      if ((Contact[i].geom.g1=Cars.Hook_Geom) or (Contact[i].geom.g2=Cars.Hook_Geom))
         and (Cars.Hook_Joint2=0)
         //and (dAreConnected(dGeomGetBody(Contact[i].geom.g1), dGeomGetBody(Contact[i].geom.g2))=0)
         then
      begin
        for i1:=0 to Map.ModelCount-1 do
        for i2:=0 to Map.Model[i1].ObjCount-1 do
        if map.Model[i1].TypeObj=1 then
        begin
          if (Map.Model[i1].Obj[i2].Body<>nil) then
          begin
            if (dGeomGetBody(Contact[i].geom.g1)=Map.Model[i1].Obj[i2].Body)
               or (dGeomGetBody(Contact[i].geom.g2)=Map.Model[i1].Obj[i2].Body) then
            begin
              Cars.p1 := Map.Model[i1].Obj[i2].Body;

              Cars.Hook_Joint2 := dJointCreateSlider(MSE.Phys.World, 0);
              dJointAttach(Cars.Hook_Joint2, dGeomGetBody(Contact[i].geom.g1), dGeomGetBody(Contact[i].geom.g2));
              dJointSetSliderAxis(Cars.Hook_Joint2, 0, 1, 0);
            end;
          end;
        end;
      end;

      c := dJointCreateContact(MSE.Phys.World, MSE.Phys.ContactGroup, @Contact[i]);

      dJointAttach (c,
        dGeomGetBody(Contact[i].geom.g1),
		    dGeomGetBody(Contact[i].geom.g2));
    end;
  end;
end;

procedure TPhys.Init(gX, gY, gZ: Single; _UPS: Integer);
var
  i: Integer;
begin
try
  UPS := _UPS;

  dInitODE2(0);

  Blocking := False;
  World := dWorldCreate;
//  Space := dHashSpaceCreate(0);
  Space := dSimpleSpaceCreate(0);
  dWorldSetGravity(World, gX, gY, gZ);
  ContactGroup := dJointGroupCreate(0);
  dWorldSetContactSurfaceLayer(World, 0.05);

  // setup autodisabling
  {dWorldSetAutoDisableFlag(World, 1);
  dWorldSetAutoDisableLinearThreshold(World, 0.1);
  dWorldSetAutoDisableAngularThreshold(World, 0.1);
  dWorldSetAutoDisableSteps(World, 10);
  dWorldSetAutoDisableTime(World, 0);
  dWorldSetContactMaxCorrectingVel(World, 10);

  dWorldSetCFM(World, 0.1);
  dWorldSetERP(World, 0.9);}

  MSE.Log.Write('������ ����������������', Msg_Good);
  InitBool := True;
except
  MSE.Log.Write('������! ��� ������������� ������', Msg_Error);
end;
end;

procedure TPhys.Update;
var
  i1, i2: Integer;
begin
  MSE.Phys.Blocking := True;
  Inc(FPS);
  if MSE.GetTime-t>1000 then
  begin
    t := MSE.GetTime;
    sFPS := IntToStr(FPS);
    FPS := 0;
  end;


  obj_c := 0;
  obj_c_yes := 0;
  for i1:=0 to Map.ModelCount-1 do
  if (map.Model[i1].TypeObj=1) then
  begin
    map.Model[i1].cont_yes := False;
    Inc(obj_c);
  end;

  dSpaceCollide(Space, 0, nearCallback);
  dWorldStep(World, 1/UPS);
  dJointGroupEmpty(ContactGroup);
  MSE.Phys.Blocking := False;


  for i1:=0 to Map.ModelCount-1 do
  if (map.Model[i1].cont_yes) then
    obj_c_yes := obj_c_yes+1;

  if obj_c_yes=obj_c then
  begin
    Finish := Finish-Round(1000/UPS);
  end
  else
    Finish := win_t;
end;

function TPhys.CreateSphere(X, Y, Z, Radius, Mas: Single): TBody;
var
  Body: PdxBody;
  Geom: PdxGeom;
  Mass: TdMass;
  BodySpace: PdxSpace;
begin
try
  Body := dBodyCreate(World);

  dBodySetPosition(Body, X, Y, Z);
  Geom := dCreateSphere(nil, Radius);
  dMassSetSphere(Mass, 1, Radius);
  dMassAdjust(Mass, Mas);
  dBodySetMass(Body, @Mass);

  dGeomSetBody(Geom, Body);
  BodySpace := dSimpleSpaceCreate(Space);
  dSpaceSetCleanup(BodySpace, 0);
  dSpaceAdd(BodySpace, Geom);

  Result := Body;
except
  MSE.Log.Write('������! �������� ���� "�����" ��� ������', Msg_Error);
end;
end;

procedure TPhys.CreateBox(X, Y, Z, lx, ly, lz, Mas: Single; var B: TBody; var G: PdxGeom);
var
  Body: PdxBody;
  Geom: PdxGeom;
  Mass: TdMass;
  BodySpace: PdxSpace;
begin
try
  Body := dBodyCreate(World);

  dBodySetPosition (Body, X, Y, Z);
  Geom := dCreateBox(nil, lx, ly, lz);
  dGeomSetPosition (Geom, X, Y, Z);
  dMassSetBox(Mass, 1, lx, ly, lz);

  dMassAdjust(Mass, Mas);
  dBodySetMass(Body, @Mass);

  dGeomSetBody(Geom, Body);
  BodySpace := dSimpleSpaceCreate(Space);
  dSpaceSetCleanup(BodySpace, 0);
  dSpaceAdd(BodySpace, Geom);

  B := Body;
  G := Geom;
except
  MSE.Log.Write('������! �������� ���� "����" ��� ������', Msg_Error);
end;
end;

procedure TPhys.CreateBox(BBox: TBBox; Mas: Single; var B: TBody; var G: PdxGeom);
begin
  CreateBox(BBox.X, BBox.Y, BBox.Z, BBox.W, BBox.H, BBox.D, Mas, B, G);
end;

function TPhys.CreateBoxStatic(BBox: TBBox): PdxGeom;
begin
  CreateBoxStatic(BBox.X, BBox.Y, BBox.Z, BBox.W, BBox.H, BBox.D);
end;

function TPhys.CreateCapsule(X, Y, Z, R, L, Mas: Single): TBody;
var
  Body: PdxBody;
  Geom: PdxGeom;
  Mass: TdMass;
  BodySpace: PdxSpace;
  rr: TdMatrix3;
begin
try
  Body := dBodyCreate(World);

  dBodySetPosition (Body, X, Y, Z);
  Geom := dCreateCapsule(nil, R, L);
  dGeomSetPosition (Geom, X, Y, Z);
  dMassSetCapsule(Mass, 1, 1, R, L);

  dMassAdjust(Mass, Mas);
  dBodySetMass(Body, @Mass);

  dGeomSetBody(Geom, Body);
  BodySpace := dSimpleSpaceCreate(Space);
  dSpaceSetCleanup(BodySpace, 0);
  dSpaceAdd(BodySpace, Geom);

  Result := Body;

  dRFromAxisAndAngle (rr, 1, 0, 0, 1+Rad);
  dBodySetRotation(Body, rr);
except
  MSE.Log.Write('������! �������� ���� "�������" ��� ������', Msg_Error);
end;
end;

function TPhys.CreateCylinder(X, Y, Z, R, L, Mas: Single): TBody;
var
  Body: PdxBody;
  Geom: PdxGeom;
  Mass: TdMass;
  BodySpace: PdxSpace;
  rr: TdMatrix3;
begin
try
  Body := dBodyCreate(World);

  dBodySetPosition (Body, X, Y, Z);
  Geom := dCreateCylinder(nil, R, L);
  dGeomSetPosition (Geom, X, Y, Z);
  dMassSetCylinder(Mass, 1, 1, R, L);

  dMassAdjust(Mass, Mas);
  dBodySetMass(Body, @Mass);

  dGeomSetBody(Geom, Body);
  BodySpace := dSimpleSpaceCreate(Space);
  dSpaceSetCleanup(BodySpace, 0);
  dSpaceAdd(BodySpace, Geom);

  Result := Body;

  dRFromAxisAndAngle (rr, 1, 0, 0, 1+Rad);
  dBodySetRotation(Body, rr);
except
  MSE.Log.Write('������! �������� ���� "�������" ��� ������', Msg_Error);
end;
end;

function TPhys.CreateBoxStatic(X, Y, Z, lx, ly, lz: Single): PdxGeom;
var
  Geom: PdxGeom;
  BodySpace: PdxSpace;
begin
  Geom := dCreateBox(nil, lx, ly, lz);
  dGeomSetPosition(Geom, X, Y, Z);

  BodySpace := dSimpleSpaceCreate(Space);
  dSpaceSetCleanup(BodySpace, 0);
  dSpaceAdd(BodySpace, Geom);

  Result := Geom;
end;

function TPhys.CreateCapsuleBox(X, Y, Z, R, L, Mas: Single): TBody;
const
  Count=10;
var
  Body: array[1..Count] of PdxBody;
  i: Integer;
  Joint: array[1..Count] of TdJointID;
  rr: TdMatrix3;
begin
try
{  for i:=1 to Count do
  begin
    CreateBox(X, Y, Z, R, R, L-R, Mas, Body[i], @);

    dRFromAxisAndAngle (rr, 0, 1, 0, ((Rad*3)/Count)*i);
    dBodySetRotation(Body[i], rr);

    if i>1 then
    begin
      Joint[i] := dJointCreateFixed(World, 0);
      dJointAttach(Joint[i], Body[i], Body[i-1]);
      dJointSetFixed(Joint[i]);
    end;
  end;}

  Result := Body[1];

  {Body1 := CreateCylinder(X, Y, Z, R, L, Mas/2);
  Body2 := CreateBox(X, Y, Z, R, R, L, Mas);
 // Body2 := CreateCapsule(X, Y, Z, 0.01, 0.01, Mas/2);

  //dRFromAxisAndAngle (rr, 1, 0, 0, 1+Rad);
  //dBodySetRotation(Body2, rr);

  //��������� ����
  Joint := dJointCreateFixed(World, 0);
  dJointAttach(Joint, Body1, Body2);
  dJointSetFixed(Joint);

  Result := Body1;}
except
  MSE.Log.Write('������! �������� ���� "�������-����" ��� ������', Msg_Error);
end;
end;

function TPhys.GetBodyPos(Body: TBody): TVector3f;
var
  Tr: PdVector3;
begin
  Tr := dBodyGetPosition(Body);
  Result.X := Tr[0];
  Result.Y := Tr[1];
  Result.Z := Tr[2];
end;

function TPhys.GetBodyRot(Body: TBody): TVector3f;
var
  Tr: PdMatrix3;
begin
  Tr := dBodyGetRotation(Body);
  Result.X := Tr[9];
  Result.Y := Tr[0];
  Result.Z := Tr[2];
end;

function TPhys.GetBodyQuaternion(Body: TBody): TVector4;
var
  q: PdQuaternion;
begin
  q := dBodyGetQuaternion(Body);
  Result[0] := q[0];
  Result[1] := q[1];
  Result[2] := q[2];
  Result[3] := q[3];
end;

function SetDMatrix3(x,y,z:single): TdMatrix3;
begin
  result[0] := sin(Y)*cos(Z)+sin(X)*cos(Y)*sin(Z);
  result[1] := cos(X)*sin(Z);
  result[2] := sin(X)*sin(Y)*sin(Z)-cos(Z)*cos(Y);
  result[4] := sin(X)*cos(Y)*cos(Z)-sin(Y)*sin(Z);
  result[5] := cos(X)*cos(Z);
  result[6] := cos(Y)*sin(Z)+sin(X)*sin(Y)*cos(Z);
  result[8] := cos(X)*cos(Y);
  result[9] := -sin(X);
  result[10]:= cos(X)*sin(Y);
end;

procedure TPhys.GeomSetRotatioY(var Geom: PdxGeom; Angle: Single);
var
  R: TdMatrix3;
begin
  dRSetIdentity(R);
  dRFromAxisAndAngle(R, 0, 1, 0, Rad*Angle);
  //R := dGeomGetRotation(Geom)^;
//������� �������� ������ ��� y:
//[ cos a 0 -sin a 0 ]
//[ 0 1 0 0 ]
//[ sin a 0 cos a 0 ]
//[ 0 0 0 1 ]

  {R[0] := Cos(Angle);
  R[2] := -Sin(Angle);
  R[5] := 1;
  R[8] := Sin(Angle);
  R[10] := Cos(Angle);
  //R[15] := 1;}

  //R := dGeomGetRotation(Geom)^;
  //R := SetDMatrix3(1, Angle, 1);
  dGeomSetRotation(Geom, R);
  //R := (dBodyGetRotation(Cars.Hook_Body));
  //dBodySetRotation(Cars.Hook_Body, R^);
end;

function TPhys.GetBodyLinearVel(Body: TBody): TVector3f;
var
  Tr: PdVector3;
begin
  Tr := dBodyGetLinearVel(Body);
  Result.X := Tr[0];
  Result.Y := Tr[1];
  Result.Z := Tr[2];
end;

function TPhys.GetFPS: string;
begin
  Result := sFPS;
end;

function TPhys.GetObjectOrientation(Body: TBody): TMatrix;
var
  Pos: PdVector3;
  Rot: PdMatrix3;
  matrix: TMatrix;
begin
  Pos := dBodyGetPosition(Body);
  Rot := dBodyGetRotation(Body);

  matrix[0]  := Rot[0];
  matrix[1]  := Rot[4];
  matrix[2]  := Rot[8];
  matrix[3]  := 0;
  matrix[4]  := Rot[1];
  matrix[5]  := Rot[5];
  matrix[6]  := Rot[9];
  matrix[7]  := 0;
  matrix[8]  := Rot[2];
  matrix[9]  := Rot[6];
  matrix[10] := Rot[10];
  matrix[11] := 0;
  matrix[12] := Pos[0];
  matrix[13] := Pos[1];
  matrix[14] := Pos[2];
  matrix[15] := 1;

  Result := matrix;
end;

procedure TPhys.CreateTriMesh;
begin
try
  Mesh[Meshi] := dGeomTriMeshDataCreate;
  dGeomTriMeshDataBuildSimple(Mesh[Meshi], MeshV, CountV, MeshF, CountF);
  //dGeomTriMeshDataBuildSingle(Mesh, MeshV, 3 * sizeof(single), CountV, MeshF, CountF, 3 * sizeof(MeshF));
  MeshGeom[Meshi] := dCreateTriMesh(Space, Mesh[Meshi], nil, nil, nil);
except
  MSE.Log.Write('������! �������� ���� "������" ��� ������', Msg_Error);
end;
end;

procedure TPhys.AddObj(var Obj: TObj; Metod: Integer; Pos, Rot: TVector3f);
var
  i, k: Integer;
  Box: TBBox;
  R: TdMatrix3;
  MaxX, MaxZ: Single;
begin
  Box.W := Obj.BBox.W;
  Box.H := Obj.BBox.H;
  Box.D := Obj.BBox.D;
  Box.X := Obj.BBox.X+Pos.X;
  Box.Y := Obj.BBox.Y+Pos.Y;
  Box.Z := Obj.BBox.Z+Pos.Z;

  //Obj.Phys := 2;

  if Obj.Phys=1 then
  begin
    Obj.Geom := CreateBoxStatic(Box.X, Box.Y, Box.Z, Box.W, Box.H, Box.D);

    dRSetIdentity(R);
    if Obj.Rot.Y <> 0 then
      dRFromAxisAndAngle(R, 0, 1, 0, DegToRad(Obj.Rot.Y));
    if Obj.Rot.X <> 0 then
      dRFromAxisAndAngle(R, 1, 0, 0, DegToRad(Obj.Rot.X));
    if Obj.Rot.Z <> 0 then
      dRFromAxisAndAngle(R, 0, 0, 1, DegToRad(Obj.Rot.Z));
    dGeomSetRotation(Obj.Geom, R);

    MaxX := Box.X-Pos.X;
    MaxZ := Box.Z-Pos.Z;

    dGeomSetPosition(Obj.Geom, Pos.X+(MaxX*cos(DegToRad(Obj.Rot.Y)))+(MaxZ*sin(DegToRad(Obj.Rot.Y))),
                               Box.Y,
                               Pos.Z+(MaxZ*cos(DegToRad(Obj.Rot.Y)))-(MaxX*sin(DegToRad(Obj.Rot.Y))) );
  end;

  if Obj.Phys=2 then
  begin
    Inc(Meshi);
    CountV := Obj.VertexCount;
    CountF := Obj.FacesCount*3;

    GetMem(MSE.Phys.MeshV, MSE.Phys.CountV * SizeOf(TdVector3));
    for i:=0 to Obj.VertexCount-1 do begin
      MeshV[i][0] := Obj.vertex[i].X;
      MeshV[i][1] := Obj.vertex[i].Y;
      MeshV[i][2] := Obj.vertex[i].Z;
    end;

    GetMem(MSE.Phys.MeshF, MSE.Phys.CountF * SizeOf(Integer));
    i := 0;
    k := 0;
    repeat
      MSE.Phys.MeshF[k]   := Obj.VF[i][0];
      MSE.Phys.MeshF[k+1] := Obj.VF[i][1];
      MSE.Phys.MeshF[k+2] := Obj.VF[i][2];

      k := k+3;
      i := i+1;
    until i > Obj.FacesCount-1;
    MSE.Phys.CreateTriMesh;

    dRSetIdentity(R);
    dRFromAxisAndAngle(R, 0, 1, 0, DegToRad(Obj.Rot.Y));
    //dRFromAxisAndAngle(R, 1, 0, 0, DegToRad(Obj.Rot.X));
//    dRFromAxisAndAngle(R, 0, 0, 1, Obj.Rot.Z/Rad);
    dGeomSetRotation(MeshGeom[Meshi], R);

    MaxX := Box.X-Pos.X;
    MaxZ := Box.Z-Pos.Z;

    dGeomSetPosition(MeshGeom[Meshi], Pos.X, Pos.Y, Pos.Z );
  end;

  if Obj.Phys=100 then
  begin
    CreateBox(Box, 0.1, Obj.Body, Obj.Geom);

    dRSetIdentity(R);
    dRFromAxisAndAngle(R, 0, 1, 0, Obj.Rot.Y/Rad);
    dRFromAxisAndAngle(R, 1, 0, 0, Obj.Rot.X/Rad);
    //dRFromAxisAndAngle(R, 0, 0, 1, Obj.Rot.Z/Rad);


    MaxX := Box.X-Pos.X;
    MaxZ := Box.Z-Pos.Z;

    dBodySetPosition(Obj.Body, Pos.X+(MaxX*cos(Obj.Rot.Y/Rad))+(MaxZ*sin(Obj.Rot.Y/Rad)), Box.Y,
                               Pos.Z+(MaxZ*cos(Obj.Rot.Y/Rad))-(MaxX*sin(Obj.Rot.Y/Rad)) );
    dBodySetRotation(Obj.Body, R);
  end;
end;

procedure TPhys.AddModel(Model: TModel; Metod: Integer);
var
  i: Integer;
  old: Integer;
begin
  for i:=0 to Model.ObjCount-1 do
  begin
    old := Model.Obj[i].Phys;
    if Model.TypeObj=1 then
      Model.Obj[i].Phys := 100;

    if (Model.Obj[i].Phys=100) then
    begin
      if old>0 then
        AddObj(Model.Obj[i], Metod, Model.Pos, Model.Rot);
    end
    else
      AddObj(Model.Obj[i], Metod, Model.Pos, Model.Rot);
  end;
end;

procedure TPhys.AddMap(Map: TMap);
var
  i: Integer;
begin
  repeat until (MSE.Phys.Blocking=False);
  for i:=0 to Map.ModelCount-1 do
    AddModel(Map.Model[i], 1);
end;


end.
