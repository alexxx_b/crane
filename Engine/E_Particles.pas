unit E_Particles;

interface

uses
  Windows, OpenGL20, E_Type;

type
  TParticles = class
    public
      constructor Create;
      procedure Update;
      function NewSystem(Tex: Cardinal; Count: Cardinal; Length, Radius, Speed: Single; Glare, Alpha: Byte): TSmoke;
      procedure Draw(Smoke: TSmoke);
    private
      CountSmoke: Integer;
      Manager: array of TSmoke;
  end;

implementation

uses E_Engine;

constructor TParticles.Create;
begin
  CountSmoke := 0;
end;

procedure TParticles.Update;
var
  i, j: Integer;
begin
  for j:=0 to CountSmoke-1 do
  begin
    for i:=0 to Manager[j].Count do
    begin
      Manager[j].Point[i].Pos.Y := Manager[j].Point[i].Pos.Y+(((Random(10000)/500000)+(Manager[j].Point[i].Pos.Y/500))*Manager[j].Speed);
      Manager[j].Point[i].Pos.X := Manager[j].Point[i].Pos.X + (Random(2000)/100000-0.01);
      Manager[j].Point[i].Pos.Z := Manager[j].Point[i].Pos.Z + (Random(2000)/100000-0.01);

      if Manager[j].Point[i].Pos.Y > Manager[j].Length then
      begin
        if (Manager[j].Point[i].Alpha > 0) then
          Manager[j].Point[i].Alpha := Manager[j].Point[i].Alpha-Manager[j].Glare
        else
        begin
          Manager[j].Point[i].Pos.X := 0;
          Manager[j].Point[i].Pos.Y := 0;
          Manager[j].Point[i].Pos.Z := 0;
        end;
      end
      else
      if (Manager[j].Point[i].Alpha < 255) then
        Manager[j].Point[i].Alpha := Manager[j].Point[i].Alpha+Manager[j].Glare;
    end;
  end;
end;

function TParticles.NewSystem(Tex: Cardinal; Count: Cardinal; Length, Radius, Speed: Single; Glare, Alpha: Byte): TSmoke;
var
  i: Integer;
  Sm: TSmoke;
begin
  Sm.Texture := Tex;
  Sm.Count   := Count;
  Sm.Length  := Length;
  Sm.Radius  := Radius;
  Sm.Speed   := Speed;
  Sm.Glare   := Glare;
  Sm.Alpha   := Alpha;
  SetLength(Sm.Point, Sm.Count+1);

  for i:=0 to Sm.Count do
  begin
    Sm.Point[i].Alpha := 0;
    Sm.Point[i].Pos.X := 0;
    Sm.Point[i].Pos.Y := Random(Round(Sm.Length*1000))/1000;
    Sm.Point[i].Pos.Z := 0;
  end;

  Inc(CountSmoke);
  SetLength(Manager, CountSmoke);
  Result := Sm;
  Manager[CountSmoke-1] := Sm;
end;

procedure TParticles.Draw(Smoke: TSmoke);
var
  i: Integer;
begin
  glDisable(GL_LIGHTING);
  glDepthMask(False);
  MSE.Texture.Use(Smoke.Texture);

  for i:=0 to Smoke.Count do
  begin
    //MSE.OGL.Alpha := Smoke.Point[i].Alpha;
    glEnable(GL_BLEND);
    if Smoke.Alpha>Smoke.Point[i].Alpha then
      glColor4f(1.0, 1.0, 1.0, Smoke.Point[i].Alpha/255)
    else
      glColor4f(1.0, 1.0, 1.0, Smoke.Alpha/255);

    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GEQUAL, 0.0);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    MSE.Draw.BillBoard(Smoke.Point[i].Pos, 1+(Smoke.Point[i].Pos.Y*Smoke.Radius), 1+(Smoke.Point[i].Pos.Y*Smoke.Radius), Smoke.Texture);
  end;

  glDisable(GL_ALPHA_TEST);
  glDisable(GL_BLEND);
  //COGL.Blend(BT_NONE, 255);
  //MSE.OGL.Alpha := 255;
  glEnable(GL_LIGHTING);
  glDepthMask(True);
end;


end.
