unit E_System;

interface

uses
  Windows, MMSystem, SysUtils, psapi;

type
  TSystem = class
    public
      constructor Create;
      function GetWindowsDir: PChar;// �������, ��� ����������� Windows
      function GetSystemDir: PChar;// ��������� ������� Windows
      function GetOSName: PChar;// ��� ��
      function GetCompName: PChar;// ��� ����������
      function GetUserLogin: PChar;// ��� ������������

      function GetTotalPhys: Cardinal;//���������� ������ �����
      function GetAvailPhys: Cardinal;//���������� ������ ���������
      function GetTotalPageFile: Cardinal;//���� �������� �����
      function GetAvailPageFile: Cardinal;//���� �������� ���������
      function GetTotalVirtual: Cardinal;//����������� ������ �����
      function GetAvailVirtual: Cardinal;//����������� ������ ���������

      function GetCPUVendor: PChar;//������������� ���������
      function GetCPUName: PChar;//��� ����������
      function GetCPUFrequency: word;//������� ��������� � ��
      function CheckMMX: Boolean;
      function CheckMMXPlus: Boolean;
      function CheckSSE: Boolean;
      function CheckSSE2: Boolean;
      function CheckAMD3DNow: Boolean;
      function CheckAMD3DNowPlus: Boolean;

      function SetMode(W, H, BPP, Freq: Integer): Boolean; overload;
      function SetMode(s: string): Boolean; overload;
      function ModeGetW(s: string): Integer;
      function ModeGetH(s: string): Integer;
      function GetProgDir: string;

      function GetProcessMemory: Integer;
    private
  end;  

implementation

constructor TSystem.Create;
begin

end;

//> �� � Windows <//

function TSystem.GetWindowsDir: PChar;// �������, ��� ����������� Windows
var
  PRes: PChar;
  Res : word;
begin
  PRes := StrAlloc(255);
  Res  := GetWindowsDirectory(PRes, 255);
  if Res > 0 then Result := PRes;
end;

function TSystem.GetSystemDir: PChar;// ��������� ������� Windows
var
  PRes: PChar;
  Res : word;
begin
  PRes := StrAlloc(255);
  Res := GetSystemDirectory(PRes, 255);
  if Res > 0 then Result := PRes;
end;

function TSystem.GetOSName: PChar;// ��� ��
var
  PRes: PChar;
  Res : word;
  BRes: boolean;
  lpVersionInformation: TOSVersionInfo;
  c: string;
begin
  PRes := StrAlloc(255);
  lpVersionInformation.dwOSVersionInfoSize := SizeOf(TOSVersionInfo);
  BRes := GetVersionEx(lpVersionInformation);
  if BRes then
    with lpVersionInformation do
    case dwMajorVersion of
      4:
      begin
        if dwMinorVersion = 0 then
          c := 'Windows 95';
        if dwMinorVersion = 10 then
          c := 'Windows 98';
        if dwMinorVersion = 90 then
          c := 'Windows ME';
      end;
          
      3:
      begin
        if dwMinorVersion = 51 then
          c := 'Windows NT 3.51';
        if dwMinorVersion = 0 then
          c := 'Windows NT 4.0';
      end;

      5:
      begin
        if dwMinorVersion = 0 then
          c := 'Windows 2000';
        if dwMinorVersion = 1 then
          c := 'Windows XP';
      end;                    
    end;
  Result := PChar(c);
end;

function TSystem.GetProcessMemory: Integer;
var
  pmc: PProcess_Memory_Counters;
  cb: Integer;
begin
  cb:=SizeOf(_Process_Memory_Counters);
  try
    GetMem(pmc,cb);
    pmc^.cb:=cb;
    GetProcessMemoryInfo(GetCurrentProcess(),pmc,cb);
    Result := pmc.WorkingSetSize;
  finally
    FreeMem(pmc);
  end;
end;

function TSystem.GetProgDir: string;
begin
  Result := ExtractFilePath(ParamStr(0));
end;

function TSystem.GetCompName: PChar;// ��� ����������
var
  Size: cardinal;
  PRes: PChar;
  BRes: boolean;
begin
  Size := MAX_COMPUTERNAME_LENGTH + 1;
  PRes := StrAlloc(Size);
  BRes := GetComputerName(PRes, Size);
  if BRes then Result := PRes;
end;

function TSystem.GetUserLogin: PChar;// ��� ������������
var
  Size: cardinal;
  PRes: PChar;
  BRes: boolean;
begin
  Size := MAX_COMPUTERNAME_LENGTH + 1;
  PRes := StrAlloc(Size);
  BRes := GetUserName(PRes, Size);
  if BRes then Result := PRes;
end;

//> ���������� � ������ �������, � �� <//

function TSystem.GetTotalPhys: Cardinal;//���������� ������ �����
var
  lpMemoryStatus : TMemoryStatus;
begin
  lpMemoryStatus.dwLength := SizeOf(lpMemoryStatus);
  GlobalMemoryStatus(lpMemoryStatus);
  Result := lpMemoryStatus.dwTotalPhys;
end;

function TSystem.GetAvailPhys: Cardinal;//���������� ������ ���������
var
  lpMemoryStatus : TMemoryStatus;
begin
  lpMemoryStatus.dwLength := SizeOf(lpMemoryStatus);
  GlobalMemoryStatus(lpMemoryStatus);
  Result := lpMemoryStatus.dwAvailPhys;
end;

function TSystem.GetTotalPageFile: Cardinal;//���� �������� �����
var
  lpMemoryStatus : TMemoryStatus;
begin
  lpMemoryStatus.dwLength := SizeOf(lpMemoryStatus);
  GlobalMemoryStatus(lpMemoryStatus);
  Result := lpMemoryStatus.dwTotalPageFile;
end;

function TSystem.GetAvailPageFile: Cardinal;//���� �������� ���������
var
  lpMemoryStatus : TMemoryStatus;
begin
  lpMemoryStatus.dwLength := SizeOf(lpMemoryStatus);
  GlobalMemoryStatus(lpMemoryStatus);
  Result := lpMemoryStatus.dwAvailPageFile;
end;

function TSystem.GetTotalVirtual: Cardinal;//����������� ������ �����
var
  lpMemoryStatus : TMemoryStatus;
begin
  lpMemoryStatus.dwLength := SizeOf(lpMemoryStatus);
  GlobalMemoryStatus(lpMemoryStatus);
  Result := lpMemoryStatus.dwTotalVirtual;
end;

function TSystem.GetAvailVirtual: Cardinal;//����������� ������ ���������
var
  lpMemoryStatus : TMemoryStatus;
begin
  lpMemoryStatus.dwLength := SizeOf(lpMemoryStatus);
  GlobalMemoryStatus(lpMemoryStatus);
  Result := lpMemoryStatus.dwAvailVirtual;
end;

//> ���������� � ��������� <//

function TSystem.GetCPUVendor: PChar;//������������� ���������
var
  s1, s2, s3: array [0..3] of AnsiChar;
  TempVendor: string;
  i: integer;
begin
  asm
    push eax
    push ebx
    push ecx
    push edx
    mov eax,0
    db $0F,$A2 /// cpuid
    mov s1,ebx
    mov s2,edx
    mov s3,ecx
    pop edx
    pop ecx
    pop ebx
    pop eax
  end;
  TempVendor:='';
  for i:=0 to 3 do
    TempVendor:=TempVendor+s1[i];
  for i:=0 to 3 do
    TempVendor:=TempVendor+s2[i];
  for i:=0 to 3 do
    TempVendor:=TempVendor+s3[i];
    
  Result := PChar(TempVendor);
end;

function TSystem.GetCPUName: PChar;//��� ����������
var
  s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12: array[0..3] of AnsiChar;
  TempCpuName: string;
  i: integer;
begin
  asm
    push eax
    push ebx
    push ecx
    push edx
    mov eax,$80000002
    mov ebx,0
    mov ecx,0
    mov edx,0
    db $0F,$A2 /// cpuid
    mov s1,eax
    mov s2,ebx
    mov s3,ecx
    mov s4,edx
    mov eax,$80000003
    mov ebx,0
    mov ecx,0
    mov edx,0
    db $0F,$A2 /// cpuid
    mov s5,eax
    mov s6,ebx
    mov s7,ecx
    mov s8,edx
    mov eax,$80000004
    mov ebx,0
    mov ecx,0
    mov edx,0
    db $0F,$A2 /// cpuid
    mov s9,eax
    mov s10,ebx
    mov s11,ecx
    mov s12,edx
    pop edx
    pop ecx
    pop ebx
    pop eax
  end;
  TempCpuName:='';
  for i:=0 to 3 do
    TempCpuName:=TempCpuName+s1[i];
  for i:=0 to 3 do
    TempCpuName:=TempCpuName+s2[i];
  for i:=0 to 3 do
    TempCpuName:=TempCpuName+s3[i];
  for i:=0 to 3 do
    TempCpuName:=TempCpuName+s4[i];
  for i:=0 to 3 do
    TempCpuName:=TempCpuName+s5[i];
  for i:=0 to 3 do
    TempCpuName:=TempCpuName+s6[i];
  for i:=0 to 3 do
    TempCpuName:=TempCpuName+s7[i];
  for i:=0 to 3 do
    TempCpuName:=TempCpuName+s8[i];
  for i:=0 to 3 do
    TempCpuName:=TempCpuName+s9[i];
  for i:=0 to 3 do
    TempCpuName:=TempCpuName+s10[i];
  for i:=0 to 3 do
    TempCpuName:=TempCpuName+s11[i];
  for i:=0 to 3 do
    TempCpuName:=TempCpuName+s12[i];
  Result := PChar(TempCpuName);
end;

function TSystem.GetCPUFrequency: word;//������� ��������� � ��
var
  TimeStart: integer;
  TimeStop: integer;
  StartTicks: dword;
  EndTicks: dword;
  TotalTicks: dword;
  cpuSpeed: dword;
  NeverExit: Boolean;
begin
  TimeStart:=0;
  TimeStop:=0;
  StartTicks:=0;
  EndTicks:=0;
  TotalTicks:=0;
  cpuSpeed:=0;
  NeverExit:=True;
  TimeStart:=timeGetTime;
  while NeverExit do
  begin
    TimeStop:=timeGetTime;
    if ((TimeStop-TimeStart)>1) then
    begin
      asm
        xor eax,eax
        xor ebx,ebx
        xor ecx,ecx
        xor edx,edx
        db $0F,$A2 /// cpuid
        db $0F,$31 /// rdtsc
        mov StartTicks,eax
      end;
      Break;
    end;
  end;
  TimeStart:=TimeStop;
  while NeverExit do
  begin
    TimeStop:=timeGetTime;
    if ((TimeStop-TimeStart)>1000) then
    begin
      asm
        xor eax,eax
        xor ebx,ebx
        xor ecx,ecx
        xor edx,edx
        db $0F,$A2 /// cpuid
        db $0F,$31 /// rdtsc
        mov EndTicks,eax
      end;
      Break;
    end;
  end;
  TotalTicks:=EndTicks-StartTicks;
  cpuSpeed:=TotalTicks div 1000000;
  Result := cpuSpeed;
end;

function TSystem.CheckMMX: Boolean;
label
  NoMMX;
var
  TempCheck: dword;
begin
  TempCheck:=1;
  asm
    push eax
    push ebx
    push ecx
    push edx
    mov eax,1
    db $0F,$A2 /// cpuid
    test edx,$800000
    jz NoMMX
    mov edx,0
    mov TempCheck,edx
    NoMMX:
    pop edx
    pop ecx
    pop ebx
    pop eax
  end;
  Result := (TempCheck=0);
end;

function TSystem.CheckMMXPlus: Boolean;
label
  NoMMXplus;
var
  TempCheck: dword;
begin
  TempCheck:=1;
  asm
    push eax
    push ebx
    push ecx
    push edx
    mov eax,$80000001
    mov ebx,0
    mov ecx,0
    mov edx,0
    db $0F,$A2 /// cpuid
    test edx,$400000
    jz NoMMXplus
    mov edx,0
    mov TempCheck,edx
    NoMMXplus:
    pop edx
    pop ecx
    pop ebx
    pop eax
  end;
  Result := (TempCheck=0);
end;

function TSystem.CheckSSE: Boolean;
label
  NoSSE;
var
  TempCheck: dword;
begin
  TempCheck:=1;
  asm
    push eax
    push ebx
    push ecx
    push edx
    mov eax,1
    db $0F,$A2 /// cpuid
    test edx,$2000000
    jz NoSSE
    mov edx,0
    mov TempCheck,edx
    NoSSE:
    pop edx
    pop ecx
    pop ebx
    pop eax
  end;
  Result := (TempCheck=0);
end;

function TSystem.CheckSSE2: Boolean;
label
  NoSSE2;
var
  TempCheck: dword;
begin
  TempCheck:=1;
  asm
    push eax
    push ebx
    push ecx
    push edx
    mov eax,1
    db $0F,$A2 /// cpuid
    test edx,$4000000
    jz NoSSE2
    mov edx,0
    mov TempCheck,edx
    NoSSE2:
    pop edx
    pop ecx
    pop ebx
    pop eax
  end;
  Result := (TempCheck=0);
end;

function TSystem.CheckAMD3DNow: Boolean;
label
  NoAMD3DNow;
var
  TempCheck: dword;
begin
  TempCheck:=1;
  asm
    push eax
    push ebx
    push ecx
    push edx
    mov eax,$80000001
    mov ebx,0
    mov ecx,0
    mov edx,0
    db $0F,$A2 /// cpuid
    test edx,$80000000
    jz NoAMD3DNow
    mov edx,0
    mov TempCheck,edx
    NoAMD3DNow:
    pop edx
    pop ecx
    pop ebx
    pop eax
  end;
  Result := (TempCheck=0);
end;

function TSystem.CheckAMD3DNowPlus: Boolean;
label
  NoAMD3DNowPlus;
var
  TempCheck: dword;
begin
  TempCheck:=1;
  asm
    push eax
    push ebx
    push ecx
    push edx
    mov eax,$80000001
    mov ebx,0
    mov ecx,0
    mov edx,0
    db $0F,$A2 /// cpuid
    test edx,$40000000
    jz NoAMD3DNowPlus
    mov edx,0
    mov TempCheck,edx
    NoAMD3DNowPlus:
    pop edx
    pop ecx
    pop ebx
    pop eax
  end;
  Result := (TempCheck=0);
end;

////////////////////////////////////////////////////////////////////////////////
//������������� ���������� ������, � ��������� ������� ����� �������� � ���
function TSystem.SetMode(W, H, BPP, Freq: Integer): Boolean;
var
  Rect  : TRect;
  dm: DevMode;
  hr: HRESULT;
begin
  FillChar(dm, sizeof(dm),0);
  dm.dmSize             := sizeof(DevMode);
  dm.dmFields           := DM_PELSWIDTH or DM_PELSHEIGHT or DM_BITSPERPEL or DM_DISPLAYFREQUENCY;
  dm.dmPelsWidth        := W;
  dm.dmPelsHeight       := H;
  dm.dmBitsPerPel       := BPP;
  dm.dmDisplayFrequency := Freq;
  hr                    := ChangeDisplaySettings(dm,CDS_FULLSCREEN);
end;

function TSystem.SetMode(s: string): Boolean;
var
  W, H, BPP, Freq, i: Integer;
  s1, s2: string;
begin
  W := 0;
  H := 0;
  BPP := 0;
  Freq := 0;
  s1 := s;
  s2 := '';
  for i:=1 to Length(s1) do
    if (s1[i]<>'*') and (s1[i]<>' ') and (s1[i]<>'�') then
      s2 := s2+s1[i]
    else
    begin
      if W=0 then W := StrToInt(s2)
      else
      if H=0 then H := StrToInt(s2)
      else
      if BPP=0 then BPP := StrToInt(s2)
      else
      if Freq=0 then Freq := StrToInt(s2);
      s2 := '';
    end;

    SetMode(W, H, BPP, Freq);
    {MSE.Window.Width := W;
    MSE.Window.Height := H;}
end;

function TSystem.ModeGetW(s: string): Integer;
var
  W, H, BPP, Freq, i: Integer;
  s1, s2: string;
begin
  W := 0;
  H := 0;
  BPP := 0;
  Freq := 0;
  s1 := s;
  s2 := '';
  for i:=1 to Length(s1) do
    if (s1[i]<>'*') and (s1[i]<>' ') and (s1[i]<>'�') then
      s2 := s2+s1[i]
    else
    begin
      if W=0 then W := StrToInt(s2)
      else
      if H=0 then H := StrToInt(s2)
      else
      if BPP=0 then BPP := StrToInt(s2)
      else
      if Freq=0 then Freq := StrToInt(s2);
      s2 := '';
    end;

    Result := W;
end;

function TSystem.ModeGetH(s: string): Integer;
var
  W, H, BPP, Freq, i: Integer;
  s1, s2: string;
begin
  W := 0;
  H := 0;
  BPP := 0;
  Freq := 0;
  s1 := s;
  s2 := '';
  for i:=1 to Length(s1) do
    if (s1[i]<>'*') and (s1[i]<>' ') and (s1[i]<>'�') then
      s2 := s2+s1[i]
    else
    begin
      if W=0 then W := StrToInt(s2)
      else
      if H=0 then H := StrToInt(s2)
      else
      if BPP=0 then BPP := StrToInt(s2)
      else
      if Freq=0 then Freq := StrToInt(s2);
      s2 := '';
    end;

    Result := H;
end;


end.
