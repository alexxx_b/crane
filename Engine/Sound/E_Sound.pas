unit E_Sound;

interface

uses
  Windows, OpenAL, Alut, E_Type, Classes;

type
  TSounds = class
    public
      Enable: Boolean;
      mVel: array[0..3] of TALfloat;
      mPos: array[0..3] of TALfloat;
      mLooped: Boolean;
      Extensions: String;

      constructor Create;
      destructor Destroy;
      procedure Init(Dll, DeviceName: PAnsiChar);
      procedure Update;
      function Open(FileName: String; Looped: Boolean): TSound;
      procedure Play(Sound: TSound);
      procedure Stop(Sound: TSound);
      procedure Pause(Sound: TSound);
      procedure SetSourcePos(Sound: TSound; X, Y, Z: Single);
      procedure SetGlobalPower(Power: Single);
    private
      MainDevice: TALCdevice; //���������� ������� �������� �� ���������� ���������������
      MainContext: TALCcontext; //���������� ���������� �� ��������

      // 5 ���������� ���� ����� ����� ��� ��� �������� ����� � �����
      Format: TALenum;
      Data: TALvoid;
      Size: TALsizei;
      Freq: TALsizei;
      Loop: TALint;
  end;

const
  AL_FORMAT_VORBIS_EXT = $10003;  

var
  SourcePos: array [0..2] of TALfloat= (0.0, 0.0, 0.0); //������� ���������
  SourceVel: array [0..2] of TALfloat= (0.0, 0.0, 0.0); //�������� ���������

  //�� �� ������� ��� � � ����������, X,Y,Z � ���� ��������
  ListenerPos: array [0..2] of TALfloat= (0.0, 0.0, 0.0);  //�������
  ListenerVel: array [0..2] of TALfloat= (0.0, 0.0, 0.0);  //��������
  ListenerOri: array [0..5] of TALfloat= (0.0, 0.0, -1.0, 0.0, 1.0, 0.0); //����������

implementation

uses E_Engine, SysUtils, Ogg, VorbisFile, Codec;

constructor TSounds.Create;
begin
  Enable := False;
end;

destructor TSounds.Destroy;
begin
  alcMakeContextCurrent(nil);
  alcDestroyContext(MainContext);
  alcCloseDevice(MainDevice);
end;

procedure TSounds.Init(Dll, DeviceName: PAnsiChar);
begin
try
  InitOpenAL(Dll);

  MainDevice := alcOpenDevice(DeviceName);// ��������� �������� �� ��������� ����������
  MainContext := alcCreateContext(MainDevice, nil);// ������� �������� ����������
  alcMakeContextCurrent(MainContext);// ������ �������� �������

  Extensions := alGetString(AL_EXTENSIONS);
  MSE.Log.Write('���� ���������������', Msg_Good);
  MSE.Log.Write('�������������� ���������� �������� �����: '+Extensions, Msg_Good);
  Enable := True;
except
  MSE.Log.Write('������! ������������� �����', Msg_Error);
end;  
end;

procedure TSounds.Update;
begin
  if Enable then
  begin
    ListenerPos[0] := MSE.Camera.Pos.X;
    ListenerPos[1] := MSE.Camera.Pos.Y;
    ListenerPos[2] := MSE.Camera.Pos.Z;

    alListenerfv(AL_POSITION, @listenerPos);
  end;
end;

function TSounds.Open(FileName: String; Looped: Boolean): TSound;
//const
  //DYNBUF_SIZE = 4;
  //NUM_OF_DYNBUF = 4;
var
  FirstBuffer: TALuint;
  FirstSource: TALuint;
      
  cb: ov_callbacks;
  mVF: OggVorbis_File;
  OGGFile: TMemoryStream;
  OGGExt: TALboolean;
  BlockSize, DynBuffs: Integer;
begin
try
  if Enable then
  begin
  OGGExt := alIsExtensionPresent(PAnsiChar('AL_EXT_vorbis'));
   
  alGenBuffers(1, @FirstBuffer);
  alGenSources(1, @FirstSource);

  //���� ���� ������� �����
  if ExtractFileExt(FileName) = '.wav' then
  begin
    alutLoadWAVFile(FileName, format, data, size, freq, loop);
    alBufferData(FirstBuffer,format,data,size,freq);
    alutUnloadWAV(format,data,size,freq);
  end;

  //���� ���� ������� ������
  if (ExtractFileExt(FileName) = '.ogg') and (OGGExt) then
  begin
    cb.close_func := ops_close_func;
    cb.read_func  := ops_read_func;
    cb.seek_func  := ops_seek_func;
    cb.tell_func  := ops_tell_func;

    OGGFile := TMemoryStream.Create;
    OGGFile.LoadFromFile(FileName);

    if ov_open_callbacks(OggFile, mVF, nil, -1, cb) < 0 then
    begin
      MSE.Log.Write('������! � �������� ����� '+FileName, Msg_Error);
      Result := 0;
      Exit;
    end;  

    AlBufferData(FirstBuffer, AL_FORMAT_VORBIS_EXT, OGGFile.Memory, OGGFile.Size, freq);
    OGGFile.Free;

    // ������ ����� �����
    {BlockSize  := DYNBUF_SIZE;
    // ���������� ������� � ������� ������
    DynBuffs := NUM_OF_DYNBUF;
    alSourcei(FirstSource, AL_LOOPING, AL_FALSE);}
  end;

  alSourcei(FirstSource, AL_BUFFER, FirstBuffer);//��������� ��� �������� � �������
  
  alSourcefv(FirstSource, AL_POSITION, @SourcePos);//������������� �������
  alSourcefv(FirstSource, AL_VELOCITY, @SourceVel);//�������������  ��������
  alSourcei(FirstSource, AL_LOOPING, Integer(Looped));//������������

  Result := FirstSource;

  MSE.Log.Write('���� '+FileName+' ��������', Msg_Good);
  end
  else
    MSE.Log.Write('���� '+FileName+' �� ��������, �.�. ������ ����� �� ���������������', Msg_Warning);
except
  MSE.Log.Write('������! ���� '+FileName+' �� ��������', Msg_Error);
end;  
end;

procedure TSounds.Play(Sound: TSound);
begin
  if Enable then
  begin
    alSourcePlay(Sound);
  end;
end;

procedure TSounds.Stop(Sound: TSound);
begin
  if Enable then
  begin
    alSourceStop(Sound);
  end;  
end;

procedure TSounds.Pause(Sound: TSound);
begin
  if Enable then
  begin
    alSourcePause(Sound);
  end;
end;

procedure TSounds.SetSourcePos(Sound: TSound; X, Y, Z: Single);
begin
  if Enable then
  begin
    SourcePos[0] := X;
    SourcePos[1] := Y;
    SourcePos[2] := Z;

    alSourcefv(Sound, AL_POSITION, @SourcePos);
  end;  
end;

procedure TSounds.SetGlobalPower(Power: Single);
begin
  alListenerf(AL_GAIN, Power);
end;


end.
