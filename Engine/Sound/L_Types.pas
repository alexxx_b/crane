{
   Linderdaum 3D engine data types
   (Part of Linderdaum 3D Engine)
   Version 0.15.22
   (07/01/2004)
   (C) Kosarevsky Sergey, 2002-2004
   netsurfer@au.ru
}

Unit L_Types;

Interface

Uses SysUtils;

Const MAX_ENTITIES=1000;

//Type PPointer=^Pointer;

Type Lfloat=Single;
     Ldouble=Double;
     Luint=LongWord;
     Lsizei=LongInt;
     Lclampf=Single;
     Lclampd=Double;
     Lbyte=ShortInt; 
     Lubyte=Byte;
     Lenum=Cardinal;
     Lboolean=Boolean;
     Lbitfield=Cardinal;
     Lshort=SmallInt;
     Lint=Longint;
     Lushort=Word;

     PLvoid=Pointer;
     PLenum=^Lenum;
     PLsizei=^Lsizei;
     PLboolean=^Lboolean;
     PLbyte=^Lbyte;
     PLshort=^Lshort;
     PLint=^Lint;
     PLubyte=^Lubyte;
     PLushort=^Lushort;
     PLuint=^Luint;
     PLfloat=^Lfloat;
     PLclampf=^Lclampf;
     PLdouble=^Ldouble;

Type pLHandle = ^LHandle;
     LHandle  = Integer;
     ppLChar  = ^pLChar;
     pLChar   = ^LChar;
     LChar    = Char;


Type pMatrix3 = ^tMatrix3;
     pMatrix4 = ^tMatrix4;

     tMatrix3 = Array[1..3,1..3] Of Lfloat;
     tMatrix4 = Array[1..4,1..4] Of Lfloat;

Type pVector2=^tVector2;
     pVector3=^tVector3;
     pVector4=^tVector4;
    
     tVector2=Record
        Case Longint Of
           1:(X,Y:Lfloat);
           2:(U,V:Lfloat);
           3:(Vec:Array[1..2] Of Longint);
     End;

     tPoint=Record
        X,Y:Longint;
     End;

     tVector3=Record
        Case Longint Of
           1:(X,Y,Z:Lfloat);
           2:(Vec:Array[1..3] Of Lfloat);
     End;

     tVector4=Record
        Case Longint Of
           1:(X,Y,Z,W:Lfloat);
           2:(Vec:Array[1..4] Of Lfloat);
     End;

Type pBoundingBox=^tBoundingBox;
     tBoundingBox=Record
        Min,Max:tVector3;        // VIKTOR : obsolete !!!

        Center:tVector3;
        W,H,D:Lfloat;
     End;

Type pBoundingSphere=^tBoundingSphere;
     tBoundingSphere=Record
        Center:tVector3;
        Radius:Lfloat;
     End;

Const IDENTITY_MATRIX3:tMatrix3=((1,0,0),
                                 (0,1,0),
                                 (0,0,1));
      IDENTITY_MATRIX4:tMatrix4=((1,0,0,0),
                                 (0,1,0,0),
                                 (0,0,1,0),
                                 (0,0,0,1));
      ZERO_MATRIX3:tMatrix3=((0,0,0),
                             (0,0,0),
                             (0,0,0));
      ZERO_MATRIX4:tMatrix4=((0,0,0,0),
                             (0,0,0,0),
                             (0,0,0,0),
                             (0,0,0,0));
      ZERO_VECTOR3:tVector3=(X:0;Y:0;Z:0);
      ZERO_VECTOR4:tVector4=(X:0;Y:0;Z:0;W:0);
      IDENTITY_VECTOR3:tVector3=(X:1;Y:1;Z:1);
      IDENTITY_VECTOR4:tVector4=(X:1;Y:1;Z:1;W:1);

Implementation

Begin
End.

{
 * 07/01/2004
     IDENTITY_VECTOR3
     IDENTITY_VECTOR4
 * 18/11/2003
     tBoundingSphere
 * 17/05/2002
     Vectors
 * 16/05/2002
     Matrices
 * 12/05/2002
     It's here
}