unit E_Type;

interface

uses
  ODEImport, modelki, MatrixMaths, OpenGL20, Windows, Messages;

type
  TVector3f = record
    X, Y, Z: Single;
  end;
  TVector3d = record
    X, Y, Z: Double;
  end;
  TVector4 = array[0..3] of Single;
  TVector4f = record
    X, Y, W, H: Single;
  end;

  TVector4fa = array [0..3] of Single;

  TVector2f = record
    X, Y: Single;
  end;
  TVector2i = record
    X, Y: Integer;
  end;

  TColor4f = record
    R, G, B, A: Single;
  end;

  TFaces_i = array [0..2] of Integer;
  TFaces_w = array [0..2] of Word;

//������
type
  TMatrix = array[0..15] of Single;
//  TGeom
  TBody = PdxBody;

  TRope = record
    Count: Integer;
    SectionLen: Single;
    Body: array of TBody;
    Geom: array of PdxGeom;
    Joint: array of TdJointID;
    Remainder: Single;
  end;

type
  TFont = record
    W, H: Single;
    Texture: Cardinal;
    Key: array[0..255] of Cardinal;
  end;

  TSkyBox = Cardinal;
  TTex = Cardinal;

  TBBox = record
    X, Y, Z, W, H, D: Single;
  end;    

  TRGB = record
    R, G, B: Single;
  end;

  TRGBA = record
    R, G, B, A: Single;
  end;

//������
type
  TTVector3f = array[Word] of TVector3f;
  PVector3f = ^TTVector3f;
  TTVector2f = array[Word] of TVector2f;
  PVector2f = ^TTVector2f;
  TTColor4f = array[Word] of TColor4f;
  PColor4f = ^TTColor4f;
  TTFaces_w = array[Word] of TFaces_w;
  TTFaces_i = array[Word] of TFaces_i;
  PFaces_w = ^TTFaces_w;
  PFaces_i = ^TTFaces_i;
  //�������� ����� ������
  TObj = record
    Draw: Boolean;
    List: Cardinal;
    Vertex  : PVector3f;
    Normal  : PVector3f;
    TexCoord: PVector2f;
    VF, TF  : PFaces_w;
    Name: string;
    TextureName: string;
    Texture: TTex;
    VBOv, VBOf: Cardinal;
    VertexCount, FacesCount, TexCoordCount: Integer;
    BBox: TBBox;
    Color: TRGB;
    Alpha, Alpha2: Byte;
    Phys, Transparency: Byte;
    Pos, Rot: TVector3f;
    Body: TBody;
    Geom: PdxGeom;
  end;

  //������
  TModel = record
    Path: string;
    Name: string;
    ObjCount: Integer;
    Obj: array of TObj;
    BBox: TBBox;
    Pos, Rot: TVector3f;
    Matrix: TMatrix;
    TypeObj: Integer;//1-����
    cont_yes: Boolean;//���� ���� �� ������ ��������
  end;

  //�����
  TMap = record
    Name: string;
    ModelCount: Integer;
    Model: array of TModel;
    Pos, Rot: TVector3f;
  end;

  TBlendType = Integer;

  //SMD
  Sustav = record
    Otnosit: Mat;
    Absolut: Mat;
    Tochka : Vertex;
  end;

  TSMDModel = object
    AnimCount: Integer;
    AnimNumber: Integer;
    Scale: Single;
    Model: TModelka;
    Anim: array of TAnimacia;

      Tex: array of Cardinal;  // ��������
      NameTex: array of String;

      TimeKadr: Single;
      Time1, Time2: Integer;
      Kadr: Integer;
      SledKadr: Integer;

      TempPovorot: TPovorot;
      TempPeremesh: TPeremesh;

      Sustavi: array of Sustav;
      Svernuli: array of TPoligon;

      ttt: Cardinal;
  end;

//����
  TSound = Cardinal;

//�������
  TShader = GLHandleARB;

  //�������
  TPoints = record
    Pos: TVector3f;
    Alpha: Byte;
  end;

  TSmoke = record
    Texture: Cardinal;
    Count: Integer;
    Point: array of TPoints;
    Length: Single;
    Radius: Single;
    Speed: Single;
    Glare: Byte;
    Alpha: Byte;
  end;

  TPlayer = record
    Pos, Rot: TVector3f;
    lx, ly, lz: Single;
    Acc, dAcc, MaxAcc, MaxAccRun: Single;//���������
    Jump: Single;
    SightTexture: Cardinal;
  end;

  TKirka = record
    Model: TModel;
    Angle, AngleMax, AngleSpeed: Single;
  end;

const
  BT_NONE = 0;
  BT_SUB  = 1;
  BT_ADD  = 2;
  BT_MULT = 3;

  CF_None = 0;
  CF_Front = 1;
  CF_Back = 2;
  CF_Front_and_Back = 3;

  //���
  Msg_Good = 0;
  Msg_Error = 1;
  Msg_Warning = 2;

  MSG_LOAD = WM_USER + 1;
  MSG_SAVE = WM_USER + 2;
  MSG_SHOW_CURSOR = WM_USER + 3;
  MSG_HIDE_CURSOR = WM_USER + 4;

implementation

end.
