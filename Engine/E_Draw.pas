unit E_Draw;

interface

uses
  Windows, OpenGL20, Math, E_Type;

type
  TDraw = class
    public
      procedure Init;
      
      function CreateSkyBox(Dir: String): TSkyBox;
      procedure SkyBox(Id: TSkyBox);
      procedure SkyDome(Tex: TTex);
      function CreateFont(Dir: PChar; W, H: Single): TFont;
      procedure DrawText(Font: TFont; S: string; X, Y: Integer); overload;
      procedure DrawText(Font: TFont; S: string; X, Y: Integer; R, G, B: Single); overload;
      procedure SetFog(R, G, B: Byte; zNear, zFar: Single);
      procedure FogVisible(Param: Boolean);
      procedure Sphere(R: Single; Tex: TTex);
      procedure Box(lx, ly, lz: Single; Tex: TTex); overload;
      procedure Box(BBox: TBBox; Tex: TTex); overload;
      procedure Box(BBox: TBBox;  R, G, B: Single; A: Byte = 255); overload;
      procedure BoxLine(BBox: TBBox; Size: Integer); overload;
      procedure BoxLine(BBox: TBBox; Size: Integer; R, G, B: Single); overload;
      procedure Cylinder(R, L: Single; Tex: TTex);
      procedure Sprite3D(Size: Single; Tex: TTex);
      procedure BillBoard(Pos: TVector3f; SizeX, SizeY: Single; Tex: TTex); overload;
      procedure BillBoard(Pos: TVector3f; SizeX, SizeY: Single; Tex: TTex; A: Integer); overload;

      procedure Sprite2D(X, Y, W, H: Single; Tex: TTex);
      procedure Cursor(W, H: Single; Tex: TTex);
      procedure Sight(W, H: Single; Tex: TTex);

      procedure Line(v1, v2: TVector3f; Color: TRGB; Size: Integer); overload;
      procedure Line(X1, Y1, Z1, X2, Y2, Z2: Single; Color: TRGB; Size: Integer); overload;
    private
      SphereID: Cardinal;
      Sprite3DID: Cardinal;
      BoxID: Cardinal;
      SkyDomeID: Cardinal;
  end;

var
  im: Integer;

implementation

uses SysUtils, E_Engine;

procedure TDraw.Init;
var
  d: GLfloat;
var
  i, j: Integer;
  theta1, theta2, theta3: Single;
  x, y, z, px, py, pz: Single;
  CX, CY, CZ, R: Single;
  N : Integer;
begin
  //��������� �������
  Sprite3DID := glGenLists(1);
  glNewList(Sprite3DID, GL_COMPILE);
    glBegin(GL_QUADS);
      glTexCoord2f(0, 0); glVertex3f(-0.5, -0.5, 0);
      glTexCoord2f(1, 0); glVertex3f(0.5, -0.5, 0);
      glTexCoord2f(1, 1); glVertex3f(0.5, 0.5, 0);
      glTexCoord2f(0, 1); glVertex3f(-0.5, 0.5, 0);
    glEnd;
  glEndList;

  //������ ��������� �����, �������� 1*1*1
  BoxID := glGenLists(1);
  d := 0.5;
  glNewList(BoxID, GL_COMPILE);
    glBegin(GL_QUADS);
      // Front Face
      glNormal3f( 0.0, 0.0, 1.0);
      glTexCoord2f(0.0, 0.0); glVertex3f(-d, -d,  d);
      glTexCoord2f(1.0, 0.0); glVertex3f( d, -d,  d);
      glTexCoord2f(1.0, 1.0); glVertex3f( d,  d,  d);
      glTexCoord2f(0.0, 1.0); glVertex3f(-d,  d,  d);
      // Back Face
      glNormal3f( 0.0, 0.0,-1.0);
      glTexCoord2f(1.0, 0.0); glVertex3f(-d, -d, -d);
      glTexCoord2f(1.0, 1.0); glVertex3f(-d,  d, -d);
      glTexCoord2f(0.0, 1.0); glVertex3f( d,  d, -d);
      glTexCoord2f(0.0, 0.0); glVertex3f( d, -d, -d);
      // Top Face
      glNormal3f( 0.0, 1.0, 0.0);
      glTexCoord2f(0.0, 1.0); glVertex3f(-d,  d, -d);
      glTexCoord2f(0.0, 0.0); glVertex3f(-d,  d,  d);
      glTexCoord2f(1.0, 0.0); glVertex3f( d,  d,  d);
      glTexCoord2f(1.0, 1.0); glVertex3f( d,  d, -d);
      // Bottom Face
      glNormal3f( 0.0,-1.0, 0.0);
      glTexCoord2f(1.0, 1.0); glVertex3f(-d, -d, -d);
      glTexCoord2f(0.0, 1.0); glVertex3f( d, -d, -d);
      glTexCoord2f(0.0, 0.0); glVertex3f( d, -d,  d);
      glTexCoord2f(1.0, 0.0); glVertex3f(-d, -d,  d);
      // Right face
      glNormal3f( 1.0, 0.0, 0.0);
      glTexCoord2f(1.0, 0.0); glVertex3f( d, -d, -d);
      glTexCoord2f(1.0, 1.0); glVertex3f( d,  d, -d);
      glTexCoord2f(0.0, 1.0); glVertex3f( d,  d,  d);
      glTexCoord2f(0.0, 0.0); glVertex3f( d, -d,  d);
      // Left Face
      glNormal3f(-1.0, 0.0, 0.0);
      glTexCoord2f(0.0, 0.0); glVertex3f(-d, -d, -d);
      glTexCoord2f(1.0, 0.0); glVertex3f(-d, -d,  d);
      glTexCoord2f(1.0, 1.0); glVertex3f(-d,  d,  d);
      glTexCoord2f(0.0, 1.0); glVertex3f(-d,  d, -d);
    glEnd();
  glEndList;

  //������ ��������� �����
  CX := 0;
  CY := 0;
  CZ := 0;
  R := 1;
  N := 20;

  SphereID := glGenLists(1);
  glNewList(SphereID, GL_COMPILE);

  for j:=0 to N DIV 2-1 do
  begin
    theta1 := J*2*PI/N - PI/2;
    theta2 := (J+1)*2*PI/n - PI/2;

    glBegin(GL_TRIANGLE_STRIP);
      for i:=0 to N do
      begin
        theta3 := i*2*PI/N;
        x := cos(theta2) * cos(theta3);
        y := sin(theta2);
        z := cos(theta2) * sin(theta3);
        px := CX + R*x;
        py := CY + R*y;
        pz := CZ + R*z;

        glNormal3f(X, Y, Z);
        glTexCoord2f(1-I/n, 2*(J+1)/n);
        glVertex3f(px,py,pz);

        X := cos(theta1) * cos(theta3);
        Y := sin(theta1);
        Z := cos(theta1) * sin(theta3);
        px := CX + R*X;
        py := CY + R*Y;
        pz := CZ + R*Z;

        glNormal3f(X, Y, Z);
        glTexCoord2f(1-i/n, 2*j/n);
        glVertex3f(px, py, pz);
      end;
    glEnd;
  end;
  glEndList();

  //������ ��������� SkyDome
  CX := 0;
  CY := 0;
  CZ := 0;
  R := 10;
  N := 32;
    
  SkyDomeID := glGenLists(1);
  glNewList(SkyDomeID, GL_COMPILE);
  for j:=8 to N DIV 2 -1 do
  begin
    theta1 := j*2*PI/N - PI/2;
    theta2 := (j+1)*2*PI/n - PI/2;

    glBegin(GL_TRIANGLE_STRIP);
      for i:=0 to N do
      begin
        theta3 := i*2*PI/N;
        x := cos(theta2) * cos(theta3);
        y := sin(theta2);
        z := cos(theta2) * sin(theta3);
        px := CX + R*x;
        py := CY + R*y;
        pz := CZ + R*z;

        glNormal3f(x, y, z);
        glTexCoord2f(1-i/n, 2*(j+1)/n);
        glVertex3f(px, py, pz);

        x := cos(theta1) * cos(theta3);
        y := sin(theta1);
        z := cos(theta1) * sin(theta3);
        px := CX + R*x;
        py := CY + R*y;
        pz := CZ + R*z;

        glNormal3f(x, y, z);
        glTexCoord2f(1-i/n, 2*j/n);
        glVertex3f(px, py, pz);
      end;
    glEnd;
  end;
  glEndList;
end;

function TDraw.CreateSkyBox(dir: String): TSkyBox;
const
  d = 10;
var
  SkyBoxTexture: array[0..5] of Cardinal;
begin
  MSE.Texture.Clamp := True;
  SkyBoxTexture[0] := MSE.Texture.Load(dir+'\1.jpg');
  SkyBoxTexture[1] := MSE.Texture.Load(dir+'\2.jpg');
  SkyBoxTexture[2] := MSE.Texture.Load(dir+'\3.jpg');
  SkyBoxTexture[3] := MSE.Texture.Load(dir+'\4.jpg');
  SkyBoxTexture[4] := MSE.Texture.Load(dir+'\5.jpg');
  SkyBoxTexture[5] := MSE.Texture.Load(dir+'\6.jpg');
  MSE.Texture.Clamp := False;

  Result := glGenLists(1);
  glNewList(Result, GL_COMPILE);
  glPushMatrix;
  //�������
  glBindTexture(GL_TEXTURE_2D, SkyBoxTexture[5]);
    glbegin(GL_QUADS);
      glTexCoord2f(0, 0); glVertex3f(-d,-d,-d);
      glTexCoord2f(1, 0); glVertex3f( d,-d,-d);
      glTexCoord2f(1, 1); glVertex3f( d, d,-d);
      glTexCoord2f(0, 1); glVertex3f(-d, d,-d);
    glend;
  //� ����
  glBindTexture(GL_TEXTURE_2D, SkyBoxTexture[2]);
    glbegin(GL_QUADS);
      glTexCoord2f(0, 0); glvertex3f( d,-d, d);
      glTexCoord2f(1, 0); glvertex3f(-d,-d, d);
      glTexCoord2f(1, 1); glvertex3f(-d, d, d);
      glTexCoord2f(0, 1); glvertex3f( d, d, d);
    glend;
  //� �����
  glBindTexture(GL_TEXTURE_2D, SkyBoxTexture[3]);
    glbegin(GL_QUADS);
      glTexCoord2f(0, 0); glvertex3f(d,-d,-d);
      glTexCoord2f(1, 0); glvertex3f(d,-d, d);
      glTexCoord2f(1, 1); glvertex3f(d, d, d);
      glTexCoord2f(0, 1); glvertex3f(d, d,-d);
    glend;
  // � ����
  glBindTexture(GL_TEXTURE_2D, SkyBoxTexture[0]);
    glbegin(GL_QUADS);
      glTexCoord2f(0, 0); glvertex3f(-d,-d, d);
      glTexCoord2f(1, 0); glvertex3f(-d,-d,-d);
      glTexCoord2f(1, 1); glvertex3f(-d, d,-d);
      glTexCoord2f(0, 1); glvertex3f(-d, d, d);
    glend;
  //� ����
  glBindTexture(GL_TEXTURE_2D, SkyBoxTexture[1]);
    glbegin(GL_QUADS);
      glTexCoord2f(0, 0); glvertex3f(-d,-d, d);
      glTexCoord2f(1, 0); glvertex3f( d,-d, d);
      glTexCoord2f(1, 1); glvertex3f( d,-d,-d);
      glTexCoord2f(0, 1); glvertex3f(-d,-d,-d);
    glend;
  //� �����
  glBindTexture(GL_TEXTURE_2D, SkyBoxTexture[4]);
    glbegin(GL_QUADS);
      glTexCoord2f(0, 0); glvertex3f(-d, d,-d);
      glTexCoord2f(1, 0); glvertex3f( d, d,-d);
      glTexCoord2f(1, 1); glvertex3f( d, d, d);    
      glTexCoord2f(0, 1); glvertex3f(-d, d, d);
    glend;
  glPopMatrix;
  glEndList;
end;

procedure TDraw.SkyBox(Id: TSkyBox);
begin
  glEnable(GL_TEXTURE_2D);
  glDepthMask(False);
  glDisable(GL_LIGHTING);

  glCallList(Id);

  glEnable(GL_LIGHTING);
  glDepthMask(True);
  glDisable(GL_TEXTURE_2D);
end;

procedure TDraw.SkyDome(Tex: TTex);
begin
  glDepthMask(False);
  glDisable(GL_LIGHTING);
  MSE.Texture.Use(Tex);

  glPushMatrix;
    //glTranslatef(MSE.Camera.Pos.X, MSE.Camera.Pos.Y, MSE.Camera.Pos.Z);
    glScalef(1, 0.3, 1);

    //glCallList(SkyDomeID);
  glPopMatrix;

  MSE.Texture.Use(0);
  glEnable(GL_LIGHTING);
  glDepthMask(True);
end;

function TDraw.CreateFont(Dir: PChar; W, H: Single): TFont;
const
  q = 1/16;
var
  d: Single;
  i: Integer;
  xt, yt: Single;
begin
  Result.W := W;
  Result.H := H;
  Result.Texture := MSE.Texture.Load(Dir);

  MSE.Texture.Use(Result.Texture);

  for i:=0 to 255 do
  begin
    Result.Key[i] := glGenLists(1);
    glNewList(Result.Key[i], GL_COMPILE);
  
    d  := i;
    xt := 0;//���������� ����������
    yt := 0;//���������� ����������

    repeat//���� ��� ������� ������ � ��������
      if d > 15 then begin
        d := d-16;
        yt := yt+q;
      end;
    until d < 16;
    xt := d*q;

    glBegin(GL_QUADS);//������ ���� � ����� ��������
      glTexCoord2f(xt,  1-yt-q); glVertex2f(-1,  1-h);
      glTexCoord2f(xt+q,1-yt-q); glVertex2f(-1+w,1-h);
      glTexCoord2f(xt+q,1-yt);   glVertex2f(-1+w,1);
      glTexCoord2f(xt,  1-yt);   glVertex2f(-1,  1);
    glEnd;

  glEndList;
  end;

  MSE.Texture.Use(0);
end;

procedure TDraw.DrawText(Font: TFont; s: string; X, Y: Integer);
begin
  MSE.OGL.Alpha := 255;
  DrawText(font, s, X, Y, 1, 1, 1);
end;

procedure TDraw.DrawText(Font: TFont; s: string; X, Y: Integer; R, G, B: Single);
var
  i, kod: Integer;
begin
  glEnable(GL_TEXTURE_2D);
  glDisable(GL_LIGHTING);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glColor3f(R, G, B);  

  MSE.Texture.Use(Font.Texture);

  glPushMatrix;
    glTranslatef((Font.W/2)*X, -Font.H*Y, 0);
    for i:=1 to Length(s) do
    begin
      if ord(s[i])>256 then
        kod := ord(s[i])-848
      else
        kod := ord(s[i]);

      glCallList(font.Key[kod]);
      glTranslatef(Font.W/2, 0, 0);
    end;
  glPopMatrix;

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glDisable(GL_BLEND);
  MSE.Texture.Use(0);
  MSE.OGL.SetColor(1, 1, 1);
  glDisable(GL_TEXTURE_2D);
end;

procedure TDraw.SetFog(R, G, B: Byte; zNear, zFar: Single);
var
  Color: array[0..3] of GLfloat;
begin
  Color[0] := R;
  Color[1] := G;
  Color[2] := B;
  Color[3] := 1;

  glFogi(GL_FOG_MODE, GL_LINEAR);
  glHint(GL_FOG_HINT, GL_NICEST);
  glFogf(GL_FOG_START, zNear);
  glFogf(GL_FOG_END, zFar);
  glFogfv(GL_FOG_COLOR, @Color);
end;

procedure TDraw.FogVisible(Param: Boolean);
begin
  if Param then
    glEnable(GL_FOG)
  else
    glDisable(GL_FOG);  
end;

procedure TDraw.Sphere(R: Single; Tex: TTex);
var
  q: PGLUquadric;
begin
  if Tex = 0 then
    glDisable(GL_TEXTURE_2D);

  MSE.Texture.Use(Tex);
  q := gluNewQuadric;
  glPushMatrix();
    glScalef(R, R, R);
    gluQuadricTexture(q, TRUE);
    gluSphere(q, 1, 30, 30);
    glCallList(SphereID);
  glPopMatrix();
  gluDeleteQuadric(q);
  MSE.Texture.Use(0);
  glEnable(GL_TEXTURE_2D);
end;

procedure TDraw.Box(lx, ly, lz: Single; Tex: TTex);
begin
  MSE.Texture.Use(Tex);
  glPushMatrix();
    glScalef(lx, ly, lz);
    glCallList(BoxID);
  glPopMatrix();
  MSE.Texture.Use(0);
end;

procedure TDraw.Cylinder(R, L: Single; Tex: TTex);
var
  q: PGLUquadric;
begin
  MSE.Texture.Use(Tex);
  glPushMatrix();
    glScalef(R, R, L);
    q := gluNewQuadric;
    glPushMatrix;
      glTranslatef(0, 0, -0.5);
      gluQuadricTexture(q, TRUE);
      gluCylinder(q, 1, 1, 1, 20, 20);
    glPopAttrib;
    gluDeleteQuadric(q);
  glPopMatrix;
  MSE.Texture.Use(0);
end;

procedure TDraw.BillBoard(Pos: TVector3f; SizeX, SizeY: Single; Tex: TTex;
  A: Integer);
var
  m: TGLMatrixd4;
  v: array [0..3] of TVector3f;
begin
  glDisable(GL_LIGHTING);
  glDepthMask(False);
  glEnable(GL_TEXTURE_2D);
  MSE.Texture.Use(Tex);

    glEnable(GL_BLEND);
    glColor4f(1.0, 1.0, 1.0, A/255);

    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GEQUAL, 0.0);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  glPushMatrix;
    glGetDoublev(GL_MODELVIEW_MATRIX, @m);

    v[0].X := (-m[0][0]-m[0][1]);
    v[0].Y := (-m[1][0]-m[1][1]);
    v[0].Z := (-m[2][0]-m[2][1]);
    v[0]   := MSE.Math.NormalizeVector(v[0]);
    v[0]   := MSE.Math.VextorMultConst(v[0], SizeX);

    v[1].X := (m[0][0]-m[0][1]);
    v[1].Y := (m[1][0]-m[1][1]);
    v[1].Z := (m[2][0]-m[2][1]);
    v[1]   := MSE.Math.NormalizeVector(v[1]);
    v[1]   := MSE.Math.VextorMultConst(v[1], SizeX);

    v[2].X := (m[0][0]+m[0][1]);
    v[2].Y := (m[1][0]+m[1][1]);
    v[2].Z := (m[2][0]+m[2][1]);
    v[2]   := MSE.Math.NormalizeVector(v[2]);
    v[2]   := MSE.Math.VextorMultConst(v[2], SizeY);

    v[3].X := (-m[0][0]+m[0][1]);
    v[3].Y := (-m[1][0]+m[1][1]);
    v[3].Z := (-m[2][0]+m[2][1]);
    v[3]   := MSE.Math.NormalizeVector(v[3]);
    v[3]   := MSE.Math.VextorMultConst(v[3], SizeY);

    glTranslatef(pos.X, pos.Y, pos.Z);
    glBegin(GL_QUADS);
      glTexCoord2f(0, 0); glVertex3fv(@v[0]);
      glTexCoord2f(1, 0); glVertex3fv(@v[1]);
      glTexCoord2f(1, 1); glVertex3fv(@v[2]);
      glTexCoord2f(0, 1); glVertex3fv(@v[3]);
    glEnd;
  glPopMatrix;

  glDisable(GL_ALPHA_TEST);
  glDisable(GL_BLEND);
  glEnable(GL_LIGHTING);
  glDepthMask(True);
end;

procedure TDraw.Box(BBox: TBBox; Tex: TTex);
begin
  glPushMatrix;
    glTranslatef(BBox.X, BBox.Y, BBox.Z);
    Box(BBox.W, BBox.H, BBox.D, Tex);
  glPopMatrix;
end;

procedure TDraw.BillBoard(Pos: TVector3f; SizeX, SizeY: Single; Tex: TTex);
var
  m: TGLMatrixd4;
  v: array [0..3] of TVector3f;
begin
  glPushMatrix;
    glEnable(GL_TEXTURE_2D);
    MSE.Texture.Use(Tex);
    glDisable(GL_LIGHTING);
    glGetDoublev(GL_MODELVIEW_MATRIX, @m);

    v[0].X := (-m[0][0]-m[0][1]);
    v[0].Y := (-m[1][0]-m[1][1]);
    v[0].Z := (-m[2][0]-m[2][1]);
    v[0]   := MSE.Math.NormalizeVector(v[0]);
    v[0]   := MSE.Math.VextorMultConst(v[0], SizeX);

    v[1].X := (m[0][0]-m[0][1]);
    v[1].Y := (m[1][0]-m[1][1]);
    v[1].Z := (m[2][0]-m[2][1]);
    v[1]   := MSE.Math.NormalizeVector(v[1]);
    v[1]   := MSE.Math.VextorMultConst(v[1], SizeX);

    v[2].X := (m[0][0]+m[0][1]);
    v[2].Y := (m[1][0]+m[1][1]);
    v[2].Z := (m[2][0]+m[2][1]);
    v[2]   := MSE.Math.NormalizeVector(v[2]);
    v[2]   := MSE.Math.VextorMultConst(v[2], SizeY);

    v[3].X := (-m[0][0]+m[0][1]);
    v[3].Y := (-m[1][0]+m[1][1]);
    v[3].Z := (-m[2][0]+m[2][1]);
    v[3]   := MSE.Math.NormalizeVector(v[3]);
    v[3]   := MSE.Math.VextorMultConst(v[3], SizeY);
    
    glTranslatef(pos.X, pos.Y, pos.Z);
    glBegin(GL_QUADS);
      glTexCoord2f(0, 0); glVertex3fv(@v[0]);
      glTexCoord2f(1, 0); glVertex3fv(@v[1]);
      glTexCoord2f(1, 1); glVertex3fv(@v[2]);
      glTexCoord2f(0, 1); glVertex3fv(@v[3]);
    glEnd;

    glEnable(GL_LIGHTING);
    MSE.Texture.Use(0);
    glDisable(GL_TEXTURE_2D);
  glPopMatrix;
end;


procedure TDraw.Sprite3D(Size: Single; Tex: TTex);
begin
  glEnable(GL_TEXTURE_2D);
  MSE.Texture.Use(Tex);
  glPushMatrix();
    glScalef(Size, Size, Size);
    glCallList(Sprite3DID);
  glPopMatrix();
  MSE.Texture.Use(0);
  glDisable(GL_TEXTURE_2D);
end;

procedure TDraw.Sprite2D(X, Y, W, H: Single; Tex: TTex);
begin
  glEnable(GL_TEXTURE_2D);
  MSE.Texture.Use(Tex);
    glBegin(GL_QUADS);
      glTexCoord2f(0, 0); glVertex2f(X,   Y-H);
      glTexCoord2f(1, 0); glVertex2f(X+W, Y-H);
      glTexCoord2f(1, 1); glVertex2f(X+W, Y);
      glTexCoord2f(0, 1); glVertex2f(X,   Y);
    glEnd;
  MSE.Texture.Use(0);
  glDisable(GL_TEXTURE_2D);
end;

procedure TDraw.Cursor(W, H: Single; Tex: TTex);
begin
  {MSE.GUI.Draw;
  COGL.Blend(BT_SUB, 255);
  Sprite2D(CInput.GetMousePosOGL.X,
           CInput.GetMousePosOGL.Y,
           W, H, Tex);
  COGL.Blend(BT_NONE, 255);}         
end;

procedure TDraw.Sight(W, H: Single; Tex: TTex);
begin
  glEnable(GL_BLEND);
    Sprite2D((-W/2), H/2, W, H, Tex);
  glDisable(GL_BLEND);
end;

procedure TDraw.Line(v1, v2: TVector3f; Color: TRGB; Size: Integer);
begin
  glDisable(GL_LIGHTING);
  MSE.OGL.SetColor(Color);
  glLineWidth(Size);
  glBegin(GL_LINES);
    glVertex3fv(@v1);
    glVertex3fv(@v2);
  glEnd;
  glLineWidth(1);
  MSE.OGL.SetColor(1, 1, 1);
  glEnable(GL_LIGHTING);
end;

procedure TDraw.Line(X1, Y1, Z1, X2, Y2, Z2: Single; Color: TRGB;
  Size: Integer);
var
  v1, v2: TVector3f;
begin
  v1.X := X1;
  v1.Y := Y1;
  v1.Z := Z1;

  v2.X := X2;
  v2.Y := Y2;
  v2.Z := Z2;

  Line(v1, v2, Color, Size);
end;

procedure TDraw.BoxLine(BBox: TBBox; Size: Integer);
begin
  glDisable(GL_LIGHTING);
  glLineWidth(Size);
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    Box(BBox, 0);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glLineWidth(1);
  glEnable(GL_LIGHTING);
end;

procedure TDraw.Box(BBox: TBBox; R, G, B: Single; A: Byte = 255);
begin
  glDisable(GL_TEXTURE_2D);
    MSE.OGL.SetColor(R, G, B);
    MSE.OGL.Alpha := A;
    Box(BBox, 0);
    MSE.OGL.Alpha := 255;
  glEnable(GL_TEXTURE_2D);
end;

procedure TDraw.BoxLine(BBox: TBBox; Size: Integer; R, G, B: Single);
begin
  MSE.OGL.SetColor(R, G, B);
    BoxLine(BBox, Size);
  MSE.OGL.SetColor(1, 1, 1);
end;


end.
