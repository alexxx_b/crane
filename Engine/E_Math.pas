unit E_Math;

interface

uses
  Windows, OpenGL20, E_Type;

type
  TMath = class
    public
      function IntToStr(Value: Integer): string;
      function StrToInt(s: string): Integer;
      function NormalizeVector(Normal: TVector3f): TVector3f;
      function VextorMultConst(Vector: TVector3f; Scale: Single): TVector3f;
      function Distance2D(x1, y1, x2, y2: Single): Single;
      function CrossVector(Vector1, Vector2: TVector3f): TVector3f;
      function SubVector(Vector1, Vector2: TVector3f): TVector3f;
      function GetNormalVector(v1, v2, v3: TVector3f): TVector3f;
      procedure SetTransform (Matrix: TMatrix);
      function Distance3D(v1, v2: TVector3f): Single;
      function AddVector(V1, V2: TVector3f): TVector3f;

      //��������� ������������
      function Dot3d(V1, V2: TVector3f): Single;
      function GetVector(x, y, z: Single): TVector3f;

      procedure MatrixInverse(var M: TMatrix);
      function MatrixDeterminant(M: TMatrix): Single;
      procedure MatrixAdjoint(var M: TMatrix);
      procedure MatrixScale(var M: TMatrix; Factor: Single);

      function GetMax2(x1, x2: Single): Single;
      function GetMax3(x1, x2, x3: Single): Single;
      procedure GetSinCosMatrix(m: TMatrix; var sx,sy,sz,cx,cy,cz: Double);

      function GetZnak(AValue: Single): Integer;

      function GetBBox(X, Y, Z, W, H, D: Single): TBBox;

//      GetPosBox;
    private
  end;

const
  Rad = 360/(2*pi);
  Epsilon = 0.0001;
  IdentityMatrix : TMatrix = (1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

implementation

function TMath.IntToStr(Value: Integer): string;
begin
  Str(Value, Result);
end;

function TMath.StrToInt(s: string): Integer;
var
  er: Integer;
begin
  Val(s, Result, er);
end;

function MagnitudeVector(Normal: TVector3f): GLfloat;
begin
  Result := Sqrt((Normal.X * Normal.X) + (Normal.Y * Normal.Y) + (Normal.Z * Normal.Z));
end;

function TMath.NormalizeVector(Normal: TVector3f): TVector3f;
var
  Magnitude: GLfloat;
begin
  Magnitude := MagnitudeVector(Normal);
  Result.X := Normal.X / Magnitude;
  Result.Y := Normal.Y / Magnitude;
  Result.Z := Normal.Z / Magnitude;
end;


function TMath.VextorMultConst(Vector: TVector3f; Scale: Single): TVector3f;
begin
  Result.X := Vector.X*Scale;
  Result.Y := Vector.Y*Scale;
  Result.Z := Vector.Z*Scale;
end;

function TMath.Distance2D(x1, y1, x2, y2: Single): Single;
begin
  Result := ABS(sqrt(sqr(x2-x1) + sqr(y2-y1)));
end;

function TMath.AddVector(V1, V2: TVector3f): TVector3f;
begin
  Result.X := V1.X + V2.X;
  Result.Y := V1.Y + V2.Y;
  Result.Z := V1.Z + V2.Z;
end;

function TMath.CrossVector(Vector1, Vector2: TVector3f): TVector3f;
begin
  Result.X := (Vector1.Y * Vector2.Z) - (Vector1.Z * Vector2.Y);
  Result.Y := (Vector1.Z * Vector2.X) - (Vector1.X * Vector2.Z);
  Result.Z := (Vector1.X * Vector2.Y) - (Vector1.Y * Vector2.X);
end;

function TMath.SubVector(Vector1, Vector2: TVector3f): TVector3f;
begin
  Result.X := Vector1.X - Vector2.X;
  Result.Y := Vector1.Y - Vector2.Y;
  Result.Z := Vector1.Z - Vector2.Z;
end;

function TMath.GetNormalVector(v1, v2, v3: TVector3f): TVector3f;
begin
  Result.X := NormalizeVector(CrossVector(SubVector(v2, v1), SubVector(v3, v2))).x;
  Result.Y := NormalizeVector(CrossVector(SubVector(v2, v1), SubVector(v3, v2))).y;
  Result.Z := NormalizeVector(CrossVector(SubVector(v2, v1), SubVector(v3, v2))).z;
end;

procedure TMath.GetSinCosMatrix(m: TMatrix; var sx, sy, sz, cx, cy, cz: Double);
begin
 sx := -m[9];
 cx := 1-sx*sx;
 sy := m[10]/cx;
 cy := m[8]/cx;
 sz := m[1]/cx;
 cz := m[5]/cx;
end;

function TMath.GetVector(x, y, z: Single): TVector3f;
begin
  Result.X := x;
  Result.Y := y;
  Result.Z := z;
end;

function TMath.GetZnak(AValue: Single): Integer;
begin
  Result := 1;
  if AValue < 0 then Result := -1;
  if AValue = 0 then Result := 0;
end;

procedure TMath.SetTransform(Matrix: TMatrix);
begin
  glMultMatrixf(@Matrix);
end;


function TMath.Distance3D(v1, v2: TVector3f): Single;
begin
  Result := Sqrt(Sqr(v2.X-v1.X)+Sqr(v2.Y-v1.Y)+Sqr(v2.Z-v1.Z));
end;


function TMath.Dot3d(V1, V2: TVector3f): Single;
begin
  Result := V1.X * V2.X + V1.Y * V2.Y + V1.Z * V2.Z;
end;

procedure TMath.MatrixInverse(var M: TMatrix);
var
 Det: Single;
begin
  Det := MatrixDeterminant(M);
  if Abs(Det) < EPSILON then M := IdentityMatrix
                        else
  begin
    MatrixAdjoint(M);
    MatrixScale(M, 1 / Det);
  end;
end;

function MatrixDetInternal(a1, a2, a3, b1, b2, b3, c1, c2, c3: Single): Single;
begin
Result := a1 * (b2 * c3 - b3 * c2) -
          b1 * (a2 * c3 - a3 * c2) +
          c1 * (a2 * b3 - a3 * b2);
end;

function TMath.MatrixDeterminant(M: TMatrix): Single;
var a1, a2, a3, a4,
    b1, b2, b3, b4,
    c1, c2, c3, c4,
    d1, d2, d3, d4  : Single;
begin
  a1 := M[0];  b1 := M[1];  c1 := M[2];  d1 := M[3];
  a2 := M[4];  b2 := M[5];  c2 := M[6];  d2 := M[7];
  a3 := M[8];  b3 := M[9];  c3 := M[10];  d3 := M[11];
  a4 := M[12];  b4 := M[13];  c4 := M[14];  d4 := M[15];
  Result := a1 * MatrixDetInternal(b2, b3, b4, c2, c3, c4, d2, d3, d4) -
            b1 * MatrixDetInternal(a2, a3, a4, c2, c3, c4, d2, d3, d4) +
            c1 * MatrixDetInternal(a2, a3, a4, b2, b3, b4, d2, d3, d4) -
            d1 * MatrixDetInternal(a2, a3, a4, b2, b3, b4, c2, c3, c4);
end;

procedure TMath.MatrixAdjoint(var M: TMatrix);
var a1, a2, a3, a4,
    b1, b2, b3, b4,
    c1, c2, c3, c4,
    d1, d2, d3, d4: Single;
begin
    a1 :=  M[0]; b1 :=  M[1];
    c1 :=  M[2]; d1 :=  M[3];
    a2 :=  M[4]; b2 :=  M[5];
    c2 :=  M[6]; d2 :=  M[7];
    a3 :=  M[8]; b3 :=  M[9];
    c3 :=  M[10]; d3 :=  M[11];
    a4 :=  M[12]; b4 :=  M[13];
    c4 :=  M[14]; d4 :=  M[15];
    // row column labeling reversed since we transpose rows & columns
    M[0] :=  MatrixDetInternal(b2, b3, b4, c2, c3, c4, d2, d3, d4);
    M[4] := -MatrixDetInternal(a2, a3, a4, c2, c3, c4, d2, d3, d4);
    M[8] :=  MatrixDetInternal(a2, a3, a4, b2, b3, b4, d2, d3, d4);
    M[12] := -MatrixDetInternal(a2, a3, a4, b2, b3, b4, c2, c3, c4);

    M[1] := -MatrixDetInternal(b1, b3, b4, c1, c3, c4, d1, d3, d4);
    M[5] :=  MatrixDetInternal(a1, a3, a4, c1, c3, c4, d1, d3, d4);
    M[9] := -MatrixDetInternal(a1, a3, a4, b1, b3, b4, d1, d3, d4);
    M[13] :=  MatrixDetInternal(a1, a3, a4, b1, b3, b4, c1, c3, c4);

    M[2] :=  MatrixDetInternal(b1, b2, b4, c1, c2, c4, d1, d2, d4);
    M[6] := -MatrixDetInternal(a1, a2, a4, c1, c2, c4, d1, d2, d4);
    M[10] :=  MatrixDetInternal(a1, a2, a4, b1, b2, b4, d1, d2, d4);
    M[14] := -MatrixDetInternal(a1, a2, a4, b1, b2, b4, c1, c2, c4);

    M[3] := -MatrixDetInternal(b1, b2, b3, c1, c2, c3, d1, d2, d3);
    M[7] :=  MatrixDetInternal(a1, a2, a3, c1, c2, c3, d1, d2, d3);
    M[11] := -MatrixDetInternal(a1, a2, a3, b1, b2, b3, d1, d2, d3);
    M[15] :=  MatrixDetInternal(a1, a2, a3, b1, b2, b3, c1, c2, c3);
end;

procedure TMath.MatrixScale(var M: TMatrix; Factor: Single);
var
  i: Integer;
begin
  for i := 0 to 15 do
    M[i] := M[i]*Factor;
end;

function TMath.GetBBox(X, Y, Z, W, H, D: Single): TBBox;
begin
  Result.X := X;
  Result.Y := Y;
  Result.Z := Z;

  Result.W := W;
  Result.H := H;
  Result.D := D;
end;

function TMath.GetMax2(x1, x2: Single): Single;
begin
  if x1 > x2 then Result := x1
  else
    Result := x2;
end;

function TMath.GetMax3(x1, x2, x3: Single): Single;
begin
  Result := x1;
  if (x2>x1) and (x2>x3) then Result := x2;
  if (x3>x1) and (x3>x2) then Result := x3;
end;

end.
